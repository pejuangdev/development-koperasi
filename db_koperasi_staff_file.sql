# Host: localhost  (Version: 5.5.39)
# Date: 2016-06-29 16:33:11
# Generator: MySQL-Front 5.3  (Build 4.187)

/*!40101 SET NAMES latin1 */;

#
# Structure for table "tbl_file"
#

DROP TABLE IF EXISTS `tbl_file`;
CREATE TABLE `tbl_file` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(200) NOT NULL,
  `deskripsi_file` text,
  `tanggal_file` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `source_file` text NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_file"
#

INSERT INTO `tbl_file` VALUES (1,'Piutang Oke','utang','2016-06-28 16:21:32','utang\n');

#
# Structure for table "tbl_staff"
#

DROP TABLE IF EXISTS `tbl_staff`;
CREATE TABLE `tbl_staff` (
  `id_staff` int(11) NOT NULL AUTO_INCREMENT,
  `nama_staff` varchar(100) NOT NULL,
  `jabatan` varchar(40) NOT NULL,
  `deskripsi_staff` text,
  `email_staff` varchar(100) DEFAULT NULL,
  `foto_staff` text,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_staff`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_staff"
#

INSERT INTO `tbl_staff` VALUES (1,'Rachmat Yunisar','Ketua Umum','Kadiv. Marketing & TND',NULL,'rachmat.png',1),(2,'I Komang Adiwiguna','Bendahara I','Kadept.HR & GA',NULL,'komang.png',0),(3,'Hardiyanto','Bendahara II','Kasie.Operasional',NULL,'hardiyanto.png',0),(4,'Osshie Ghonie Prasetyo','Sekretaris','Kasie.Marketing',NULL,'oshie.png',0),(5,'Hadi Muhardjono','Boss','Bosss\t',NULL,'hadi.png',0),(6,'Yoga Tufikurohman','Boss2','boss2',NULL,'yoga.png',0),(7,'A.Setyo Mulyanto','Supir','bosss',NULL,'setyo.png',0),(8,'Rachmat Yunisar','',NULL,NULL,'rachmat.png',0);
