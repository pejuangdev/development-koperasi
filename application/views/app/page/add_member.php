<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA ANGGOTA</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH ANGGOTA</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/anggota/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">NPK</label>
                        <input type="text" class="form-control" value="" name="npk" placeholder="Kode Cost Center" required>
                    </div>
                    <div class="form-group">
                      <label for="">Nama Anggota</label>
                        <input type="text" class="form-control" name="nama" placeholder="Nama Cost Center" required>                      
                    </div>
                    <div class="form-group">
                      <label for="">Alamat Anggota</label>
                      <textarea style="height: 100px;" name="alamat" placeholder="Alamat"  class="form-control" required></textarea>                      
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Tanggal Lahir</label>
                        <input type="text" class="form-control" value="" name="tgl_lahir" placeholder="Tanggal Lahir" required>
                    </div>
                    <div class="form-group">
                      <label for="">Tanggal Gabung</label>
                        <input type="text" class="form-control" value="" name="tgl_gabung" placeholder="Tanggal Gabung" required>
                    </div>
                    <div class="form-group">
                      <label for="">Status</label>
                      <select name="status" class="form-control">
                        <option value="1">Aktif</option>
                        <option value="0">Tidak Aktif</option>
                      </select>                      
                    </div>
                    
                  </div>
                    
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/anggota" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->
      </section><!-- /.content -->

<script type="text/javascript">
$(function() {
    $('input[name="tgl_lahir"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
          format:'YYYY-MM-DD'
        }
    });
    $('input[name="tgl_gabung"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
          format:'YYYY-MM-DD'
        }
    });

});
</script>