<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA INSTALASI</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH INSTALASI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/instalasi/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Kode Instalasi</label>
                        <input type="text" class="form-control" value="" name="id_instalasi" placeholder="Kode Instalasi" required>
                    </div>
                    <div class="form-group">
                      <label for="">Nama Instalasi</label>
                        <input type="text" class="form-control" name="nama_instalasi" placeholder="Nama Instalasi" required>                      
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">ARH</label>
                        <input type="text" value="" class="form-control" id="" name="arh" placeholder="Nama ARH" required>                        
                    </div>
                    <div class="form-group">
                      <label for="">Kode Cost Center</label>
                        <select name="kode_cost" class="form-control">
                          <?php foreach ($data as $key => $value) { ?>
                          <option value="<?php echo $value->id_cost_center ?>"><?php echo $value->id_cost_center . "-" . $value->nama_cost_center; ?></option>
                          <?php } ?>
                        </select> 
                    </div>          
                  </div>
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/instalasi" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
