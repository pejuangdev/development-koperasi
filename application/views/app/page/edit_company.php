<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA COST CENTER</b>
        </h1>
        </section>
        <?php foreach ($data as $key => $value) { ?>
          
        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT COST CENTER</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/company/update_data/<?php echo $value->id_cost_center; ?>" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Kode Cost Center</label>
                        <input type="text" class="form-control" value="<?php echo $value->id_cost_center;?>" name="id_cost" placeholder="Kode Cost Center" required>
                    </div>
                    <div class="form-group">
                      <label for="">Nama Cost Center</label>
                        <input type="text" value="<?php echo $value->nama_cost_center; ?>" class="form-control" name="nama_cost" placeholder="Nama Cost Center" required>                      
                    </div>
                  </div>
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/company" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">
            <?php } ?>
        
          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->