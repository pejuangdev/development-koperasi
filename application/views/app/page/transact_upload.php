<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>UPLOAD DATA TRANSAKSI</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM UPLOAD TRANSAKSI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/transaksi/upload_data" method="POST" enctype="multipart/form-data">
                   

                   <div class="col-lg-6">
                    <div class="form-group">
                        <!-- <label for="">Pilih Tipe Potongan</label>
                        <select name="tipe_potongan" class="form-control">
                            <option value="1">Simpanan Wajib</option> 
                            <option value="2">Simpanan Pokok</option> 
                            <option value="3">Simpanan Sukarela</option> 
                            <option value="4">Potongan</option> 
                            <option value="5">Potongan Belanja</option> 
                            <option value="6">Potongan Sembako</option> 
                            <option value="7">Potongan OR</option> 
                        </select> -->
                        <label for="">Tahun</label>
                        <select name="tahun" class="form-control" required="require">
                            <?php
                                for ($i=2009; $i < 2031; $i++) { 
                                  # code...
                                
                            ?>
                            <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group"> 
                        <label for="">Tahun</label>
                        <select name="bulan" class="form-control" required="require">
                            <?php
                                $arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agutus","September","Oktober","November","Desember");
                                for ($i=1; $i <= 12; $i++) { 
                                  
                                
                            ?>
                            <option value="<?php echo $i; ?>" ><?php echo $arr[$i];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="form-group">
                        <label for="">Data Transaksi (.XLS)</label>
                        <input type="file" class="form-control" value="" name="userfile" placeholder="Data Cost Center (HARUS FORMAT XLS)" required>
                    </div>
                  </div>
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/transaksi" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->