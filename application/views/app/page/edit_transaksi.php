<!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          <b>EDIT DATA TRANSAKSI</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT TRANSAKSI</h3>
              </div>
              <?php foreach ($data as $key => $value_trans) { ?>
              
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/transaksi/update_data/<?php echo $value_trans->id_transaksi; ?>" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">NPK</label>
                        <select name="npk" id="npk" required class="form-control selectpicker" data-live-search="true">
                            <option value="<?php echo $value_trans->npk; ?>"><?php echo $value_trans->nama_anggota; ?></option>
                            <?php foreach ($anggota as $key => $value) {
                              echo "<option value='". $value->npk ."'>". $value->nama_anggota ."</option>";
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="">Instalasi</label>
                        <select name="instalasi" required class="form-control selectpicker" data-live-search="true">
                            <option value="<?php echo $value_trans->id_instalasi; ?>"><?php echo $value_trans->nama_instalasi; ?></option>
                            <?php foreach ($instalasi as $key => $value) {
                              echo "<option value='". $value->id_instalasi ."'>". $value->nama_instalasi ."</option>";
                            }?>
                        </select>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Bulan Transaksi</label>
                        <select name="bulan" required class="form-control">
                            <option value="<?php echo $value_trans->bulan_transaksi; ?>"><?php echo $this->mod_transaksi->listBulan($value_trans->bulan_transaksi); ?></option>
                            <?php for ($i=1; $i <= 12; $i++) { 
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $this->mod_transaksi->listBulan($i);?></option>
                              <?php
                                  }
                              ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="">Tahun Transaksi</label>
                        <select name="tahun" required class="form-control">
                            <option value="<?php echo $value_trans->tahun_transaksi; ?>"><?php echo $value_trans->tahun_transaksi; ?></option>
                            <?php
                                  for ($i=2009; $i < 2031; $i++) { 
                                    # code...
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                              <?php
                                  }
                              ?>
                        </select>
                    </div>
                  </div>
                  
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Simpanan Wajib</label>
                      <input type="text" name="sim_jip" required class="form-control" value="<?php echo $value_trans->simpanan_wajib; ?>" placeholder="Simpanan Wajib (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Simpanan Pokok</label>
                      <input type="text" name="sim_pok" class="form-control" value="<?php echo $value_trans->simpanan_pokok; ?>" placeholder="Simpanan Pokok (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Simpanan Sukarela</label>
                      <input type="text" name="sim_suk" class="form-control" value="<?php echo $value_trans->simpanan_sukarela; ?>"type placeholder="Simpanan Sukarela (contoh : 400000)">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Pinjaman Anggota</label>
                      <input type="text" name="pinjaman" class="form-control" value="<?php echo $value_trans->pinjaman; ?>" placeholder="Pinjaman Anggota (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Potongan Belanja</label>
                      <input type="text" name="pot_bel" class="form-control" value="<?php echo $value_trans->potongan_belanja; ?>" placeholder="Potongan Belanja (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Potongan Sembako</label>
                      <input type="text" name="pot_sem" class="form-control" value="<?php echo $value_trans->potongan_sembako; ?>" placeholder="Potongan Belanja (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Potongan OR</label>
                      <input type="text" name="pot_or" class="form-control" value="<?php echo $value_trans->potongan_or; ?>" placeholder="Potongan Belanja (contoh : 400000)">
                    </div>
                  </div>
              <?php } ?>    
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/transaksi" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
