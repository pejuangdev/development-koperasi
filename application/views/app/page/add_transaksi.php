<!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA TRANSAKSI</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH TRANSAKSI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/transaksi/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">NPK</label>
                        <select name="npk" id="npk" required class="form-control selectpicker" data-live-search="true">
                            <option value="">Pilih NPK</option>
                            <?php foreach ($anggota as $key => $value) {
                              echo "<option value='". $value->npk ."'>". $value->nama_anggota ."</option>";
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="">Instalasi</label>
                        <select name="instalasi" required class="form-control selectpicker" data-live-search="true">
                            <option value="">Pilih Instalasi</option>
                            <?php foreach ($instalasi as $key => $value) {
                              echo "<option value='". $value->id_instalasi ."'>". $value->nama_instalasi ."</option>";
                            }?>
                        </select>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Bulan Transaksi</label>
                        <select name="bulan" required class="form-control">
                            <option value="">Pilih Bulan</option>
                            <?php for ($i=1; $i <= 12; $i++) { 
                                    
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $this->mod_transaksi->listBulan($i);?></option>
                              <?php
                                  }
                              ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="">Tahun Transaksi</label>
                        <select name="tahun" required class="form-control">
                            <option value="">Pilih Tahun</option>
                            <?php
                                  for ($i=2009; $i < 2031; $i++) { 
                                    # code...
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                              <?php
                                  }
                              ?>
                        </select>
                    </div>
                  </div>
                  
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Simpanan Wajib</label>
                      <input type="text" name="sim_jip" required class="form-control" placeholder="Simpanan Wajib (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Simpanan Pokok</label>
                      <input type="text" name="sim_pok" class="form-control" placeholder="Simpanan Pokok (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Simpanan Sukarela</label>
                      <input type="text" name="sim_suk" class="form-control" placeholder="Simpanan Sukarela (contoh : 400000)">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Pinjaman Anggota</label>
                      <input type="text" name="pinjaman" class="form-control" placeholder="Pinjaman Anggota (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Potongan Belanja</label>
                      <input type="text" name="pot_bel" class="form-control" placeholder="Potongan Belanja (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Potongan Sembako</label>
                      <input type="text" name="pot_sem" class="form-control" placeholder="Potongan Belanja (contoh : 400000)">
                    </div>
                    <div class="form-group">
                      <label for="">Potongan OR</label>
                      <input type="text" name="pot_or" class="form-control" placeholder="Potongan Belanja (contoh : 400000)">
                    </div>
                  </div>
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/transaksi" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
