<section class="content-header">
        <h1>
          <b>DATA ANGGOTA KOPERASI</b>
        </h1>
        
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Add Anggota </button>
            <button class="btn btn-warning" onclick="upload()"><i class="glyphicon glyphicon-upload"></i> Upload Anggota </button>
            <button class="btn btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
            <button class="btn btn-danger" onclick="document.location.href='<?php echo site_url(); ?>/app/anggota/delete_all'"><i class="glyphicon glyphicon-remove"></i> Delete All </button>
              <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Anggota Koperasi</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>Id Kop</th>
                          <th>NPK</th>
                          <th>Nama</th>
                          <th>Tanggal Lahir</th>
                          <th>Tanggal Gabung</th>
                          <th>Tanggal Keluar</th>
                          <th>Alamat</th>
                          <th>Status</th>
                          <th style="width:125px;">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>

                  <tfoot>
                  <tr>
                      <th>Id Kop</th>
                      <th>NPK</th>
                      <th>Nama</th>
                      <th>Tanggal Lahir</th>
                      <th>Tanggal Gabung</th>
                      <th>Tanggal Keluar</th>
                      <th>Alamat</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                  </tfoot>
              </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('app/anggota/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});



function add_person()
{   
    document.formanggota.tgl_keluar.disabled=true;
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Anggota'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Instalasi'); // Set Title to Bootstrap modal title
}

function edit_person(id)
{
    document.formanggota.tgl_keluar.disabled=false;
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('app/anggota/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_kop"').val(data.id_kop);
            $('[name="npk"]').val(data.npk);
            $('[name="nama"]').val(data.nama_anggota);
            $('[name="status"]').val(data.status_anggota);
            $('[name="alamat"]').val(data.alamat_anggota);
            $('[name="tgl_lahir"]').datepicker('update',data.tanggal_lahir);
            $('[name="tgl_gabung"]').datepicker('update',data.tanggal_gabung);
            $('[name="tgl_keluar"]').datepicker('update',data.tanggal_keluar);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('app/anggota/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        url = "<?php echo site_url('app/anggota/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Anggota Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Anggota Gagal '+ message + ' ( Data Salah atau Kurang Lengkap )');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_person(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('app/anggota/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                alert('Data Anggota Berhasil Dihapus');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH ANGGOTA</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" name="formanggota" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Koperasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="id_kop" placeholder="ID Koperasi" data-validation="required" data-validation-error-msg="ID Koperasi is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NPK</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="npk" placeholder="NPK" data-validation="required" data-validation-error-msg="NPK is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Anggota</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nama" placeholder="Nama Anggota" data-validation="required" data-validation-error-msg="Nama Anggota is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Alamat Anggota</label>
                            <div class="col-md-9">
                                <textarea name="alamat" placeholder="Alamat Anggota" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9">
                                <input name="tgl_lahir" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" data-validation="required" data-validation-error-msg="Tanggal Lahir is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Gabung</label>
                            <div class="col-md-9">
                                <input name="tgl_gabung" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" data-validation="required" data-validation-error-msg="Tanggal Gabung is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Keluar</label>
                            <div class="col-md-9">
                                <input name="tgl_keluar" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select name="status" class="form-control" data-validation="required" data-validation-error-msg="Status is required">
                                    <option value="">--Select Status--</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_upload" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM UPLOAD ANGGOTA</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/app/anggota/upload_data" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Data Anggota (.XLS)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile" placeholder="Data Anggota (HARUS FORMAT .XLS)" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- End Bootstrap modal -->