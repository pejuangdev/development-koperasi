<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>SEGERA GANTI PASSWORD ANDA!</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM GANTI PASSWORD</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/app/anggota/doChangePassword" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Password Lama</label>
                        <input type="text" class="form-control" value="<?php echo $this->session->userdata('password'); ?>" name="lastPassword" placeholder="Password Lama Anda" readonly>
                        <input type="hidden" name="immediately" value="<?php echo $this->session->userdata('immediately'); ?>">
                    </div>
                    <div class="form-group">
                      <label for="">Password Baru</label>
                        <input type="text" class="form-control" name="currentPass" placeholder="Password Baru anda" required>                      
                    </div>
                  </div>
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/app/company" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
