<section class="content-header">
        <h1>
          <b>DATA POTONGAN LAINNYA KOPERASI</b>
        </h1>
        </section>
        <?php 
          $month = $bulan;
          $year = $tahun;

          if(empty($month)&&empty($year)){
            $month = 1;
            $year = 2016;
          }
        ?>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Add Transaksi </button>
            <button class="btn btn-warning" onclick="upload()"><i class="glyphicon glyphicon-upload"></i> Upload Transaksi </button>
            <button class="btn btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
            <button class="btn btn-danger" onclick="document.location.href='<?php echo site_url(); ?>/app/transaksi/remove_all/<?php echo $month."/".$year; ?>'"><i class="glyphicon glyphicon-remove"></i> Delete All ( This Date )</button>
            <form method="get" action="<?php echo site_url(); ?>/app/transaksi">
              <input name="type" value="lainnya" type="hidden">
                  <table>
                    <tr>
                      <td><h4>Tahun </h4></td>
                      <td style="padding:10px;">
                        <select name="tahun" id="tahun" class="form-control" required="require">
                              <?php
                                  for ($i=2009; $i < 2031; $i++) { 
                                    # code...
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                              <?php
                                  }
                              ?>
                          </select> 
                      </td>

                      <td><h4>Bulan </h4></td>
                      <td style="padding:10px;">
                          <select name="bulan" id="bulan" class="form-control" required="require">
                                  <?php for ($i=1; $i <= 12; $i++) { 
                                    
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $this->mod_transaksi->listBulan($i);?></option>
                              <?php
                                  }
                              ?>
                          </select>
                      </td>
                      <td><button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button></td>
                    </tr>
                  </table>
                </form>  
              <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Potongan Lainnya Koperasi ( <?php echo $this->mod_transaksi->listBulan($month) . " - " . $year; ?> )</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>ID KOP</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Date</th>
                          <th>Potongan E-Money</th>
                          <th>Potongan Motor</th>
                          <th>Potongan OR</th>
                          <th>Potongan Lainnya</th>
                          <th style="width:125px;">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>

                  <tfoot>
                  <tr>
                      <th>ID KOP</th>
                      <th>Nama</th>
                      <th>Instalasi</th>
                      <th>Date</th>
                      <th>Potongan E-Money</th>
                      <th>Potongan Motor</th>
                      <th>Potongan OR</th>
                      <th>Potongan Lainnya</th>
                      <th>Action</th>
                  </tr>
                  </tfoot>
              </table>
              </div>
            </div><!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-title" style="margin-left:10px;">
                      <h4>Total Potongan Utama Anggota</h4>
                    </div><!-- /.box-title -->
                    <div class="box-body">
                        <table class="table" style="table-border:5px solid #000;">
                            <tr>
                              <td>Potongan E-Money</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('potongan_emoney'));
                                  ?> 
                              </td>
                            </tr> 

                             <tr>
                              <td>Potongan Motor</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('potongan_motor'));
                                  ?> 
                              </td>
                            </tr> 

                            <tr>
                              <td>Potongan OR</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('potongan_or'));
                                  ?> 
                              </td>
                            </tr>
                            <tr>
                              <td>Potongan Lainnya</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('potongan_other'));
                                  ?> 
                              </td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('app/transaksi/ajax_list_lainnya/'.$month.'/'.$year)?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});



function add_person()
{   
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Transaksi'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Transaksi'); // Set Title to Bootstrap modal title
}

function edit_transaksi(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('app/transaksi/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="npk"').selectpicker('val',data.id_kop);
            $('[name="id_transaksi"').val(data.id_transaksi);
            $('[name="instalasi"]').selectpicker('val',data.id_instalasi);
            $('[name="bulan"]').val(data.bulan_transaksi);
            $('[name="tahun"]').val(data.tahun_transaksi);
            $('[name="sim_jip"]').val(data.simpanan_wajib);
            $('[name="sim_pok"]').val(data.simpanan_pokok);
            $('[name="sim_suk"]').val(data.simpanan_sukarela);
            $('[name="pot_bel"]').val(data.potongan_belanja);
            $('[name="pot_sem"]').val(data.potongan_sembako);
            $('[name="pot_or"]').val(data.potongan_or);
            $('[name="shu"]').val(data.SHU);
            $('[name="icare"]').val(data.potongan_icare);
            $('[name="emoney"]').val(data.potongan_emoney);
            $('[name="motor"]').val(data.potongan_or);
            $('[name="other"]').val(data.potongan_other);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Transaksi'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('app/transaksi/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        url = "<?php echo site_url('app/transaksi/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Transaksi Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Transaksi Gagal '+ message + ' ( Data Salah atau Kurang lengkap ) ');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_transaksi(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('app/transaksi/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                alert('Data Transaksi Berhasil Dihapus');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH TRANSAKSI</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" name="formtransaksi" class="form-horizontal">
                    <input type="hidden" value="" name="id_transaksi"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Anggota</label>
                            <div class="col-md-9">
                                <select name="npk" id="npk" required class="form-control selectpicker" data-live-search="true">
                                    <option value="">Pilih Anggota</option>
                                    <?php foreach ($anggota as $key => $value) {
                                      echo "<option value='". $value->id_kop ."'>". $value->nama_anggota ."</option>";
                                    }?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Instalasi</label>
                            <div class="col-md-9">
                                <select name="instalasi" required class="form-control selectpicker" data-live-search="true">
                                  <option value="">Pilih Instalasi</option>
                                  <?php foreach ($instalasi as $key => $value) {
                                    echo "<option value='". $value->id_instalasi ."'>". $value->nama_instalasi ."</option>";
                                  }?>
                              </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bulan Transaksi</label>
                            <div class="col-md-9">
                                <select name="bulan" required class="form-control" data-validation="required" data-validation-error-msg="Bulan Transaksi is required">
                                    <option value="">Pilih Bulan</option>
                                    <?php for ($i=1; $i <= 12; $i++) { 
                                            
                                          
                                      ?>
                                      <option value="<?php echo $i; ?>" ><?php echo $this->mod_transaksi->listBulan($i);?></option>
                                      <?php
                                          }
                                      ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tahun Transaksi</label>
                            <div class="col-md-9">
                                <select name="tahun" required class="form-control" data-validation="required" data-validation-error-msg="Tahun Transaksi is required">
                                    <option value="">Pilih Tahun</option>
                                    <?php
                                          for ($i=2009; $i < 2031; $i++) { 
                                            # code...
                                          
                                      ?>
                                      <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                                      <?php
                                          }
                                      ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Simpanan Wajib</label>
                            <div class="col-md-9">
                                <input type="text" name="sim_jip" required class="form-control" placeholder="Simpanan Wajib (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Simpanan Pokok</label>
                            <div class="col-md-9">
                                <input type="text" name="sim_pok" class="form-control" placeholder="Simpanan Pokok (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Simpanan Sukarela</label>
                            <div class="col-md-9">
                                <input type="text" name="sim_suk" class="form-control" placeholder="Simpanan Sukarela (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">SHU</label>
                            <div class="col-md-9">
                                <input type="text" name="shu" class="form-control" placeholder="SHU (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan Belanja</label>
                            <div class="col-md-9">
                                <input type="text" name="pot_bel" class="form-control" placeholder="Potongan Belanja (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan Sembako</label>
                            <div class="col-md-9">
                                <input type="text" name="pot_sem" class="form-control" placeholder="Potongan Belanja (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan Handphone</label>
                            <div class="col-md-9">
                                <input type="text" name="icare" class="form-control" placeholder="Potongan Handphone (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan E-Money</label>
                            <div class="col-md-9">
                                <input type="text" name="emoney" class="form-control" placeholder="Potongan E-Money (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan Motor</label>
                            <div class="col-md-9">
                                <input type="text" name="motor" class="form-control" placeholder="Potongan Motor (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan OR</label>
                            <div class="col-md-9">
                                <input type="text" name="pot_or" class="form-control" placeholder="Potongan Belanja (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Potongan Lain - Lain</label>
                            <div class="col-md-9">
                                <input type="text" name="other" class="form-control" placeholder="Potongan Lain - Lain (contoh : 400000)">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_upload" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM UPLOAD TRANSAKSI</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/app/transaksi/upload_data" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Data Transaksi (.XLS)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile" placeholder="Data Anggota (HARUS FORMAT .XLS)" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bulan Transaksi</label>
                            <div class="col-md-9">
                                <select name="bulan" class="form-control" required="require">
                                  <?php
                                      $arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                                      for ($i=1; $i <= 12; $i++) { 
                                        
                                      
                                  ?>
                                  <option value="<?php echo $i; ?>" ><?php echo $arr[$i];?></option>
                                  <?php
                                      }
                                  ?>
                              </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tahun Transaksi</label>
                            <div class="col-md-9">
                                <select name="tahun" class="form-control" required="require">
                                    <?php
                                        for ($i=2009; $i < 2031; $i++) { 
                                          # code...
                                        
                                    ?>
                                    <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- End Bootstrap modal -->