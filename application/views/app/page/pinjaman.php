<?php
  $bulan = 0;
  $tahun = 0;
  $type = $this->input->get('type');

  if($this->input->post()){
    $bulan = $this->input->post('bulan');
    $tahun = $this->input->post('tahun');
  }else{
    $max = $this->mod_transaksi->getMax();
    foreach ($max as $key => $value) {
      $bulan = $value->bulan;
      $tahun = $value->tahun;
    }
  }
?>

<section class="content-header">
        <h1>
          <b>DATA TRANSAKSI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo site_url(); ?>/app/transaksi/add" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH TRANSAKSI </a>
            <a style="margin-bottom:3px;margin-left:10px;" href="<?php echo site_url(); ?>/app/transaksi/upload" class="btn btn-success no-radius dropdown-toggle"><i class="fa fa-plus"></i> UPLOAD DATA TRANSAKSI </a>
            <form method="post" action="<?php echo site_url(); ?>/app/transaksi?type=<?php echo $type; ?>">
                  <table>
                    <tr>
                      <td><h4>Tahun </h4></td>
                      <td style="padding:10px;">
                        <select name="tahun" id="tahun" class="form-control" required="require">
                              <option value="<?php echo $tahun; ?>"><?php echo $tahun; ?></option>
                              <?php
                                  for ($i=2009; $i < 2031; $i++) { 
                                    # code...
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                              <?php
                                  }
                              ?>
                          </select> 
                      </td>

                      <td><h4>Bulan </h4></td>
                      <td style="padding:10px;">
                          <select name="bulan" id="bulan" class="form-control" required="require">
                              
                              <?php
                                  $arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agutus","September","Oktober","November","Desember");
                              ?>
                                  <option value="<?php echo $bulan; ?>"><?php echo $arr[$bulan]; ?></option>
                              <?php for ($i=1; $i <= 12; $i++) { 
                                    
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $arr[$i];?></option>
                              <?php
                                  }
                              ?>
                          </select>
                      </td>
                      <td><button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button></td>
                    </tr>
                  </table>
                </form>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title" style="margin-left:10px;">
                    <h4>Daftar Simpanan Anggota Koperasi</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NPK</th>
                      <th>Nama</th>
                      <th>Instalasi</th>
                      <th>Potongan</th>
                      <th>Simpanan Pokok</th>
                      <th>Simpanan Sukarela</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { 
                      if(empty($value->id_transaksi)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->npk; ?></td>
                      <td><?php echo $value->nama_anggota; ?></td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                      <td><?php echo number_format($value->simpanan_wajib); ?></td>
                      <td><?php echo number_format($value->simpanan_pokok); ?></td>
                      <td><?php echo number_format($value->simpanan_sukarela); ?></td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/app/company/edit/<?php echo $value->id_transaksi; ?>"><i class="fa fa-pencil"></i></a>
                        <a onclick="return confirm('Hapus data?');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/app/company/delete/<?php echo $value->id_transaksi; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php }} ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-title" style="margin-left:10px;">
                      <h4>Total Simpanan Anggota</h4>
                    </div><!-- /.box-title -->
                    <div class="box-body">
                        <table class="table" style="table-border:5px solid #000;">
                            <tr>
                              <td>Simpanan Wajib</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('simpanan_wajib'));
                                  ?> 
                              </td>
                            </tr> 

                             <tr>
                              <td>Simpanan Pokok</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('simpanan_pokok'));
                                  ?> 
                              </td>
                            </tr> 

                            <tr>
                              <td>Simpanan Sukarela</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total('simpanan_sukarela'));
                                  ?> 
                              </td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>
