<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>ADMIN PAGE :: OLSHOP</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <!-- Ionicons 2.0.0 -->
    <!-- <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />-->
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>asset/css/admin/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>asset/css/admin/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
    
    <script src="<?php echo base_url(); ?>asset/js/jquery-1.12.2.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
  
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap-select.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/moment.min.js"></script>

    <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
    <script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
    <script src="<?php echo base_url('asset/js/jquery.form-validator.min.js')?>"></script>

</head>
  <body class="skin-blue">
  <!-- wrapper di bawah footer -->
    <div class="wrapper">
        <?php echo $template['header']; ?>
        <!-- Left side column. contains the logo and sidebar -->
        <!-- sidebar: style can be found in sidebar.less -->
        <?php echo $template['sidebar']; ?>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php echo $template['body']; ?>
      </div><!-- /.content-wrapper -->
         <?php echo $template['footer']; ?>
    </div><!-- ./wrapper -->
  </body>
</html>