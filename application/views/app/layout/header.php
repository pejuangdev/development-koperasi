<header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>dashboard" class="logo"><b>Admin-</b>SIGAP</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="<?php echo site_url(); ?>/app/anggota/forgot_password">
                  <i class="fa fa-envelope-o"></i>
                   <?php 

                   $data = $this->db->query("SELECT COUNT(*) AS forgot FROM tbl_anggota WHERE forgot_password=1")->result();
                   foreach ($data as $value) { ?>
                  <span class="label label-success">

                    <?php echo $value->forgot;
                  } ?></span>
                </a>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <b>Selamat Datang, </b><span class="hidden-xs"><?php echo "ADMIN"; ?></span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
</header>