<aside class="main-sidebar">
  <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url(); ?>asset/images_homepage/Logo.png" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <p><?php echo "APPLIKASI SIMPANAN & POTONGAN KOPERASI SIGAP "; ?></p>

            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MENU</li>
          <li>
            <a href="<?php echo site_url(); ?>/app/instalasi">
              <i class="fa fa-tag"></i> <span>Data Instalasi</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/app/company">
              <i class="fa fa-book" aria-hidden="true"></i> <span>Data Cost Center</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/app/anggota">
              <i class="fa fa-user" aria-hidden="true"></i> <span>Data Anggota</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/app/transaksi">
              <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Data Simpanan</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/app/transaksi?type=potongan">
              <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Data Potongan Utama</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/app/transaksi?type=lainnya">
              <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Data Potongan Lainnya</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/app/login/logout">
              <i class="fa fa-sign-out"></i> <span>Keluar</span>
            </a>
          </li>
          
          <li class="header">Ini merupakan halaman pengelola web</li>
        </ul>
  </section>
</aside>