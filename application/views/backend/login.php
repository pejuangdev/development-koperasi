<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/bootstrap.min.css">

    <!-- CUSTOM CSS -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/admin/custom.css">

    <meta charset="utf-8">
    <title>Flat Login</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->

    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>

    <div class="container">
        <div class="flat-form">
            <div id="login" class="form-action show">
                <div class="login-header">
                    <h1>LOGIN ADMIN</h1>
                </div>
                <form method="POST" action="<?php echo base_url(); ?>index.php/backend/login/proseslog">
                    <ul class="none">
                        <li class="none">
                            <input type="text" name="usernameTxt" placeholder="Username" />
                        </li>
                        <li>
                            <input type="password" name="passTxt" placeholder="Password" />
                        </li>
                        <li>
                            <input type="submit" value="LOGIN" class="button" />
                        </li>
                    </ul>
                </form>
            </div>
            <!--/#login.form-action-->
        </div>
    </div>
</body>
</html>


