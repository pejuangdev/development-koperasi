<aside class="main-sidebar">
  <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url(); ?>asset/images_homepage/Logo.png" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <p><?php echo "KOPERASI SIGAP CMS"; ?></p>

            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MENU</li>
          
          <li>
            <a href="<?php echo site_url(); ?>/backend/usaha">
              <i class="fa fa-tag"></i> <span>Usaha</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/slider">
              <i class="fa fa-photo" aria-hidden="true"></i> <span>Slider Banner</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/news">
              <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>News</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/about">
              <i class="fa fa-share-alt" aria-hidden="true"></i> <span>About Us</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/faq">
              <i class="fa fa-reply-all" aria-hidden="true"></i> <span>FAQ</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/staff">
              <i class="fa fa-users" aria-hidden="true"></i> <span>Staff</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/file">
              <i class="fa fa-upload" aria-hidden="true"></i> <span>File Upload</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/backend/login/logout">
              <i class="fa fa-sign-out"></i> <span>Keluar</span>
            </a>
          </li>
          
          <li class="header">Ini merupakan halaman pengelola web online shop</li>
        </ul>
  </section>
</aside>