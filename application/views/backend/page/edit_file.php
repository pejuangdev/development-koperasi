<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA FILE</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT FILE</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/file/do_edit" method="POST" enctype="multipart/form-data">
                  <?php foreach ($data as $key => $value) { ?>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Judul Slider</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_file; ?>">
                        <input type="text" class="form-control" value="<?php echo $value->nama_file; ?>" id="" name="nama" placeholder="Isikan Nama File" required>
                    </div>
                    <div class="form-group">
                      <label for="">Source File: <span>  <b><?php echo $value->source_file; ?></b></span></label><br>
                        <input type="hidden" name="foto" value="<?php echo $value->source_file; ?>">
                        <input type="file" class="form-control" value="" id="" name="userfile" placeholder="">                        
                    </div> 
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Deskripsi </label>
                      <textarea style="height:100px;" name="deskripsi" class="form-control" placeholder="Deskripsi Banner"><?php echo $value->deskripsi_file; ?></textarea>                 
                    </div>
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/backend/file" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->

                <?php } ?>
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->