<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA BERITA</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH BERITA</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/news/do_edit" method="POST" enctype="multipart/form-data">
                  <?php foreach ($data as $key => $value) { ?>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Judul Berita</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_news; ?>">
                        <input type="text" name="judul" class="form-control" value="<?php echo $value->judul_news; ?>" placeholder="Isikan Judul Berita" required>
                    </div>
                    <div class="form-group">
                      <label for="">Deskripsi Highlight</label>
                        <textarea style="height:100px"  class="form-control" value="" name="highlight" placeholder="Highlight"><?php echo $value->deskripsi_news; ?></textarea>                      
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Gambar Banner</label>
                        <input type="hidden" value="<?php echo $value->image_news; ?>" name="foto">
                        <input type="file" id="" value="" name="userfile" placeholder=""> 
                        <img style="width:80px;height:80px;margin-top:20px;" src="<?php echo base_url(); ?>public/img/news/<?php echo $value->image_news; ?>" alt="User Image" />                       
                    </div>
                    <div class="form-group">
                      <label for="">Status </label>
                        <select name="status" class="form-control">
                          <option value="1">Publish</option>
                          <option value="0">Draft</option>
                        </select> 
                    </div>          
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Konten Berita</label>
                      <textarea name="konten" value="" class="ckeditor" id="text-editor"><?php echo $value->main_news; ?></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script>                
                    </div>
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo site_url(); ?>/backend/news" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
                <?php } ?>
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->