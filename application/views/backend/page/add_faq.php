<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA FAQ</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH FAQ</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/faq/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">TITLE FAQ</label>
                        <input type="text" class="form-control" value="" name="judul" placeholder="Isikan Judul Berita" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Status</label>
                        <select name="status" class="form-control">
                          <option value="1">Publish</option>
                          <option value="0">Hidden</option>
                        </select> 
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Question FAQ</label>
                        <textarea style="height:100px"  class="form-control" name="question" placeholder="Isi Pertanyaan Untuk FAQ disini..."></textarea>                      
                    </div>
                    <div class="form-group">
                      <label for="">Answer FAQ</label>
                        <textarea style="height:100px"  class="form-control" name="answer" placeholder="Isi Jawaban Untuk FAQ disini..."></textarea>                      
                    </div>
                  </div>  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>produk" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->