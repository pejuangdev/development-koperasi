<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA STAFF</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>
        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH STAFF</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/staff/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    
                    <div class="form-group">
                      <label for="">Nama</label>
                        <input type="text" class="form-control" value="" id="" name="nama" placeholder="Isikan Nama Staff" required>
                    </div>
                    <div class="form-group">
                      <label for="">Jabatan Koperasi</label>
                        <input type="text" class="form-control" value="" id="" name="jabatan" placeholder="Isikan Jabatan Staff" required>
                    </div>
                    <div class="form-group">
                      <label for="">Jabatan PT SIGAP</label>
                      <textarea style="height:100px"  class="form-control" name="deskripsi" placeholder="Jabatan di PT SIGAP"></textarea>                      
                    </div> 
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Tahun Bergabung</label>
                      <input type="text" class="form-control" value="" id="" name="email" placeholder="tahun bergabung di koprasi" required>
                    </div>
                    <div class="form-group">
                      <label for="">Foto Staff</label><br>
                        
                        <input type="file" class="form-control" value="" id="" name="userfile" placeholder="">                        
                    </div>
                    <div class="form-group">
                      <label for="">Kategori</label>
                        <select name="status" class="form-control">
                          <option value="1">Pengawas</option>
                          <option value="2">Pengurus</option>
                          <option value="3">Pelaksana</option>
                        </select> 
                    </div> 
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo site_url(); ?>/backend/staff" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->