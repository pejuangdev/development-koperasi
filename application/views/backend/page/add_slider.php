<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA SLIDER</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH SLIDER</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/slider/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Judul Slider</label>
                        <input type="text" class="form-control" value="" id="" name="judul" placeholder="Isikan Judul Slider" >
                    </div>
                    <div class="form-group">
                      <label for="">Gambar Banner<h5>(Ukuran Gambar 1500x640)<h5></label>
                        <input type="file" class="form-control" value="" id="" name="userfile" placeholder="" required>                        
                    </div> 
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Status</label>
                        <select name="status" class="form-control">
                          <option value="1">Fist Image Slider</option>
                          <option value="0">Another Image Slider</option>
                        </select> 
                    </div>
                    <div class="form-group">
                      <label for="">Deskripsi Banner</label>
                      <textarea style="height:100px;" name="deskripsi" class="form-control" placeholder="Deskripsi Banner" required></textarea>                 
                    </div>
                    
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/backend/slider" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->