<section class="content-header">
        <h1>
          <b>DATA PRODUK</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo site_url(); ?>/backend/product/add" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH PRODUK </a>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NAMA PRODUK</th>
                      <th>DESKRIPSI</th>
                      <th>HARGA</th>
                      <th>FOTO</th>
                      <th>CREATE AT</th>
                      <th>STATUS</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->product_name; ?></td>
                      <td>
                        <?php 
                        if(strlen($value->product_description) <= 50) { 
                          echo $value->product_description;
                        } else{
                          echo substr($value->product_description, 0, 50)." ...";
                        }
                        ?>
                        </td>
                      <td><?php echo "Rp. ". number_format($value->price, 2); ?></td>
                      <td>
                        <img style="width:80px;height:80px" src="<?php echo base_url(); ?>asset/la_product/<?php echo $value->product_image; ?>" class="img-circle" alt="User Image" />
                      </td>
                      <td><?php echo $value->create_date; ?></td>
                      <td>
                        <?php if($value->status==1) { ?>
                        <span class="label label-success">Publish</span>
                        <?php }else{ ?>
                          <span class="label label-danger">Draft</span>
                        <?php }?>
                      </td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/backend/product/edit/<?php echo $value->id_product; ?>"><i class="fa fa-pencil"></i></a>
                        <a onclick="return confirm('Hapus data?');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/product/delete/<?php echo $value->id_product; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                  <div class="pagination-area">
                    <ul class="pagination">
                      <?php echo $pages; ?>
                    </ul>
                  </div>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>