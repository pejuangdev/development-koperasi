<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA KATEGORI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT KATEGORI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <?php foreach ($data as $key => $value) { ?>
                  <form role="form" action="<?php echo site_url(); ?>/backend/kategori/do_edit" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Nama Kategori</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_cake_category; ?>">
                        <input type="text" class="form-control" value="<?php echo $value->cake_category_title; ?>" id="" name="judul" placeholder="Isikan Jenis Kategori" required>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Status</label>
                        <select name="status" class="form-control">
                          <option value="1">Publish</option>
                          <option value="0">Hidden</option>
                        </select> 
                    </div>           
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Deskripsi</label>
                      <textarea name="kontenTxt" class="ckeditor" id="text-editor"><?php echo $value->cake_category_description; ?></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script>                
                    </div>
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>produk" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
                <?php } ?>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->