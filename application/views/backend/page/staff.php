<section class="content-header">
        <h1>
          <b>DATA STAFF</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo site_url(); ?>/backend/staff/add" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH STAFF </a>
             <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NAMA</th>
                      <th>JABATAN KOPERASI</th>
                      <th>JABATAN PT SIGAP</th>
                      <th>TAHUN MASUK</th>
                      <th>FOTO</th>
                      <th>KATEGORI</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->nama_staff; ?></td>
                      <td class="text-center"><?php echo $value->jabatan; ?></td>
                      <td class="text-center">
                        <?php 
                        if(strlen($value->deskripsi_staff)<50) {
                          echo $value->deskripsi_staff; 
                        } else{
                          echo substr($value->deskripsi_staff, 0,50)." ...";
                        }?>
                      </td>
                      <td class="text-center"><?php echo $value->email_staff; ?></td>
                      <td>
                        <img style="width:100px;height:100px" src="<?php echo base_url(); ?>public/img/staff/<?php echo $value->foto_staff; ?>" class="img-rounded" alt="User Image" />
                      </td>
                      <td>
                      <?php if($value->status==1) { ?>
                          <span class="label label-success">Pengawas</span>
                        <?php }else if($value->status==2){ ?>
                          <span class="label label-info">Pengurus</span>
                        <?php }else{?>
                          <span class="label label-warning">Pelaksana</span>
                        <?php } ?>
                      </td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/backend/staff/edit/<?php echo $value->id_staff; ?>"><i class="fa fa-pencil"></i></a>
                        <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/staff/delete/<?php echo $value->id_staff; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>