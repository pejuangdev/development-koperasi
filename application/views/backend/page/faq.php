<section class="content-header">
        <h1>
          <b>DATA FAQ</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo site_url(); ?>/backend/faq/add" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH FAQ BARU </a>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>TITLE HEAD</th>
                      <th>QUESTION</th>
                      <th>ANSWER</th>
                      <th>STATUS</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo strtoupper($value->judul); ?></td>
                      <td>
                        <?php 
                        if(strlen($value->pertanyaan)<=30) {
                          echo $value->pertanyaan; 
                        } else{
                          echo substr($value->pertanyaan, 0,30)." ...?";
                        }?>
                      </td> 
                      <td>
                        <?php 
                        if(strlen($value->jawaban)<=30) {
                          echo $value->jawaban; 
                        } else{
                          echo substr($value->jawaban, 0,30)." ...";
                        }?>
                      </td>
                      <td>
                        <?php if($value->status==1) { ?>
                        <span class="label label-success">Publish</span>
                        <?php }else{ ?>
                          <span class="label label-danger">Hidden</span>
                        <?php }?>
                      </td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/backend/faq/edit/<?php echo $value->id_faq; ?>"><i class="fa fa-pencil"></i></a>
                        <a onclick="return confirm('Hapus data ?');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/faq/delete/<?php echo $value->id_faq; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>