<section class="content-header">
        <h1>
          <b>DATA MESSAGE BOX FROM USER</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
             <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>PENGIRIM</th>
                      <th>EMAIL</th>
                      <th>PHONE</th>
                      <th>SUBJECT</th>
                      <th>MESSAGE</th>
                      <th>DATE</th>
                      <th>DETAIL/BALAS</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->nama_inbox; ?></td>
                      <td><?php echo $value->email_inbox; ?></td>
                      <td><?php echo $value->phone_inbox; ?></td>
                      <td><?php echo $value->subject_inbox; ?></td>
                      <td>
                        <?php 
                        if(strlen($value->message_inbox)<50) {
                          echo $value->message_inbox; 
                        } else{
                          echo substr($value->message_inbox, 0,50)." ...";
                        }?>
                      </td>
                      <td><?php echo $value->date_inbox; ?></td>
                      <td>
                        <a class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/contact/detail/<?php echo $value->id_inbox; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>