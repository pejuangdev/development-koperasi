<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA STAFF</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>
        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT STAFF</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                <?php foreach ($data as $value) { ?>
                  <form role="form" action="<?php echo site_url(); ?>/backend/staff/do_edit" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    
                    <div class="form-group">
                      <label for="">Nama</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_staff; ?>">
                        <input type="text" class="form-control" value="<?php echo $value->nama_staff; ?>" id="" name="nama" placeholder="Isikan Nama Staff" required>
                    </div>
                    <div class="form-group">
                      <label for="">Jabatan Koperasi</label>
                        <input type="text" class="form-control" value="<?php echo $value->jabatan; ?>" id="" name="jabatan" placeholder="Isikan Jabatan Staff" required>
                    </div>
                    <div class="form-group">
                      <label for="">Jabatan PT SIGAP</label>
                      <textarea style="height:100px"  class="form-control" name="deskripsi" placeholder="Isi Deskripsi"><?php echo $value->deskripsi_staff; ?></textarea>                      
                    </div> 
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Tahun Bergabung</label>
                      <input type="text" class="form-control" value="<?php echo $value->email_staff; ?>" id="" name="email" placeholder="Isikan Email" required>
                    </div>
                    <div class="form-group">
                      <label for="">Foto Staff</label><br>
                        <img style="height:100px; width: 100px; margin-bottom:15px;" src="<?php echo base_url(); ?>public/img/staff/<?php echo $value->foto_staff; ?>"><br>
                        <input type="hidden" name="foto" value="<?php echo $value->foto_staff; ?>">
                        <input type="file" class="form-control" value="" id="" name="userfile" placeholder="">                        
                    </div>
                    <div class="form-group">
                      <label for="">Kategori</label>
                        <select name="status" class="form-control">
                          <option value="<?php echo $value->status;?>"><?php if($value->status==1){echo "Pengawas";}else if($value->status==2){echo "Pengurus";}else{echo "Pelaksana";} ?></option>
                          <option value="1">Pengawas</option>
                          <option value="2">Pengurus</option>
                          <option value="3">Pelaksana</option>
                        </select> 
                    </div> 
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo site_url(); ?>/backend/staff" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
               <?php } ?>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->