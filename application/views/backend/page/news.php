<section class="content-header">
        <h1>
          <b>DATA BERITA</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo site_url(); ?>/backend/news/add" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH BERITA </a>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>JUDUL</th>
                      <th>HIGHLIGHT</th>
                      <th>KONTEN</th>
                      <th>GAMBAR</th>
                      <th>CREATE AT</th>
                      <th>STATUS</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->judul_news; ?></td>
                      <td>
                        <?php 
                        if (strlen($value->deskripsi_news)>=40){
                          echo substr($value->deskripsi_news, 0, 40)."...";
                        }
                        else{
                          echo $value->deskripsi_news;
                        }  
                        ?>
                      </td>
                      <td>
                        <?php
                        if (strlen($value->main_news)>=40) {
                          echo substr($value->main_news, 0, 40)."...";
                        }
                        else{
                          echo $value->main_news;
                        } ?>
                      <td>
                        <img style="width:80px;height:80px" src="<?php echo base_url(); ?>public/img/news/<?php echo $value->image_news; ?>" class="img-circle" alt="User Image" />
                      </td>
                      <td><?php echo $value->tanggal_news; ?></td>
                      <td><?php if($value->status_news==1) { ?>
                        <span class="label label-success">Publish</span>
                        <?php }else{ ?>
                          <span class="label label-danger">Draft</span>
                        <?php }?>
                      </td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/backend/news/edit/<?php echo $value->id_news; ?>"><i class="fa fa-pencil"></i></a>
                        <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/news/delete/<?php echo $value->id_news; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <div class="pagination-area">
                  <ul class="pagination">
                    <?php echo $pages; ?>
                  </ul>
                </div>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>