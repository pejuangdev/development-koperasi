<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>DETAIL INBOX FROM USER</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">MESSAGE FROM USER</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <div class="col-md-12">
                    <?php foreach ($data as $key => $value) { ?>
                    <table class="table">
                      <tr>
                        <td><h4>From</h4></td>
                        <td><h4>:</h4></td>
                        <td><h4><?php echo $value->nama_inbox; ?></h4></td>
                      </tr>
                      <tr>
                        <td><h4>Email</h4></td>
                        <td><h4>:</h4></td>
                        <td><h4><?php echo $value->email_inbox; ?></h4></td>
                      </tr>
                      <tr>
                        <td><h4>Phone</h4></td>
                        <td><h4>:</h4></td>
                        <td><h4><?php echo $value->phone_inbox; ?></h4></td>
                      </tr>
                      <tr>
                        <td><h4>Subject</h4></td>
                        <td><h4>:</h4></td>
                        <td><h4><?php echo $value->subject_inbox; ?></h4></td>
                      </tr>
                      <tr>
                        <td><h4>Message</h4></td>
                        <td><h4>:</h4></td>
                        <td><h4><?php echo $value->message_inbox; ?></h4></td>
                      </tr>
                      <tr>
                        <td width="100px"><h4>Create At</h4></td>
                        <td><h4>:</h4></td>
                        <td><h4><?php echo $value->date_inbox; ?></h4></td>
                      </tr>
                    </table>

                    <hr style="border-color:#000;">
                    <?php foreach ($outbox as $key => $value_outbox) { if(!empty($value_outbox->id_outbox)){?>
                    
                    <table class="table">
                        <tr>
                            <td>Pesan : </td>
                            <td>Waktu Balas :</td>
                        </tr>
                        <tr>
                            <td><?php echo $value_outbox->outbox; ?></td>
                            <td><?php echo $value_outbox->date; ?></td>
                        </tr> 
                    </table>
                    <?php }} ?>
                    <hr>

                    <form method="POST" action="<?php echo site_url(); ?>/backend/contact/send_mail">
                      <input type="hidden" name="id" value="<?php echo $value->id_inbox; ?>">
                      <input type="hidden" name="email" value="<?php echo $value->email_inbox; ?>">
                      <h3>BALAS INBOX: </h3>
                      <textarea class="ckeditor" id="text-editor" name="message"></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script> 
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                        <a href="<?php echo site_url(); ?>/backend/contact" class="btn btn-warning btn-block btn-flat">Kembali</a>
                      </div><!-- /.col -->
                    </form>
                    <?php } ?>
                  </div>
                  </div>
                </div><!-- /.item -->
              </div><!-- /.chat -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->