<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA PRODUK</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH PRODUK</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/product/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">ID Produk (Harus Angka)</label>
                        <input type="text" class="form-control" value="" id="" name="id" placeholder="Isikan Nomor Identitas Produk" required>
                    </div>
                    <div class="form-group">
                      <label for="">Nama Produk</label>
                        <input type="text" class="form-control" value="" id="" name="nama" placeholder="Isikan Judul Produk" required>
                    </div>

                    <div class="form-group">
                      <label for="">Harga</label>
                        <input type="text" class="form-control" value="" id="" name="harga" placeholder="Harga Produk" required>                        
                    </div>
                    <div class="form-group">
                      <label for="">Kategori</label>
                        <select name="kategori" class="form-control" required>
                          <option>--Pilih Kategori--</option>
                          <?php foreach($data as $row => $value) { ?>
                              <option value="<?php echo $value->id_cake_category; ?>"><?php echo strtoupper($value->cake_category_title); ?></option>
                          <?php } ?>
                        </select> 
                    </div>
                  </div>
                  <div class="col-lg-6">  
                    <div class="form-group">
                      <label for="">Status</label>
                        <select name="status" class="form-control">
                          <option value="1">Publish</option>
                          <option value="2">Draft</option>
                        </select> 
                    </div>
                    <div class="form-group">
                      <label for="">Foto Banner Produk</label>
                        <input type="file" class="form-control" value="" id="" name="userfile[]" placeholder="">                        
                    </div> 
                    <div class="form-group">
                      <label for="">Image Thumbnail 1</label>
                        <input type="file" class="form-control" value="" id="" name="userfile[]" placeholder="">                        
                    </div> 
                    <div class="form-group">
                      <label for="">Image Thumbnail 2</label>
                        <input type="file" class="form-control" value="" id="" name="userfile[]" placeholder="">                        
                    </div>           
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Deskripsi Produk</label>
                      <textarea name="konten" class="ckeditor" id="text-editor"></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script>                
                    </div>
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>produk" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->