<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA USAHA</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH JENIS USAHA</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                <?php foreach ($data as $value) { ?>
                  <form role="form" action="<?php echo site_url(); ?>/backend/usaha/do_edit" method="POST" enctype="multipart/form-data">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Nama Jenis Usaha</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_usaha; ?>">
                        <input type="text" class="form-control" value="<?php echo $value->nama_usaha; ?>" name="nama" placeholder="Isikan Jenis Usaha, Contoh:(Simpan Pinjam)" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Deskripsi Jenis Usaha</label>
                        <textarea style="height:100px"  class="form-control" name="deskripsi" placeholder="Isi Jenis Usaha Disini..."><?php echo $value->deskripsi; ?></textarea>
                    </div>
                  </div>
                    
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/backend/usaha" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
               <?php } ?>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->