<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA ADMIN</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH ADMIN</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/admin/save_data" method="POST" enctype="multipart/form-data">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Nama Admin</label>
                        <input type="text" class="form-control" value="" name="nama" placeholder="Isikan Nama Admin" required>
                    </div>
                    <div class="form-group">
                      <label for="">Email</label>
                        <input type="email" class="form-control" value="" name="email" placeholder="Isikan Email Admin" required>
                    </div>
                    <div class="form-group">
                      <label for="">Telephone</label>
                        <input type="text" class="form-control" value="" name="phone" placeholder="Isikan Judul Berita" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Username</label>
                        <input type="text" class="form-control" value="" name="uname" placeholder="Isikan Username" required>
                    </div>
                    <div class="form-group">
                      <label for="">Password</label>
                        <input type="text" class="form-control" value="" name="password" placeholder="Isikan Password" required>
                    </div>
                    <div class="form-group">
                      <label for="">Status</label>
                        <select name="status" class="form-control">
                          <option value="1">Active</option>
                          <option value="0">Unactive</option>
                        </select> 
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Alamat</label>
                        <textarea style="height:100px"  class="form-control" name="alamat" placeholder="Isi Alamat Disini..."></textarea>                      
                    </div>
                  </div>  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>produk" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->