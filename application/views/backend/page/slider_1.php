<section class="content-header">
        <h1>
          <b>DATA SLIDER</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo site_url(); ?>/backend/slider/add" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH SLIDER </a>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>JUDUL</th>
                      <th>DESKRIPSI</th>
                      <th>IMAGE</th>
                      <th>STATUS</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->judul_slider; ?></td>
                      <td><?php echo $value->deskripsi_slider; ?></td>
                      <td>
                        <img style="width:100px;height:100px" src="<?php echo base_url(); ?>public/img/slider/<?php echo $value->image_slider; ?>" class="img-rounded" alt="User Image" />
                      </td>
                      <td>
                        <?php if($value->status_slider==1) { ?>
                        <span class="label label-success">FIRST SLIDER</span>
                        <?php }else{ ?>
                          <span class="label label-danger">ANOTHER SLIDER</span>
                        <?php }?>
                      </td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/backend/slider/edit/<?php echo $value->id_slider; ?>"><i class="fa fa-pencil"></i></a>
                        <a onclick="return confirm('Hapus data?');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/slider/delete/<?php echo $value->id_slider; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>