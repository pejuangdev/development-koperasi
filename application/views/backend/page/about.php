<section class="content-header">
        <h1>
          <b>DATA ABOUT</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>SEJARAH</th>
                      <th>VISI</th>
                      <th>DESKRIPSI</th>
                      <th>EMAIL</th>
                      <th>TELP</th>
                      <th>ALAMAT</th>
                      <th>LOGO</th>
                      <th>BUNGA</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td>
                        <?php
                        if(strlen($value->sejarah_about) <=40){
                          echo $value->sejarah_about;   
                        } else{
                          echo substr($value->sejarah_about, 0,40)."...";
                        }
                        ?>
                      </td>
                      <td><?php echo $value->visi_about; ?></td>
                      <td>
                        <?php
                        if(strlen($value->deskripsi_about) <=40){
                          echo $value->deskripsi_about;   
                        } else{
                          echo substr($value->deskripsi_about, 0,40)."...";
                        }
                        ?>
                      </td>
                      
                      <td><?php echo $value->email_about; ?></td>
                      <td><?php echo $value->phone_about; ?></td>
                      <td><?php echo $value->address_about; ?></td>
                      <td>
                        <img style="width:100px;height:100px" src="<?php echo base_url(); ?>public/icon/<?php echo $value->logo_about; ?>" class="img-rounded" alt="User Image" />
                      </td>
                      <td><?php echo $value->bunga; ?> %</td>
                      <td>
                        <a class="btn btn-warning btn-sm" href="<?php echo site_url(); ?>/backend/about/edit/<?php echo $value->id_about; ?>"><i class="fa fa-pencil"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>