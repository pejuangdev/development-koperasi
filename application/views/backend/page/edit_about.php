<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA ABOUT</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT ABOUT</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/about/do_edit" method="POST" enctype="multipart/form-data">
                  <?php foreach ($data as $value) { ?>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Visi Perusahaan</label>
                      <textarea name="visi" class="ckeditor" id="text-editor"><?php echo $value->visi_about; ?></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script>                
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Misi Perusahaan</label>
                      <textarea name="misi" class="ckeditor" id="text-editor"><?php echo $value->misi_about; ?></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script>                
                    </div>
                  </div>
                  <!--
                  <div class="col-lg-12">
                    <div class="col-lg-6">
                      <label>Misi Perusahaan</label>
                      <ol>
                        <?php foreach ($misi as $key => $value_misi) { ?>
                        <li>
                          <?php echo $value_misi->deskripsi_misi; ?>
                          <a onclick="return confirm('Hapus data?');" class="btn btn-danger btn-sm" href="<?php echo site_url(); ?>/backend/about/delete/<?php echo $value_misi->id_misi; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </li>
                        <?php } ?>
                      </ol>
                    </div>
                    <div class="col-lg-6">
                      <label>Tambah Misi Perusahaan</label>
                      <form method="post" action="<?php echo base_url(); ?>index.php/backend/about/add_misi">
                        <table class="table">
                          <tr>
                            <td>Deskripsi Misi</td>
                            <td>
                            <input type="hidden" name="id" value="<?php echo $id; ?>">  
                            <input type="text" name="deskripsi" class="form-control" required></td>
                            <td><button class="btn btn-sm btn-success">Tambah</button></td>
                          </tr>
                        </table>
                      </form>
                  </div>
                  </div>-->
                  
                  
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Sejarah</label>
                      <textarea style="height:100px"  class="form-control" name="sejarah" placeholder="Isi Konten Notes"><?php echo $value->sejarah_about; ?></textarea>                      
                    </div>
                    <div class="form-group">
                      <label for="">Email</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_about; ?>">
                        <input type="text" class="form-control" value="<?php echo $value->email_about; ?>" id="" name="email" placeholder="Isikan Email Perusahaan" required>
                    </div>
                    <div class="form-group">
                      <label for="">Logo About</label><br>
                        <img style="height:100px; width: 100px; margin-bottom:15px;" src="<?php echo base_url(); ?>public/icon/<?php echo $value->logo_about; ?>"><br>
                        <input type="hidden" name="foto" value="<?php echo $value->logo_about; ?>">
                        <input type="file" value="" id="" name="userfile" placeholder="">                        
                    </div> 
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Telephone</label>
                      <input type="text" class="form-control" value="<?php echo $value->phone_about; ?>" id="" name="phone" placeholder="Isikan Judul Notes" required>
                    </div>
                    <div class="form-group">
                      <label for="">Fax</label>
                      <input type="text" class="form-control" value="<?php echo $value->fax_about; ?>" id="" name="fax" placeholder="Isikan Judul Notes" required>
                    </div>
                    <div class="form-group">
                      <label for="">Setting Bunga</label>
                      <input type="number" class="form-control" value="<?php echo $value->bunga; ?>" id="" name="bunga" placeholder="Isikan Judul Notes" required>
                    </div>
                    <div class="form-group">
                      <label for="">Alamat Perusahaan</label>
                      <textarea style="height:100px"  class="form-control" name="alamat" placeholder="Isi Konten Notes"><?php echo $value->address_about; ?></textarea>                      
                    </div>
                </div><!-- /.item -->
                <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Deskripsi Singkat Perusahaan</label>
                      <textarea name="desk" class="ckeditor" id="text-editor"><?php echo $value->deskripsi_about; ?></textarea>
                      <script>CKEDITOR.replace('text-ckeditor');</script>                
                    </div>
                  </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo site_url(); ?>/backend/about" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->

                <?php } ?>
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->