<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA SLIDER</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT SLIDER</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo site_url(); ?>/backend/slider/do_edit" method="POST" enctype="multipart/form-data">
                  <?php foreach ($data as $key => $value) { ?>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Judul Slider</label>
                        <input type="hidden" name="id" value="<?php echo $value->id_slider; ?>">
                        <input type="hidden" name="oldStatus" value="<?php echo $value->status_slider; ?>">
                        <input type="text" class="form-control" value="<?php echo $value->judul_slider; ?>" id="" name="judul" placeholder="Isikan Judul Slider">
                    </div>
                    <div class="form-group">
                      <label for="">Foto Banner<h5>(Ukuran gambar 1500x640)</h5></label><br>
                        <img style="height:100px; width: 100px; margin-bottom:15px;" src="<?php echo base_url(); ?>public/img/slider/<?php echo $value->image_slider; ?>"><br>
                        <input type="hidden" name="foto" value="<?php echo $value->image_slider; ?>">
                        <input type="file" class="form-control" value="" id="" name="userfile" placeholder="">                        
                    </div> 
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Status</label>
                        <select name="status" class="form-control">
                          <option value="<?php echo $value->status_slider; ?>"><?php if($value->status_slider==1){echo "Fist Image Slider"; }else{echo "Another Image Slider";} ?></option>
                          <option value="1">Fist Image Slider</option>
                          <option value="0">Another Image Slider</option>
                        </select> 
                    </div>
                    <div class="form-group">
                      <label for="">Deskripsi Banner</label>
                      <textarea style="height:100px;" name="deskripsi" class="form-control" placeholder="Deskripsi Banner"><?php echo $value->deskripsi_slider; ?></textarea>                 
                    </div>
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/backend/slider" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->

                <?php } ?>
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->