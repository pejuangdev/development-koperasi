<section class="content-header">
        <h1>
          <b>DATA ANGSURAN KOPERASI</b>
        </h1>
        </section>
        <?php 
          $month = $bulan;
          $year = $tahun;

          if(empty($month)&&empty($year)){
            $month = 1;
            $year = 2016;
          }
        ?>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-success" onclick="add_angsuran()"><i class="glyphicon glyphicon-plus"></i> Add Angsuran </button>
            <button class="btn btn-warning" onclick="upload()"><i class="glyphicon glyphicon-upload"></i> Upload Angsuran </button>
            <button class="btn btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
            <button class="btn btn-danger" onclick="document.location.href='<?php echo site_url(); ?>/loan/angsuran/remove_all/<?php echo $month."/".$year; ?>'"><i class="glyphicon glyphicon-remove"></i> Delete All ( This Date )</button>
            <form method="get" action="<?php echo site_url(); ?>/loan/angsuran">
                  <table>
                    <tr>
                      <td><h4>Tahun </h4></td>
                      <td style="padding:10px;">
                        <select name="tahun" id="tahun" class="form-control" required="require">
                              <?php
                                  for ($i=2009; $i < 2031; $i++) { 
                                    # code...
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                              <?php
                                  }
                              ?>
                          </select> 
                      </td>

                      <td><h4>Bulan </h4></td>
                      <td style="padding:10px;">
                          <select name="bulan" id="bulan" class="form-control" required="require">
                                  <?php for ($i=1; $i <= 12; $i++) { 
                                    
                                  
                              ?>
                              <option value="<?php echo $i; ?>" ><?php echo $this->mod_angsuran->listBulan($i);?></option>
                              <?php
                                  }
                              ?>
                          </select>
                      </td>
                      <td><button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button></td>
                    </tr>
                  </table>
                </form>  
              <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Angsuran Koperasi ( <?php echo $this->mod_angsuran->listBulan($month) . " - " . $year; ?> )</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>ID Loan</th>
                          <th>NPK</th>
                          <th>Nama</th>
                          <th>Jumlah Cicilan</th>
                          <th>Tanggal Angsuran</th>
                          <th>Keterangan</th>
                          <th style="width:125px;">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>

                  <tfoot>
                  <tr>
                      <th>No</th>
                      <th>ID Loan</th>
                      <th>NPK</th>
                      <th>Nama</th>
                      <th>Jumlah Cicilan</th>
                      <th>Tanggal Angsuran</th>
                      <th>Keterangan</th>
                      <th>Action</th>
                  </tr>
                  </tfoot>
              </table>
              </div>
            </div><!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-title" style="margin-left:10px;">
                      <h4>Total Angsuran Anggota</h4>
                    </div><!-- /.box-title -->
                    <div class="box-body">
                        <table class="table" style="table-border:5px solid #000;">
                            <tr>
                              <td>Jumlah Angsuran</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_angsuran->hitung_total('jumlah_angsuran'));
                                  ?> 
                              </td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('loan/angsuran/ajax_list/'.$month.'/'.$year)?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [0,7] }
        ]

    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});



function add_angsuran()
{   
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Angsuran'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Angsuran'); // Set Title to Bootstrap modal title
}

function edit_angsuran(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('loan/angsuran/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_pinjaman"').selectpicker('val',data.id_pinjaman);
            $('[name="bulan"]').val(data.bulan_angsuran);
            $('[name="tahun"]').val(data.tahun_angsuran);
            $('[name="id_angsuran"]').val(data.id_angsuran);
            $('[name="jumlah"]').val(data.jumlah_angsuran);
            $('[name="keterangan"]').val(data.keterangan);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Angsuran'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('loan/angsuran/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        url = "<?php echo site_url('loan/angsuran/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Angsuran Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Angsuran Gagal '+ message + ' ( Data Salah atau Kurang lengkap ) ');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_angsuran(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('loan/angsuran/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                alert('Data Angsuran Berhasil Dihapus');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH ANGSURAN</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" name="formtransaksi" class="form-horizontal">
                    <input type="hidden" value="" name="id_angsuran"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Pinjaman</label>
                            <div class="col-md-9">
                                <select name="id_pinjaman" id="id_pinjaman" required class="form-control selectpicker" data-live-search="true">
                                    <option value="">Pilih Pinjaman</option>
                                    <?php foreach ($pinjaman as $key => $value) {
                                      echo "<option value='". $value->id_pinjaman ."'>". $value->id_pinjaman . " ( ". $value->nama_anggota . " - ". $value->nama_instalasi . " ) </option>";
                                    }?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jumlah Angsuran</label>
                            <div class="col-md-9">
                                <input type="text" name="jumlah" required class="form-control" placeholder="Jumlah Angsuran (contoh : 400000)" data-validation="required" data-validation-error-msg="Jumlah Angsuran is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bulan Angsuran</label>
                            <div class="col-md-9">
                                <select name="bulan" required class="form-control" data-validation="required" data-validation-error-msg="Bulan Angsuran is required">
                                    <option value="">Pilih Bulan</option>
                                    <?php for ($i=1; $i <= 12; $i++) { 
                                            
                                          
                                      ?>
                                      <option value="<?php echo $i; ?>" ><?php echo $this->mod_angsuran->listBulan($i);?></option>
                                      <?php
                                          }
                                      ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tahun Angsuran</label>
                            <div class="col-md-9">
                                <select name="tahun" required class="form-control" data-validation="required" data-validation-error-msg="Tahun Angsuran is required">
                                    <option value="">Pilih Tahun</option>
                                    <?php
                                          for ($i=2009; $i < 2031; $i++) { 
                                            # code...
                                          
                                      ?>
                                      <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                                      <?php
                                          }
                                      ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                                <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_upload" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM UPLOAD ANGSURAN</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/loan/angsuran/upload_data" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Data Angsuran (.XLS)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile" placeholder="Data Angsuran (HARUS FORMAT .XLS)" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bulan Angsuran</label>
                            <div class="col-md-9">
                                <select name="bulan" class="form-control" required="require">
                                  <?php
                                      $arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                                      for ($i=1; $i <= 12; $i++) { 
                                        
                                      
                                  ?>
                                  <option value="<?php echo $i; ?>" ><?php echo $arr[$i];?></option>
                                  <?php
                                      }
                                  ?>
                              </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tahun Angsuran</label>
                            <div class="col-md-9">
                                <select name="tahun" class="form-control" required="require">
                                    <?php
                                        for ($i=2009; $i < 2031; $i++) { 
                                          # code...
                                        
                                    ?>
                                    <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- End Bootstrap modal -->