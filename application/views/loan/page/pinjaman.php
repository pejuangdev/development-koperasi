<section class="content-header">
        <h1>
          <b>DATA PINJAMAN ANGGOTA KOPERASI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-success" onclick="add_loan()"><i class="glyphicon glyphicon-plus"></i> Add Pinjaman </button>
            <button class="btn btn-warning" onclick="upload()"><i class="glyphicon glyphicon-upload"></i> Upload Pinjaman </button>
            <button class="btn btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Pinjaman</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>ID Loan</th>
                          <th>NPK</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Regular Loan</th>
                          <th>Tenor (bulan)</th>
                          <th>Awal Pinjaman</th>
                          <th>Status</th>
                          <th style="width:125px;">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>

                  <tfoot>
                  <tr>
                      <th>ID Loan</th>
                      <th>NPK</th>
                      <th>Nama</th>
                      <th>Instalasi</th>
                      <th>Regular Loan</th>
                      <th>Tenor (bulan)</th>
                      <th>Awal Pinjaman</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                  </tfoot>
              </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('loan/pinjaman/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [0,8] }
        ]

    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});



function add_loan()
{
    document.formloan.id_pinjaman.disabled=false;
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Pinjaman'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Pinjaman'); // Set Title to Bootstrap modal title
}

function edit_pinjaman(id)
{   
    document.formloan.id_pinjaman.disabled=true;
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('loan/pinjaman/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_pinjaman"]').val(data.id_pinjaman);
            $('[name="npk"]').selectpicker('val',data.id_kop);
            $('[name="instalasi"]').selectpicker('val',data.id_instalasi);
            $('[name="jumlah"]').val(data.jumlah_pinjaman);
            $('[name="tenor"]').val(data.tenor);
            $('[name="bulan"]').val(data.bulan_pinjaman);
            $('[name="tahun"]').val(data.tahun_pinjaman);
            $('[name="status"]').val(data.status_pinjaman);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Pinjaman'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('loan/pinjaman/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        document.formloan.id_pinjaman.disabled=false;
        url = "<?php echo site_url('loan/pinjaman/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Pinjaman Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Pinjaman Gagal '+ message +' ( Data Salah atau Kurang Lengkap )');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_pinjaman(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('loan/pinjaman/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                alert('Data Pinjaman Berhasil Dihapus');
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH PINJAMAN</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" name="formloan" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="id_pinjaman" placeholder="ID Pinjaman" data-validation="required" data-validation-error-msg="ID Pinjaman is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Anggota</label>
                            <div class="col-md-9">
                                <select name="npk" id="npk" required class="form-control selectpicker" data-live-search="true">
                                    <option value="">Pilih Anggota</option>
                                    <?php foreach ($anggota as $key => $value) {
                                      echo "<option value='". $value->id_kop ."'>". $value->nama_anggota ."</option>";
                                    }?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Instalasi</label>
                            <div class="col-md-9">
                                <select name="instalasi" required class="form-control selectpicker" data-live-search="true">
                                  <option value="">Pilih Instalasi</option>
                                  <?php foreach ($instalasi as $key => $value) {
                                    echo "<option value='". $value->id_instalasi ."'>". $value->nama_instalasi ."</option>";
                                  }?>
                              </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jumlah Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="jumlah" placeholder="Jumlah Pinjaman" data-validation="required" data-validation-error-msg="Jumlah Pinjaman is required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lama Pinjaman</label>
                            <div class="col-md-9">
                                <select class="form-control" id="tenor" name="tenor" placeholder="Lama pinjaman" data-validation="required" data-validation-error-msg="Lama Pinjaman is required">
                                    <option value="">Pilih Tenor</option>
                                    <option value="1">1 Bulan</option>
                                    <option value="3">3 Bulan</option>
                                    <option value="6">6 Bulan</option>
                                    <option value="12">12 Bulan</option>
                                    <option value="18">18 Bulan</option>
                                    <option value="24">24 Bulan</option>
                                    <option value="36">36 Bulan</option>
                                    <option value="72">72 Bulan</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bulan Pinjaman</label>
                            <div class="col-md-9">
                                <select name="bulan" required class="form-control" data-validation="required" data-validation-error-msg="Bulan Transaksi is required">
                                    <option value="">Pilih Bulan</option>
                                    <?php for ($i=1; $i <= 12; $i++) { 
                                            
                                          
                                      ?>
                                      <option value="<?php echo $i; ?>" ><?php echo $this->mod_transaksi->listBulan($i);?></option>
                                      <?php
                                          }
                                      ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tahun Pinjaman</label>
                            <div class="col-md-9">
                                <select name="tahun" required class="form-control" data-validation="required" data-validation-error-msg="Tahun Transaksi is required">
                                    <option value="">Pilih Tahun</option>
                                    <?php
                                          for ($i=2009; $i < 2031; $i++) { 
                                            # code...
                                          
                                      ?>
                                      <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                                      <?php
                                          }
                                      ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status Pinjaman</label>
                            <div class="col-md-9">
                                <select class="form-control" name="status" placeholder="Status User" data-validation="required" data-validation-error-msg="Status Pinjaman is required">
                                    <option value="">Pilih Status</option>
                                    <option value="1">Lunas</option>
                                    <option value="2">Pending</option>
                                    <option value="0">Belum Lunas</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_upload" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM UPLOAD PINJAMAN</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/loan/pinjaman/upload_data" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Data Pinjaman (.XLS)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile" placeholder="Data Pinjaman (HARUS FORMAT .XLS)" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->