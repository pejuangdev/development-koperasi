<?php  
    $bunga = $this->mod_archive->getBunga();
?>
<section class="content-header">
        <h1>
          <b>DATA PENGAJUAN APPLIKASI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              <div class="box" style="margin-top:30px;">
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Pengajuan Applikasi</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>ID KOP</th>
                          <th>Nama Anggota</th>
                          <th>Instalasi</th>
                          <th>Jumlah Pengajuan</th>
                          <th>Tenor Pengajuan</th>
                          <th>Waktu Pengajuan</th>
                          <th>Status Applikasi</th>
                          <th style="width:125px;">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>

                  <tfoot>
                  <tr>
                      <th>No</th>
                      <th>ID KOP</th>
                      <th>Nama Anggota</th>
                      <th>Instalasi</th>
                      <th>Jumlah Pengajuan</th>
                      <th>Tenor Pengajuan</th>
                      <th>Waktu Pengajuan</th>
                      <th>Status Applikasi</th>
                      <th>Action</th>
                  </tr>
                  </tfoot>
              </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('loan/appliance/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [0,8] }
        ]

    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});

function edit_cost(id)
{   
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('loan/appliance/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_appliance"]').val(data.id_appliance);
            $('[name="id_kop"]').val(data.id_kop);
            $('[name="npk"]').val(data.npk);
            $('[name="instalasi"]').val(data.nama_instalasi);
            $('[name="pinjaman"]').val(data.value_of);
            $('[name="tenor"]').val(data.time_of);
            $('[name="tenor_baru"]').val(data.waktu_persetujuan);
            $('[name="nilai"]').val(data.nilai_persetujuan);
            $('[name="tgl_cair"]').datepicker('update',data.rencana_pencairan)
            $('[name="nama"]').val(data.nama_menyetujui);
            $('[name="status"]').val(data.status_appliance);
            
            
            
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Persetujuan Pengajuan'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
    $('#btnSave').attr('disabled',false); //set button enable 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('loan/appliance/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        url = "<?php echo site_url('loan/appliance/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Cost Center Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Cost Center Gagal '+ message +' ( Data Salah atau Kurang Lengkap )');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_cost(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('loan/appliance/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                alert('Data Cost Center Berhasil Dihapus');
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH COST CENTER</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/loan/appliance/approve_applikasi" method="post" id="form" name="formcost" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <input type="hidden" value="" name="id_appliance">
                    
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Koperasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="id_kop" placeholder="Kode Cost Center" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NPK</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="npk" placeholder="NPK" disabled>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Instalasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="instalasi" placeholder="NPK" disabled>                      
                             <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Pengajuan Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="pinjaman" disabled name="pinjaman" onChange="calculateBobot()" placeholder="Jumlah Pinjaman contoh : 200000" data-validation="required" data-validation-error-msg="Jumlah Pinjaman is required">                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lama Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="tenor" name="tenor" disabled>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nilai Persetujuan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="nilai" name="nilai" onChange="calculateBobot()" >                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Waktu Persetujuan</label>
                            <div class="col-md-9">
                                <select class="form-control" id="tenor_baru" name="tenor_baru" onChange="calculateBobot()" placeholder="Lama pinjaman" data-validation="required" data-validation-error-msg="Lama Pinjaman is required">
                                    <option value="0">Pilih Tenor</option>
                                    <option value="1">1 Bulan</option>
                                    <option value="3">3 Bulan</option>
                                    <option value="6">6 Bulan</option>
                                    <option value="12">12 Bulan</option>
                                    <option value="18">18 Bulan</option>
                                    <option value="24">24 Bulan</option>
                                    <option value="36">36 Bulan</option>
                                    <option value="72">72 Bulan</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Cicilan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" disabled id="cicilan" name="cicilan" >                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Rencana Pencairan</label>
                            <div class="col-md-9">
                                <input name="tgl_cair" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status Applikasi</label>
                            <div class="col-md-9">
                                <select class="form-control" id="status" name="status" placeholder="Status Applikasi" data-validation="required" data-validation-error-msg="Status Applikasi is required">
                                    <option value="">Pilih Status</option>
                                    <option value="0">Proses</option>
                                    <option value="1">Diterima</option>
                                    <option value="2">Ditolak</option>
                                    <option value="3">Kurang Berkas</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Yang Menyetujui</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="nama" name="nama" >                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" name="btnSave" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script type="text/javascript">
function calculateBobot(){
    var bobotCustomString = $('#nilai').val();
    var tenor = $('#tenor_baru').val();
    if(tenor == 0){
      tenor = 1;
    }
    var tenorCustom = Number(tenor);
    var bobotCustom = Number(bobotCustomString);

    var hasil = (bobotCustom+(bobotCustom*tenorCustom/12*<?php echo $bunga; ?>/100))/tenorCustom;
    
    var bobotValue = 0;
    if(bobotCustom != ""){
        $('#cicilan').val(hasil.toFixed(2));
    }
    else{
        $('#nilai').val((bobotValue*100).toFixed(2));
    }
}

</script>