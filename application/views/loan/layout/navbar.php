 <!--                           part of navbar                       -->
    <!--  nav1 -->
     <nav class="navbar navbar-default background-biru navbar1">
     <img src="<?php echo base_url(); ?>asset/images_homepage/border_menu.png" class="img-responsive">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header ">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav ">
            <li class="li-custom about-us-custom"><a href="<?php echo base_url(); ?>index.php/controller_aboutus" target="_blank">ABOUT US</a></li>
            <li class="dropdown li-custom">
              <a href="<?php echo base_url(); ?>index.php/controller_product" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PRODUCTS <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>index.php/controller_product/detailproduct" class="dropdown-attribute" target="_blank">CAKE</a></li>
                <li><a href="#" class="dropdown-attribute" target="_blank">CUP</a></li>
                <li><a href="#" class="dropdown-attribute" target="_blank">PIE</a></li>
               
              </ul>
            </li>
             <li class="li-custom"><a href="<?php echo base_url(); ?>index.php/controller_order" target="_blank">ORDER</a></li>
             <li class="li-custom" ><a href="<?php echo base_url(); ?>index.php/controller_news" target="_blank">NEWS</a></li>
             <li class="li-custom"><a href="<?php echo base_url(); ?>index.php/controller_faq" target="_blank">FAQS</a></li>
             <li class="li-custom"><a href="<?php echo base_url(); ?>index.php/controller_contactus" target="_blank">CONTACT US</a></li>
          </ul>

           <ul class="nav navbar-nav navbar-right">
            <li><a href="#" class="listitem"><span class="fa fa-shopping-bag "></span></a></li>
            
          </ul><!-- nav navbar-nav navbar-right -->
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
      <img src="<?php echo base_url(); ?>asset/images_homepage/border_menu.png" class="img-responsive">
    </nav>
    <!-- nav1 -->

    <div class="nav2 visible-xs">
      <div class="row custom-row">
        <div class="col-xs-4">
          <!-- <span class="fa fa-trash"></span> -->
          <button type="button" class="icon-navbar-2 navbar-bars" >
                 
                      <span class="fa fa-bars "></span>
                 
              </button>
        </div><!-- col-xs-4 -->
        <div class="col-xs-4">
           <img src="<?php echo base_url(); ?>asset/images_homepage/Logo.png" class="img-responsive logo" />  
        </div><!-- col-xs-4 -->
        <div class="col-xs-4">
        <!--  <span class="fa fa-shopping-bag pull-right"></span> -->
            <button type="button"  class="pull-right icon-navbar-2 listitem" >
                   
                      <span class="fa fa-shopping-bag "></span>
                 
              </button>
        </div><!-- col-xs-4 -->
      </div><!-- row custom-row -->
    </div> <!-- nav2 -->

    <div class="navbar-responsive">
        <div class="row custom-row">
          <div class="close2 pull-right">
            <a href="" >
              <span style="color: #000000;" class="fa fa-times"></span>
            </a>
          </div><!-- close2 -->
          <div class="navbar-responsive-content">
            <a href="<?php echo base_url(); ?>index.php/controller_aboutus" target="_blank"><p>About Us</p></a>
            <div class="border-bottom"></div>
            <a href="<?php echo base_url(); ?>index.php/controller_product" target="_blank"><p>Product</p></a>
            <div class="border-bottom"></div>
            <a href="<?php echo base_url(); ?>index.php/controller_order" target="_blank"><p>Order</p></a>
            <div class="border-bottom"></div>
            <a href="<?php echo base_url(); ?>index.php/controller_news" target="_blank"><p>News</p></a>
            <div class="border-bottom"></div>
            <a href="<?php echo base_url(); ?>index.php/controller_faq" target="_blank"><p>FAQS</p></a>
            <div class="border-bottom"></div>
            <a href="<?php echo base_url(); ?>index.php/controller_contactus" target="_blank"><p>Contact Us</p></a>
            
          </div><!-- navbar-responsive-content -->
          
        </div> <!-- row custom-row -->
    </div><!-- navbar-resposive -->
      

     <div class="basket-resposive">
      <div class="close1">
        <a href="" >
          <span style="color: #000000;" class="fa fa-times"></span>
        </a>
      </div><!-- close1 -->

     

      <div class="row custom-row">
        <div class="col-xs-12">
          
          <div class="table-scrool" style="">
            <table class="table " >
            <tr>
              <th style="width: 1%">No</th>
              <th style="width: 1%">Gambar</th>
              <th style="width: 33%">item</th>
              <th style="width: 0%">harga</th>
            </tr>
            <tr>
              <td style="">1</td>
              <td style="">
                <img src="<?php echo base_url(); ?>asset/detail_product/foto1.png" class="img-responsive image-custom" />
              </td>
              <td style="">
                <input type="submit" value="-" name="min" />
                <input type="submit" value="1" name="angka" style="background-color: #FFFFFF;border:1px solid #FFFFFF" />
                <input type="submit" value="+" name="plus" />
                &nbsp;
                <a href=""><span style="color: #000000" class="fa fa-trash"></span></a>
              </td>
              <td style="">
                 13000000
              </td>
            </tr>
            <tr>
              <td style="">2</td>
              <td style="">
                <img src="<?php echo base_url(); ?>asset/detail_product/foto1.png" class="img-responsive image-custom" />
              </td>
              <td style="">
                <input type="submit" value="-" name="min" />
                <input type="submit" value="1" name="angka" style="background-color: #FFFFFF;border:1px solid #FFFFFF" />
                <input type="submit" value="+" name="plus" />
                &nbsp;
                <a href=""><span style="color: #000000" class="fa fa-trash"></span></a>
              </td>
              <td style="">
                13000000
              </td>
            </tr>
            <tr>
              <td style="">3</td>
              <td style="">
                <img src="<?php echo base_url(); ?>asset/detail_product/foto1.png" class="img-responsive image-custom" />
              </td>
              <td style="">
                <input type="submit" value="-" name="min" />
                <input type="submit" value="1" name="angka" style="background-color: #FFFFFF;border:1px solid #FFFFFF" />
                <input type="submit" value="+" name="plus" />
                &nbsp;
                <a href=""><span style="color: #000000" class="fa fa-trash"></span></a>
              </td>
              <td style="">
                13000000
              </td>
            </tr>
            <tr>
              <td style="">4</td>
              <td style="">
                <img src="<?php echo base_url(); ?>asset/detail_product/foto1.png" class="img-responsive image-custom" />
              </td>
              <td style="">
                <input type="submit" value="-" name="min" />
                <input type="submit" value="1" name="angka" style="background-color: #FFFFFF;border:1px solid #FFFFFF" />
                <input type="submit" value="+" name="plus" />
                &nbsp;
                <a href=""><span style="color: #000000" class="fa fa-trash"></span></a>
              </td>
              <td style="">
                13000000
              </td>
            </tr>
            <tr>
              <td style="">5</td>
              <td style="">
                <img src="<?php echo base_url(); ?>asset/detail_product/foto1.png" class="img-responsive image-custom" />
              </td>
              <td style="">
                <input type="submit" value="-" name="min" />
                <input type="submit" value="1" name="angka" style="background-color: #FFFFFF;border:1px solid #FFFFFF" />
                <input type="submit" value="+" name="plus" />
                &nbsp;
                <a href=""><span style="color: #000000" class="fa fa-trash"></span></a>
              </td>
              <td style="">
                13000000
              </td>
            </tr>
          </table>
          </div>
        
        </div><!-- col-xs-12 -->
      </div> <!-- row custom-row -->



      <div class="subtotal">
        <div class="row custom-row">
          <div class="col-xs-12">
            <p style="font-family: Felix Tettling;font-size: 16px">
              <span class="subtotal2">Subtotal</span>
              <span class="subtotal3">Rp 0,00</span>
            </p>
            <!-- <input type="submit" value="checkout" class="btn btn-default btn-checkout" /> -->
          </div><!-- col-xs-12 -->
          
        </div><!-- row custom-row -->
      </div><!-- subtotal -->

      <div class="deliver-date">
        <div class="row custom-row">
          <div class="col-xs-12">
            <p>DELIVER DATE</p>
            <select name="" class="deliver-date-select">
              <option value="">May - 16, 2015</option>
              <option value="">May - 17, 2015</option>
              <option value="">May - 18, 2015</option>
              <option value="">May - 19, 2015</option>
            </select>
          </div><!-- col-xs-12 -->
        </div><!-- row custom-row -->
      </div><!-- deliver-date -->

      <div class="choose-area">
        <div class="row custom-row">
          <div class="col-xs-12">
            <p>AREA</p>
            <div>
              <select name="" class="choose-area-select">
                <option value="">-- Choose Area --</option>
                <option value="">Jakarta Selatan</option>
                <option value="">Jakarta Utara</option>
                <option value="">Jakarta Timur</option>
              </select>
              <span class="area-cost">Rp 0,00</span>
            </div>
            
          </div><!-- col-xs-12 -->
        </div><!-- row custom-row -->
      </div><!-- chose-area -->

      <div class="please-send-to">
        <div class="row custom-row">
          <div class="col-xs-12">
            <p>Please send to :</p>
          <div class="checkbox">
            <label><input type="checkbox" value="">
              I want to send it to my self
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" value="">I want to send it to another person
            </label>
          </div>

          </div>
        </div>
      </div>

      <div class="total">
        <div class="row custom-row">
          <div class="col-xs-12">
            <p style="font-family: Felix Tettling;font-size: 16px">
              <span class="total1">Total</span>
              <span class="total2">Rp 0,00</span>
            </p>
            <!-- <input type="submit" value="checkout" class="btn btn-default btn-checkout" /> -->
          </div><!-- col-xs-12 -->
          
        </div><!-- row custom-row -->
      </div><!-- total -->

      <div class="subtotal">
        <div class="row custom-row">
          <div class="col-xs-12">
            <input type="submit" value="checkout" class="btn btn-default btn-checkout" />
          </div><!-- col-xs-12 -->
          
        </div><!-- row custom-row -->
      </div><!-- subtotal -->
      
     </div><!-- basket-resposive -->

      <!--                           part of navbar                       -->
