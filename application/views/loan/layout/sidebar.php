<aside class="main-sidebar">
  <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url(); ?>asset/images_homepage/Logo.png" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <p><?php echo "APPLIKASI PINJAMAN KOPERASI SIGAP "; ?></p>

            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MENU</li>
          <li>
            <a href="<?php echo site_url(); ?>/loan/pinjaman">
              <i class="fa fa-tag"></i> <span>Data Pinjaman</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/loan/angsuran">
              <i class="fa fa-book" aria-hidden="true"></i> <span>Data Angsuran</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/loan/appliance">
              <i class="fa fa-list" aria-hidden="true"></i> <span>Data Pengajuan Applikasi</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url(); ?>/loan/login/logout">
              <i class="fa fa-sign-out"></i> <span>Keluar</span>
            </a>
          </li>
          
          <li class="header">Ini merupakan halaman pengelola web</li>
        </ul>
  </section>
</aside>