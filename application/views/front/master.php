<html lang="en">
<head>
	<title>Koperasi Sigap</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-site-verification" content="7Prbkjoa8A9ghAfGN7Q5EYk23Y-54D0hlrXy05zGn1E" />
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link href="<?php echo base_url();?>public/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>public/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/css/animate.css">
	<link href="<?php echo base_url();?>public/css/animate.min.css" rel="stylesheet"> 
	<link href="<?php echo base_url();?>public/css/style.css" rel="stylesheet" />	
	<link href="<?php echo base_url();?>public/css/custom.css" rel="stylesheet">
	<link rel="icon" type="ico" href="<?php echo base_url();?>public/icon/icon.jpg"> 
</head>
<body>
	<?php
		echo $data["header"];
	?>
	<?php
		echo $data["body"];
	?>
	<?php
		echo $data["footer"];
	?>
</body>
	<script src="<?php echo base_url();?>public/js/jquery.js"></script>		 
    <script src="<?php echo base_url();?>public/js/bootstrap.min.js"></script>	
	<script src="<?php echo base_url();?>public/js/wow.min.js"></script>
	<script>
	wow = new WOW(
	 {
	
		}	) 
		.init();
	</script>	
</html>