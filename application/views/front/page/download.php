<section id="about-us">
    <div class="container" style="margin-top:50px;margin-bottom:100px;">
      <div class="text-center wow fadeInDown">
        <h2>Unduh</h2>
        <p>Berikut file yang telah di upload oleh admin Koperasi PT.SIGAP.</p>
      </div>
      <div class="table-responsive">
      <table class="table table-bordered">
          <thead style="color:#2d2d2d">
            <tr>
              <th>Judul File</th>
              <th>Deskripsi File</th>
              <th>Tanggal Upload</th>
              <th>Download</th>
            </tr>
          </thead>

          <tbody style="color:#2d2d2d">
            <?php
                foreach ($download as $key => $value) {
                  # code...
                
            ?>
            <tr>
              <td><?php echo $value->nama_file;?></td>
              <td><?php echo $value->deskripsi_file;?></td>
              <td><?php echo $value->tanggal_file;?></td>
              <td><a class="btn btn-danger" href="<?php echo base_url(); ?>public/file/<?php echo $value->source_file; ?>"> Download</td>
            </tr>
            <?php
                }
            ?> 
          </tbody>
        </table>
    </div>
    </div>
  </div> 
