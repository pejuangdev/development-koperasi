<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
	width: 100%;
	left: 0px;
	right: 0px;
	bottom: 0px;
	text-align: left;
	padding-top: 5px;
	padding-left: 15%;
	padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
	margin-top: 10px;
	font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
	.transition-timer-carousel .carousel-caption .carousel-caption-header {
		font-size: 36px;
	}
}
.transition-timer-carousel .carousel-indicators {
	bottom: 0px;
	margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
	z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
	-moz-transition: width 4.25s linear;
	-o-transition: width 4.25s linear;
	transition: width 4.25s linear;
}


.carousel-caption{
	margin-top: 20%;
}
</style>

<div class="slider">		
		<div id="about-slider">
			<div id="carousel-slider" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators visible-xs">
					<?php for($i=0;$i<$jumlah;$i++){ if($i==0){ ?>
					<li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
					<?php }else{ ?>
					<li data-target="#carousel-slider" data-slide-to="<?php $i; ?>"></li>
					<?php }} ?>
				</ol>

				<div class="carousel-inner">
					<?php foreach ($slider as $key_s => $value_slider) { if($value_slider->status_slider==1){?>
						
					<div class="item active">						
						<img src="<?php echo base_url();?>/public/img/slider/<?php echo $value_slider->image_slider; ?>" class="img-responsive" alt=""> 
						<div class="carousel-caption">
							<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">								
								<h2><span><?php if(!empty($value_slider->judul_slider)){ echo $value_slider->judul_slider;} ?></span></h2>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">								<?php if(!empty($value_slider->deskripsi_slider)){ ?>
									<p><?php echo $value_slider->deskripsi_slider; ?></p>
								<?php } ?>
                                                                </div>
							</div>
							
						</div>
				    </div>
					<?php }else{ ?>
				    <div class="item">
						<img src="<?php echo base_url();?>/public/img/slider/<?php echo $value_slider->image_slider; ?>" class="img-responsive" alt=""> 
						<div class="carousel-caption">
							<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="1.0s">								
								<h2><?php echo $value_slider->judul_slider;?></h2>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">								<?php if(!empty($value_slider->deskripsi_slider)){ ?>
									<?php echo "<p>".$value_slider->deskripsi_slider."</p>";?>
                                                                        <?php } ?>
								</div>
							</div>
							
						</div>
				    </div>
				    <?php }} ?>
					
				    <!-- 
				    <div class="item">
						<img src="<?php echo base_url();?>/public/img/slider/15.jpg" class="img-responsive" alt=""> 
						<div class="carousel-caption">
							<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">								
								<h2>Modern Design</h2>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">								
									<p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
								</div>
							</div>
							<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.9s">								
								<form class="form-inline">
									<div class="form-group">
										<button type="livedemo" name="purchase" class="btn btn-primary btn-lg" required="required">Live Demo</button>
									</div>
									<div class="form-group">
										<button type="getnow" name="subscribe" class="btn btn-primary btn-lg" required="required">Get Now</button>
									</div>
								</form>
							</div>
						</div>
				    </div> --> 
				</div>
				
				<a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
					<i class="fa fa-angle-left"></i> 
				</a>
				
				<a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
					<i class="fa fa-angle-right"></i> 
				</a>
			</div> <!--/#carousel-slider-->
		</div><!--/#about-slider-->
	</div><!--/#slider-->
