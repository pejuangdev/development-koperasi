<div class="gallery">
		<div class="text-center">
			<h2>Berita</h2>
			<p>Kegiatan - kegiatan dan berita tentang Koperasi Sigap</p>
		</div>
		<div class="container">
			<?php foreach ($query as $value) { ?>
			<div class="col-md-4">
				<a href="<?php echo base_url();?>index.php/news/detail/<?php echo $value->id_news; ?>">
				<figure class="effect-marley" style="height:330px">
					<img src="<?php echo base_url(); ?>public/img/news/<?php echo $value->image_news; ?>" style="height:100%;" width="100%" class="img-responsive"/>
					<figcaption>
						<h4><?php echo $value->judul_news; ?></h4>
						<strong><p><?php echo $value->deskripsi_news; ?></p></strong>				
					</figcaption>			
				</figure>
				</a>
			</div>
			<?php } ?>		
			
		</div>
		<div style="margin-top:50px;"class="text-center"><?php echo $halaman; ?></div>
</div>	