<section id="about-us">
	<?php foreach ($about as $key => $value_about) { ?>
	<div class="container">
		<div class="visi">
			<div class="text-center wow fadeInDown">
				<h2 style="margin-bottom:10px;">Visi</h2>
				<p style="margin-bottom:10px;font-size:13px;"><?php echo $value_about->visi_about; ?></p>
				<h2 style="margin-bottom:10px;">Misi</h2>
				<ol>
				<?php foreach ($misi as $key => $value_misi) {
					echo "<li style='color:#aaa;font-size:18px;'>".$value_misi->deskripsi_misi."</li>";
				} ?>
				</ol>
				<hr style="border-color:rgb(102, 169, 212);">
			</div>
		</div>
	</div>

	<div class="container">
		<div class="team">
			<div class="text-center wow fadeInDown">
				<h2>Sejarah Koperasi</h2>
				<p><?php echo $value_about->sejarah_about; ?></p>
				<hr style="border-color:rgb(102, 169, 212);">
			</div>
		</div>
	</div>

	<?php } ?>

	<div class="container">
		<div class="row">

		<div class="text-center wow fadeInDown team">
			<h2 style="">Jenis Usaha</h2>
		</div>
		<?php foreach ($jenis_usaha as $key => $value_usaha) { ?>
		
		<div class="col-md-3 wow fadeInDown text-center" data-wow-duration="1000ms" data-wow-delay="600ms">
			<i style="margin-top: 20px;margin-bottom: 10px;font-size: 50px;color: #ddd;" class="<?php echo $value_usaha->icon; ?>"></i>
			<h3><?php echo $value_usaha->nama_usaha; ?></h3>
			<p>
				<?php echo $value_usaha->deskripsi; ?>	
			</p>
		</div>

		<?php } ?>

		</div>
		<hr style="border-color:rgb(102, 169, 212);">
	</div>

    <div class="container">		
		<div class="team">
			<div class="text-center wow fadeInDown">
				<h2>Staff</h2>
				<p>Berikut adalah anggota yang bergabung dengan kami.</p>
			</div>

			<div class="row clearfix">
				<?php
					$bagi = $nr/2;
					$limit = 0;
					$offset = 2; 
					$tamp = 0;

					for ($i=0; $i <= $bagi; $i++) {  
 
						$team = $this->mod_team->getTeamPage($limit,$offset)->result();
						$j = 0;
						foreach ($team as $key => $value) { 
							if($i%2==0){ 
								$tamp += 1;	


				?>

				<div class="col-md-4 col-sm-6 <?php if($j%2==1){echo "col-md-offset-2";} ?>">	
					<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="<?php echo base_url();?>public/img/staff/<?php echo $value->foto_staff;?>" style="width:145px;height:137px;" width="145px" height="137px" alt=""></a>
							</div>


							<div class="media-body">
								
								<h4><?php echo $value->nama_staff;?></h4>
								<h5>Jabatan di Koperasi : <strong><?php echo $value->jabatan;?></strong></h5>
								<h5>Jabatan di PT.SIGAP : <strong><?php echo $value->deskripsi_staff;?></strong></h5>
								<h5>Sejak : <strong><?php echo $value->email_staff; ?></strong></h5>
							</div>
						</div><!--/.media -->
					</div>
				</div><!--/.col-lg-4 -->
				<?php
								$j++;
							}else{

								$tamp += 1;
				?> 

				<div class="col-md-4 col-sm-6 col-md-offset-2">
					<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="<?php echo base_url();?>public/img/staff/<?php echo $value->foto_staff;?>" style="max-width:145px;max-height:137px;" alt=""></a>
							</div>
							<div class="media-body">
								<h4><?php echo $value->nama_staff;?></h4>
								<h5>Jabatan di Koperasi : <strong><?php echo $value->jabatan;?></strong></h5>
								<h5>Jabatan di PT.SIGAP : <strong><?php echo $value->deskripsi_staff;?></strong></h5>
								<h5>Sejak : <strong><?php echo $value->email_staff; ?></strong></h5>
							</div>
						</div>
					</div>
				</div>
				<?php
						}  
					}

				?> 
			</div> <!--/.row -->


			<?php
				if($bagi!=$i){
			?>
			<div class="row team-bar">
				<div class="first-one-arrow hidden-xs">
					<hr>
				</div>
				<div class="first-arrow hidden-xs">
					<hr> <i class="fa fa-angle-up"></i>
				</div>
				<div class="second-arrow hidden-xs">
					<hr> <i class="fa fa-angle-down"></i>
				</div>
				<div class="third-arrow hidden-xs">
					<hr> <i class="fa fa-angle-up"></i>
				</div>
				<div class="fourth-arrow hidden-xs">
					<hr> <i class="fa fa-angle-down"></i>
				</div>
			</div> <!--skill_border-->    


			<?php
				}
					$limit = $tamp;   
				} 
			?>  

			<!-- <div class="row clearfix">   
				<div class="col-md-4 col-sm-6 col-md-offset-2">	
					<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="img/man3.jpg" alt=""></a>
							</div>

							<div class="media-body">
								<h4>Jhon Doe</h4>
								<h5>Founder and CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Web</a></li>
									<li class="btn"><a href="#">Ui</a></li>
									<li class="btn"><a href="#">Ux</a></li>
									<li class="btn"><a href="#">Photoshop</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-md-offset-2">
					<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="img/man4.jpg" alt=""></a>
							</div>
							<div class="media-body">
								<h4>Jhon Doe</h4>
								<h5>Founder and CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Web</a></li>
									<li class="btn"><a href="#">Ui</a></li>
									<li class="btn"><a href="#">Ux</a></li>
									<li class="btn"><a href="#">Photoshop</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
					</div>
				</div>
			</div> -->	<!--/.row-->
		</div><!--section-->
	</div>
</div>