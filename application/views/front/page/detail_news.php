<?php foreach ($detail_news as $key => $value) { ?>

<div class="container">
	<div class="col-md-12" style="margin-top:50px;">
		<div class="col-md-8" >
			<div class="row" >
				<img src="<?php echo base_url(); ?>public/img/news/<?php echo $value->image_news;?>" width="100%" height="300px">
			</div>
			<div class="row">
				<h3 class="page-header"><strong><?php echo $value->judul_news; ?></strong></h3>
				<h5 class="text-left"><i class="fa fa-calendar"> Date : &nbsp;<?php echo $value->tanggal_news; ?></i> &nbsp;&nbsp; <i class="fa fa-pencil"> Created By : &nbsp;<?php echo $value->create_news; ?></i></h5>
			</div>
			<div class="row" style="color:rgb(29, 40, 48);margin-bottom:100px;">
				<?php echo $value->main_news; ?>
			</div>	
		</div>
		<div class="col-md-4" style="margin-bottom:100px;">
			<h4 class="page-header" style="margin-top:-5px;padding:0px 10px;"><strong>Berita Terbaru</strong></h3>
			<?php foreach ($recent_news as $key_z => $value_recent) { ?>
			<div class="row" style="padding: 10px 20px;">
				<div style="float:left;margin-right:18px;">
					<img src="<?php echo base_url();?>public/img/news/<?php echo $value_recent->image_news;?>" width="80" height="80">
				</div>
				<div style="overflow:hidden;">
					<a href="<?php echo base_url();?>index.php/news/detail/<?php echo $value_recent->id_news; ?>" ><h5 class="text-justify" style="color:rgb(67, 135, 167);"><?php echo $value_recent->judul_news; ?><h5></a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php } ?>
