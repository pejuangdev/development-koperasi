    <div class="gallery">
            <div class="text-center">        
                <h2>Frequently Ask Question</h2>
                <p>Seputar pertanyaan tentang koperasi SIGAP</p>
            </div> 
            <div class="row contact-wrap"> 
                <div class="container">
                <?php $i=1; foreach ($faq as $value) {?>
                	<h4><strong><span><?php echo $i.". ".$value->judul; ?></strong> </h4>
                	<h5>Question: <span><?php echo nl2br($value->pertanyaan); ?></span></h5>
                	<h5>Answer: <span><?php echo nl2br($value->jawaban); ?></h5>
                	<hr style="border-color:rgb(102, 169, 212);">
                <?php $i++;} ?>
                </div>
            </div><!--/.row-->
        </div><!--/.container-->
