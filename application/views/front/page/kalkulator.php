<div class="gallery">
		<div class="text-center">
			<h2>Kalkulator</h2>
			<p>Menghitung cicilan dan pendapatan minimal</p>
		</div>
<?php foreach ($about as $key_about => $value_about) { ?>
<section id="result" class="action" style="margin-bottom:20px;">
	<div class="container">
<div class="row">
		<div class="col-md-6" style="background-color:#f2f2f2;height:400px">
			<h4 class="page-header"><strong>Kalkulator Pinjaman</strong></h4>
			<p class="text-justify"style="font-size:18px;"><em>Kalkulator pinjaman digunakan untuk
				menghitung jumlah cicilan dan pendapatan minimal dalam melakukan pinjaman pada koperasi. Setiap calon peminjam dapat menghitung besarnya cicilan berdasarkan jumlah pinjaman , tenor pinjaman dan 
				bunga flat sesuai ketentuan koperasi. Kalkulator ini menggunakan asumsi perhitungan bunga flat sebesar <?php echo $value_about->bunga; ?> % .</em></p>
		</div>
		<div class="col-md-6" style="background-color:rgb(180, 223, 246);height:400px">
			<h4 class="page-header"><strong><?php echo strtoupper("Hitung Pinjaman"); ?></strong></h4>
			<form method="post" action="<?php echo site_url(); ?>/kalkulator/hitung">
				<table style="color:000;">
					<tr>
						<td style="padding:10px;"><em>Jumlah Pinjaman<em></td>
						<td><input class="form-control" required type="text" name="pinjaman" value="<?php if(!empty($pinjaman)){ echo $pinjaman;} ?>"></td>
					</tr>
					<tr>
						<td style="padding:10px;"><em>Lama Pinjaman<em></td>
						<td>
							<select name="tenor" required class="form-control">
								<?php if(!empty($tenor)){ echo "<option value='".$tenor."'>".$tenor." bulan</option>";} ?>
								<option value="1">1 Bulan</option>
								<option value="3">3 Bulan</option>
								<option value="6">6 Bulan</option>
								<option value="12">12 Bulan</option>
								<option value="18">18 Bulan</option>
								<option value="24">24 Bulan</option>
								<option value="36">36 Bulan</option>
								<option value="72">72 Bulan</option>
							</select>
						</td>
					</tr>
					<tr>
						<td style="padding:10px;"><input type="submit" class="btn btn-danger" name="hitung" value="hitung"></td>
					</tr>
					<?php if(!empty($hasil)){ ?> 
					<tr>
						<td style="padding:10px;"><em>Cicilan/bulan</em></td>
						<td style="float:right;"><?php echo "Rp ".number_format($hasil,2,',','.');?></td>
					</tr>
					<tr>
						<td style="padding:10px;"><em>Pendapatan Minimal</em></td>
						<td style="float:right;"><?php echo "Rp ".number_format($pendapatan,2,',','.');?></td>
					</tr>
					<?php } ?>

				</table>
				
			</form>
		</div>
</div>
	</div>
</section>
<?php } ?>
</div>