<section id="about-us">
	<?php foreach ($about as $key => $value_about) { ?>
	<div class="container">
		<div class="team">
			<div class="text-justify wow fadeInDown">
				<h2 class="text-center">SEJARAH KOPERASI</h2>
				<h5 style="line-height:150%"><?php echo nl2br($value_about->sejarah_about); ?></h5>
				<hr style="border-color:rgb(102, 169, 212);">
			</div>
		</div>
	</div>

        <div class="container">
		<div class="visi">
			<div class="text-center wow fadeInDown">
				<h2 style="margin-bottom:10px;">Visi</h2>
				<p style="margin-bottom:10px;font-size:13px;"><?php echo $value_about->visi_about; ?></p>
				<h2 style="margin-bottom:10px;">Misi</h2>
				<p><?php echo $value_about->misi_about; ?></p>
				<hr style="border-color:rgb(102, 169, 212);">
			</div>
		</div>
	</div>

	<?php } ?>

	<div class="container">
		<div class="row">

		<div class="text-center wow fadeInDown team">
			<h2 style="">Jenis Usaha</h2>
		</div>
		<?php foreach ($jenis_usaha as $key => $value_usaha) { ?>
		
		<div class="col-md-3 wow fadeInDown text-center" data-wow-duration="1000ms" data-wow-delay="600ms">
			<i style="margin-top: 20px;margin-bottom: 10px;font-size: 50px;color: #2d2d2d;" class="<?php echo $value_usaha->icon; ?>"></i>
			<h3><?php echo $value_usaha->nama_usaha; ?></h3>
			<p>
				<?php echo $value_usaha->deskripsi; ?>	
			</p>
		</div>

		<?php } ?>

		</div>
		<hr style="border-color:rgb(102, 169, 212);">
	</div>

    <div class="container">		
		<div class="team" style="margin-bottom:50px;">
			<div class="text-center wow fadeInDown">
				<h2>Susunan Pengurus</h2>
				<p>Berikut adalah susunan pengurus Koperasi Sigap.</p>
			</div>

			<div class="row clearfix">
				<h4 style="margin : 20px 0px;">Pengawas</h4>
				<?php 
						foreach ($pengawas as $key => $value_pengawas) { 
				?>

				<div class="col-md-3 col-sm-6 " style="margin-bottom:20px;">	
					<div class="single-profile-top wow fadeInDown text-center" data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="media" style="height:270px;">
							<div class="">
								<a href="#"><img class="media-object" src="<?php echo base_url();?>public/img/staff/<?php echo $value_pengawas->foto_staff;?>" style="width:145px;height:137px;" width="145px" height="137px" alt=""></a>
							</div>


							<div class="media-body">
								<h4><?php echo $value_pengawas->nama_staff;?></h4>
								<h5><?php echo $value_pengawas->jabatan;?></h5>
								<h5><p>Saat ini menjabat sebagai <?php echo $value_pengawas->deskripsi_staff;?> di PT SIGAP dan telah bergabung di Koperasi SIGAP sejak : tahun <?php echo $value_pengawas->email_staff; ?></p></h5>
							</div>
						</div><!--/.media -->
					</div>
				</div><!--/.col-lg-4 -->

				<?php
						}
				echo "</div><div class='row clearfix'><h4 style='margin:20px 0px;'>Pengurus</h4>";
					foreach ($pengurus as $key => $value_pengurus) {
					
				?>
				

				<div class="col-md-3 col-sm-6" style="margin-bottom:20px;">
					<div class="single-profile-bottom wow fadeInUp text-center" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media" style="height:270px;">
							<div class="">
								<a href="#"><img class="media-object" src="<?php echo base_url();?>public/img/staff/<?php echo $value_pengurus->foto_staff;?>" style="width:145px;height:137px;" alt=""></a>
							</div>
							<div class="media-body">
								<h4><?php echo $value_pengurus->nama_staff;?></h4>
								<h5><?php echo $value_pengurus->jabatan;?></h5>
								<h5><p>Saat ini menjabat sebagai <?php echo $value_pengurus->deskripsi_staff;?> di PT SIGAP dan telah bergabung di  Koperasi SIGAP sejak : tahun <?php echo $value_pengurus->email_staff; ?></p></h5>
							</div>
						</div>
					</div>
				</div>

				<?php }

				echo "</div><div class='row clearfix'><h4 style='margin:20px 0px;'>Pelaksana Harian</h4>";

				foreach ($pelaksana as $key => $value_pelaksana) {
					
				?>

				<div class="col-md-3 col-sm-6" style="margin-bottom:20px;">	
					<div class="single-profile-top wow fadeInDown text-center" data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="media" style="height:270px;">
							<div class="">
								<a href="#"><img class="media-object" src="<?php echo base_url();?>public/img/staff/<?php echo $value_pelaksana->foto_staff;?>" style="width:145px;height:137px;" width="145px" height="137px" alt=""></a>
							</div>


							<div class="media-body">
								<h4><?php echo $value_pelaksana->nama_staff;?></h4>
								<h5><?php echo $value_pelaksana->jabatan;?></h5>
								<h5><p>Saat ini menjabat sebagai <?php echo $value_pelaksana->deskripsi_staff;?> di PT SIGAP dan telah bergabung di Koperasi SIGAP sejak : tahun <?php echo $value_pelaksana->email_staff; ?></p></h5>
							</div>
						</div><!--/.media -->
					</div>
				</div><!--/.col-lg-4 -->

				<?php } ?>
			</div>

			 <!--/.row -->

			<!-- <div class="row clearfix">   
				<div class="col-md-4 col-sm-6 col-md-offset-2">	
					<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="img/man3.jpg" alt=""></a>
							</div>

							<div class="media-body">
								<h4>Jhon Doe</h4>
								<h5>Founder and CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Web</a></li>
									<li class="btn"><a href="#">Ui</a></li>
									<li class="btn"><a href="#">Ux</a></li>
									<li class="btn"><a href="#">Photoshop</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-md-offset-2">
					<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="img/man4.jpg" alt=""></a>
							</div>
							<div class="media-body">
								<h4>Jhon Doe</h4>
								<h5>Founder and CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Web</a></li>
									<li class="btn"><a href="#">Ui</a></li>
									<li class="btn"><a href="#">Ux</a></li>
									<li class="btn"><a href="#">Photoshop</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
					</div>
				</div>
			</div> -->	<!--/.row-->
		</div><!--section-->
	</div>
</div>