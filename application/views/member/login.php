<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/bootstrap.min.css">

    <!-- CUSTOM CSS -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/admin/custom.css"> 
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
    
    <style>
.my-container {
    position: relative;
    height: 655px;
}
.my-container .container {
    position: relative;
} 
.my-container img {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    opacity: 0.5;
}

    </style>

    <meta charset="utf-8">
    <title>Flat Login</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
    <div class="my-container">
    <img src="<?php echo base_url();?>public/img/x.jpg">
    <div class="container">
        <div class="flat-form">
            <div id="login" class="form-action show">
                <div class="login-header">
                    <h1>LOGIN ANGGOTA</h1>
                </div>
                <form method="POST" action="<?php echo base_url(); ?>index.php/member/login/proseslog">
                    <ul class="none">
                        <li class="none">
                            <input type="text" name="idTxt" placeholder="ID Koperasi" />
                        </li>
                        <li>
                            <input type="password" name="passTxt" placeholder="Password" />
                        </li>
                        <li>
                            <input type="submit" value="LOGIN" class="button" />
                        </li>
                    </ul>
                </form>
            </div>
            <div style="text-align: center">
                <a style="color: #fff; text-align: center" data-toggle="modal" data-target="#forgot" href="#forgot">Lupa Password (Klik) </a>
                <br>
                <a style="color: #fff; text-align: center" data-toggle="modal" href="#myModal">Cek ID Koperasi (Klik) </a>
            </div>

                    <div class="modal fade" id="myModal">
                      <div class="modal-dialog modal-sm">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              <h4 class="modal-title">CHECK ID KOPERASI</h4>
                            </div><div class="container"></div>
                            <div class="modal-body">
                              <form method="post" action="<?php echo base_url();?>index.php/member/login/checkId">
                                <div class="form-group">
                                  <label for="">NPK</label>
                                  <input style="border: 1px solid #afafaf" type="text" class="form-control" value="" id="" name="id" placeholder="Masukan NPK" required>
                                
                                </div>
                                <div class="form-group">
                                  <button type="submit" class="btn btn-primary btn-block btn-flat">Check</button>
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                            </div>
                          </div>
                        </div>
                    </div>
            <!-- Modal -->
                    <div class="modal fade" id="forgot" role="dialog">
                      <div class="modal-dialog modal-sm">
                      
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">LUPA PASSWORD</h4>
                          </div>
                          <div class="modal-body">
                            <form name="form2" action="<?php echo site_url(); ?>/member/login/forgotPass" method="POST" enctype="multipart/form-data">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="">ID Koperasi</label>
                                  <input style="border: 1px solid #afafaf" type="text" class="form-control" value="" id="" name="id" placeholder="Masukan ID Koperasi" required>
                                                 
                              </div>

                              <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Kirim</button>
                              </div>
                              </form>
                          </div>
                          <div class="modal-footer">
                            
                          </div>
                        </div>
                        
                      </div>
                    </div>
            <!--/#login.form-action-->
    </div>
    </div>
</body>
</html>


