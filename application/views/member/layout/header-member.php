<header id="header">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <div class="navbar-brand" style="background:#fff;height:80px">
					<a href="<?php echo base_url();?>/beranda">
                        <?php foreach ($about as $key => $value_about) { ?>
                        <img src="<?php echo base_url();?>public/icon/<?php echo $value_about->logo_about; ?>" class="img-responsive" style="max-width:150px;">
                        <?php } ?>
                    </a>
				</div>
            </div>				
            <div class="navbar-collapse collapse" id="navbar-collapse">							
				<div class="menu">
					<ul class="nav navbar-nav">
						<li role="presentation"><a href="<?php echo base_url();?>">Beranda</a></li>
                        <li role="presentation"><a href="<?php echo base_url();?>index.php/team">Tentang Kami</a></li>
                        <li role="presentation"><a href="<?php echo base_url();?>index.php/news">Berita</a></li>
                        <li role="presentation"><a href="<?php echo base_url();?>index.php/faq">FAQ</a></li>
                        <li role="presentation"><a href="<?php echo base_url();?>index.php/kalkulator">Kalkulator</a></li>
                        <li role="presentation"><a href="<?php echo base_url();?>index.php/download">Unduh</a></li> 
<!--                         <li role="presentation"><a href="<?php echo base_url();?>index.php/member/started">Member Area</a></li>  -->
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Member Area <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url(); ?>/member/profil">Dashboard Profile</a></li>
                                    <li><a href="<?php echo site_url(); ?>/member/transaksi?type=simpanan">Simpanan</a></li>
                                    <li><a href="<?php echo site_url(); ?>/member/pinjaman">Pinjaman</a></li>
                                    <li><a href="<?php echo site_url(); ?>/member/appliance">Pengajuan Applikasi</a></li>
                                    <li><a href="<?php echo site_url(); ?>/member/login/logout">Keluar</a></li>
                                  </ul>
                                </li>
                        </li>
						<li role="presentation"><a href="<?php echo base_url();?>index.php/kontak">Kontak Kami</a></li>			
					</ul>
				</div>
			</div>		
        </div><!--/.container-->
    </nav><!--/nav-->		
</header><!--/header-->	