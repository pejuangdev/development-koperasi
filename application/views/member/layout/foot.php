<footer>
	<div class="container">
		<?php foreach ($about as $key_about => $value_about) { ?>
		<div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
			<h4>Tentang Kami</h4>
			<!--<img src="<?php echo base_url(); ?>public/icon/logo.jpg" width="60%" height="100" style="margin-bottom:10px ;"> -->
			<p style="text-align:justify;">
				<?php echo $value_about->deskripsi_about; ?>
			</p>						
		</div>
		<?php } ?>
		
		<div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">				
			<div >
				<h4>Foto Kegiatan Koperasi</h4>
				<ul class="sidebar-gallery">
					<?php foreach ($random_news as $key => $value_rand) { ?>
					<li><a href="<?php echo base_url(); ?>index.php/news/detail/<?php echo $value_rand->id_news; ?>"><img src="<?php echo base_url();?>public/img/news/<?php echo $value_rand->image_news; ?>"  style="width:110px;height:63px;" /></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		
		<div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">				
			
			<div>
				<h4>Informasi Kontak</h4>
				<div class="contact-info">
				<ul>
					<li><i class="fa fa-home fa">&nbsp</i> Alamat : &nbsp<?php echo $value_about->address_about; ?></li>
					<li><i class="fa fa-phone fa">&nbsp</i>Telepon : <?php echo $value_about->phone_about; ?></li>
					<li><i class="fa fa-book">&nbsp</i>Fax : <?php echo $value_about->fax_about; ?></li>
					<li><i class="fa fa-envelope fa">&nbsp</i>Email :<?php echo $value_about->email_about; ?></li>
				</ul>
			</div>
			</div>
		
	</div>	
</footer>

<div class="sub-footer">
	<div class="container">
		<div class="social-icon">
			<div class="col-md-4">
				<ul class="social-network">
					<li><a href="https://www.facebook.com/Koperasi-Sigap-1728943420682219/?fref=ts" class="fb tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://twitter.com/koperasi_sigap" class="twitter tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
					<li><a href="@koperasi_sigap" class="gplus tool-tip" title="Path"><i class="fa fa-pinterest"></i></a></li>
					
				</ul>	
			</div>
		</div>
		
		<div class="col-md-4 col-md-offset-4">
			<div class="copyright">
				&copy; 2016 Koperasi SIGAP All Rights Reserved.
			</div>
            <!-- 
                All links in the footer should remain intact. 
                Licenseing information is available at: http://bootstraptaste.com/license/
                You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Day
            -->
		</div>						
	</div>				
</div>