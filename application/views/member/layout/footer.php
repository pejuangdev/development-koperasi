<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 2.0 -->
    </div>
    <strong>Copyright &copy; 2015 <a href="#"></a></strong>
 </footer>

    <!-- jQuery UI 1.11.2 -->
    <script src="<?php echo base_url(); ?>asset/js/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>asset/js/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url(); ?>asset/js/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?php echo base_url(); ?>asset/js/fastclick/fastclick.min.js'></script>
    <!-- membuat efek animasi -->
    <script src="<?php echo base_url(); ?>asset/js/cust/app.js"></script>