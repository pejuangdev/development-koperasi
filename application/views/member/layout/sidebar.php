<div class="col-sm-2 sidebar-menu-nav">
    <div class="panel panel-default">
      <li>
        <a href="<?php echo site_url(); ?>/member/profil">
          <span>Dashboard Profile</span>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url(); ?>/member/transaksi?type=simpanan">
          <span>Simpanan</span>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url(); ?>/member/transaksi?type=potongan">
          <span>Potongan</span>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url(); ?>/member/appliance">
          <span>Pengajuan Berkas</span>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url(); ?>/member/pinjaman">
          <span>Pinjaman</span>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url(); ?>/member/login/logout">
          <span>Keluar</span>
        </a>
      </li>
    </div>
  </div>