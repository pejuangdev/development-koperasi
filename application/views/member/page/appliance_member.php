<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>

  <div class="row" style="margin-top:-20px;">

  <?php
    $this->load->view('member/layout/sidebar');
  ?>

    <div class="col-md-10 member-content">
        <div class="col-md-12">
          <div class="box content-box" style="color: #fff;">
                <div class="box-header">
                  <i class="fa fa-plus"></i>
                  <h3 class="box-title">FORM DATA PENGAJUAN </h3>
                </div>
                <div class="box-body chat" id="chat-box">
                  <!-- chat item -->
                  <div class="item">

                    <form role="form" action="<?php echo site_url(); ?>/member/archive/do_add_appliance" method="POST" enctype="multipart/form-data">
                    <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">NPK</label>
                            <input type="input" class="form-control" value="<?php echo $this->session->userdata("npk"); ?>" name="npk" placeholder="NPK" readonly>
                        </div>
                        <div class="form-group">
                          <label for="">Lokasi Penempatan</label>
                            <select name="penempatan" class="form-control">
                            <?php foreach ($data as $value) { ?>
                              <option value="<?php echo $value->id_instalasi; ?>"><?php echo $value->nama_instalasi; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Besar pinjaman</label>
                            <input type="number" class="form-control" value="" name="pinjaman" placeholder="Dalam rupiah, contoh format : 100000">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">No Handphone</label>
                            <input type="number" class="form-control" value="" name="telp" placeholder="Masukan Nomor Telephone yang aktif">
                        </div>
                        <div class="form-group">
                         <label for="">Jangka Waktu Peminjaman</label>
                          <select class="form-control" name="waktu">
                            <option value="1">1 Bulan</option>
                            <option value="3">3 Bulan</option>
                            <option value="6">6 Bulan</option>
                            <option value="12">12 Bulan</option>
                            <option value="18">18 Bulan</option>
                            <option value="24">24 Bulan</option>
                            <option value="36">36 Bulan</option>
                            <option value="72">72 Bulan</option>       
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="">Digunakan untuk keperluan</label>
                          <textarea placeholder="Jelaskan keperluan peminjaman dengan detail" name="usage" class="form-control" style="height:100px"></textarea>
                        </div>
                      </div>
                  </div><!-- /.item -->

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                    <a href="<?php echo base_url(); ?>index.php/member/profil" class="btn btn-warning btn-block btn-flat">Kembali</a>
                  </div><!-- /.col -->
                 </form>
                </div><!-- /.chat -->
              </div><!-- /.box (chat box) -->
        </div><!-- /.col -->
    </div>
  </div>
