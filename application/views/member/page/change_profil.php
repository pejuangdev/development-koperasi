<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA PROFIL</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM DATA PROFIL</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                <?php 
                foreach ($data as  $value) {
                ?>
                  <form role="form" action="<?php echo site_url(); ?>/app/anggota/do_change_profil" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Nama Anggota</label>
                        <input type="text" class="form-control" name="nama" placeholder="Nama Cost Center"  value ="<?php echo $value->nama_anggota; ?>" required>                      
                    </div>
                    <div class="form-group">
                      <label for="">Tanggal Lahir</label>
                        <input type="date" class="form-control" value="<?php echo $value->tanggal_lahir; ?>" name="tgl_lahir" placeholder="Tanggal Lahir" required>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    
                    <div class="form-group">
                      <label for="">Alamat Anggota</label>
                      <textarea style="height: 100px;" name="alamat" placeholder="Alamat" class="form-control" required><?php echo $value->alamat_anggota; ?></textarea>                      
                    </div>
                  </div>
                    
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/member/profil" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
               <?php } ?>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->
      </section><!-- /.content -->
