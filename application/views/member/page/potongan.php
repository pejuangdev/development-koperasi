<section class="content-header">
        <h1>
          <b>DATA POTONGAN UTAMA ANGGOTA KOPERASI</b>
        </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
            <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Potongan Utama Anggota Koperasi </h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Date</th>
                          <th>Regular Loan</th>
                          <th>Potongan Belanja</th>
                          <th>Potongan Sembako</th>
                          <th>Potongan I-Care</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>

                  <tfoot>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Instalasi</th>
                      <th>Date</th>
                      <th>Regular Loan</th>
                      <th>Potongan Belanja</th>
                      <th>Potongan Sembako</th>
                      <th>Potongan I-Care</th>
                  </tr>
                  </tfoot>
              </table>
              </div>
            </div><!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-title" style="margin-left:10px;">
                      <h4>Total Potongan Utama Anggota</h4>
                    </div><!-- /.box-title -->
                    <div class="box-body">
                        <table class="table" style="table-border:5px solid #000;">
                            <tr>
                              <td>Regular Loan</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('pinjaman',$id));
                                  ?> 
                              </td>
                            </tr> 

                             <tr>
                              <td>Potongan Belanja</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_belanja',$id));
                                  ?> 
                              </td>
                            </tr> 

                            <tr>
                              <td>Potongan Sembako</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_sembako',$id));
                                  ?> 
                              </td>
                            </tr>
                            <tr>
                              <td>Potongan I-Care</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_icare',$id));
                                  ?> 
                              </td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('member/transaksi/ajax_list_potongan')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1] }],
        "aaSorting": []

    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});



function add_person()
{   
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Transaksi'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Transaksi'); // Set Title to Bootstrap modal title
}

function edit_transaksi(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('app/transaksi/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="npk"').val(data.id_kop);
            $('[name="id_transaksi"').val(data.id_transaksi);
            $('[name="instalasi"]').val(data.id_instalasi);
            $('[name="bulan"]').val(data.bulan_transaksi);
            $('[name="tahun"]').val(data.tahun_transaksi);
            $('[name="sim_jip"]').val(data.simpanan_wajib);
            $('[name="sim_pok"]').val(data.simpanan_pokok);
            $('[name="sim_suk"]').val(data.simpanan_sukarela);
            $('[name="pinjaman"]').val(data.pinjaman);
            $('[name="pot_bel"]').val(data.potongan_belanja);
            $('[name="pot_sem"]').val(data.potongan_sembako);
            $('[name="pot_or"]').val(data.potongan_or);
            $('[name="shu"]').val(data.SHU);
            $('[name="icare"]').val(data.potongan_icare);
            $('[name="emoney"]').val(data.potongan_emoney);
            $('[name="motor"]').val(data.potongan_or);
            $('[name="other"]').val(data.potongan_other);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('app/transaksi/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        url = "<?php echo site_url('app/transaksi/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Transaksi Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Transaksi Gagal '+ message + ' ( Data Salah atau Kurang lengkap ) ');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_transaksi(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('app/transaksi/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                alert('Data Transaksi Berhasil Dihapus');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>
