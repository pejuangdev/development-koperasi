<?php $date = ""; ?>
<section class="content-header">
        <h1>
          <b>DAFTAR PENGAJUAN APLIKASI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-success" onclick="add_pengajuan()"><i class="glyphicon glyphicon-plus"></i> Tambah Pengajuan </button>
            <button class="btn btn-warning" onclick="upload()"><i class="glyphicon glyphicon-upload"></i> Upload Berkas </button>
             <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Pengajuan Aplikasi Pinjaman</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <?php if($pengajuan > 0){ ?>
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>ID Koperasi</th>
                          <th>Instalasi</th>
                          <th>Jumlah Pengajuan</th>
                          <th>Tenor (bulan)</th>
                          <th>Status Applikasi</th>
                          <th style="width:125px;">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach ($applikasi as $key => $value) {$no++; ?>
                      <tr>
                          <td><?php echo $no; ?></td>
                          <td><?php echo $value->nama_anggota; ?></td>
                          <td><?php echo $value->nama_instalasi; ?></td>
                          <td><?php echo number_format($value->value_of); ?></td>
                          <td><?php echo $value->time_of; ?></td>
                          <td><?php if($value->status_appliance == 0)
                                  {echo "<label class='label label-info'>Progress</label>";}
                                  else if($value->status_appliance == 1){echo "<label class='label label-success'>Diterima</label>";}
                                  else if($value->status_appliance == 2)
                                    {echo "<label class='label label-danger'>Ditolak</label>";} 
                                else if($value->status_appliance == 3)
                                    {echo "<label class='label label-warning'>Kurang Berkas</label>";} ?></td>
                          <td style="width:125px;">
                            <a class="btn btn-sm btn-success" href="javascript:void(0)" title="Detail" onclick="document.location.href='<?php echo base_url(); ?>index.php/member/appliance/detail_appliance/<?php echo $value->id_appliance;?>'"><i class="fa fa-list"></i></a>
                            <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_pengajuan(<?php echo $value->id_appliance;?>)"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_pengajuan(<?php echo $value->id_appliance;?>)"><i class="fa fa-trash-o"></i></a>
                          </td>
                      </tr>
                    <?php } ?>
                  </tbody>
              </table>
              <?php }else{echo "<strong>Belum ada pengajuan applikasi</strong>";} ?>
              </div>
            </div><!-- /.box -->

            <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Berkas Pemohon</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                <?php if($berkas >0){ 
                    foreach ($data_berkas as $key => $value) {
                ?>
                
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <tr>
                        <td>KTP</td>
                        <td>
                            <?php 
                                if(empty($value->ktp) || ($value->ktp=="NULL") || ($value->ktp=="null")){
                                    echo "<label class='label label-danger'>Belum Upload KTP</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/ktp/<?php echo $value->ktp;?>'>Lihat KTP</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>KK</td>
                        <td>
                            <?php 
                                if(empty($value->kk) || ($value->kk=="NULL") || ($value->kk=="null")){
                                    echo "<label class='label label-danger'>Belum Upload KK</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/kk/<?php echo $value->kk;?>'>Lihat KK</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Slip Gaji</td>
                        <td>
                            <?php 
                                if(empty($value->slip_gaji) || ($value->slip_gaji=="NULL") || ($value->slip_gaji=="null")){
                                    echo "<label class='label label-danger'>Belum Upload Slip Gaji</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/slip_gaji/<?php echo $value->slip_gaji;?>'>Lihat Slip Gaji</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Buku Tabungan</td>
                        <td>
                            <?php 
                                if(empty($value->buku_tabungan) || ($value->buku_tabungan=="NULL") || ($value->buku_tabungan=="null")){
                                    echo "<label class='label label-danger'>Belum Upload Buku Tabungan</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/buku_tabungan/<?php echo $value->buku_tabungan;?>'>Lihat Buku Tabungan</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>ID Card</td>
                        <td>
                            <?php 
                                if(empty($value->id_card) || ($value->id_card=="NULL") || ($value->id_card=="null")){
                                    echo "<label class='label label-danger'>Belum Upload ID Card</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/id_card/<?php echo $value->id_card;?>'>Lihat ID Card</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Surat Kuasa</td>
                        <td>
                            <?php 
                                if(empty($value->surat_kuasa) || ($value->surat_kuasa=="NULL") || ($value->surat_kuasa=="null")){
                                    echo "<label class='label label-danger'>Belum Upload Surat Kuasa</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/surat_kuasa/<?php echo $value->surat_kuasa;?>'>Lihat Surat Kuasa</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Lain - Lain</td>
                        <td>
                            <?php 
                                if(empty($value->npwp) || ($value->npwp=="NULL") || ($value->npwp=="null")){
                                    echo "<label class='label label-danger'>Belum Upload NPWP</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/npwp/<?php echo $value->npwp;?>'>Lihat Berkas Lain</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                </table>
              <?php }}else{echo "<strong>Belum ada berkas yang di upload</strong>";} ?>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string

function add_pengajuan()
{
    $('[name="id"]').val('add');
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Pengajuan Applikasi Pinjaman'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Cost Center'); // Set Title to Bootstrap modal title
}

function edit_pengajuan(id)
{   
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('member/appliance/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val('edit');
            $('[name="id_appliance"]').val(data.id_appliance);
            $('[name="id_cost"]').val(data.id_cost_center);
            $('[name="instalasi"]').selectpicker('val',data.id_installation);
            $('[name="pinjaman"]').val(data.value_of);
            $('[name="tenor"]').val(data.time_of);
            $('[name="telepon"]').val(data.phone_number);
            $('[name="tujuan"]').val(data.usages);
            $('[name="istri"]').val(data.wife);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Cost Center'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('app/company/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        document.formcost.id_cost.disabled=false;
        url = "<?php echo site_url('app/company/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Cost Center Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Cost Center Gagal '+ message +' ( Data Salah atau Kurang Lengkap )');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_pengajuan(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('member/appliance/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                alert('Data Pengajuan Berhasil Dihapus');
                $('#modal_form').modal('hide');
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>
<?php foreach ($data as $key => $value) { $date = $value->tanggal_gabung; ?>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH APPLIKASI PINJAMAN</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/member/appliance/add_applikasi" method="post" id="form" name="formcost" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <input type="hidden" value="<?php echo $value->id_kop; ?>" name="id_anggota">
                    <input type="hidden" value="" name="id_appliance">
                    
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Koperasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="<?php echo $value->id_kop; ?>" name="id_kop" placeholder="Kode Cost Center" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NPK</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="<?php echo $value->npk; ?>" name="npk" placeholder="NPK" disabled>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Instalasi</label>
                            <div class="col-md-9">
                                <select name="instalasi" required class="form-control selectpicker" data-live-search="true">
                                  <option value="">Pilih Instalasi</option>
                                  <?php foreach ($instalasi as $key => $value) {
                                    echo "<option value='". $value->id_instalasi ."'>". $value->nama_instalasi ."</option>";
                                  }?>
                              </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nomor Telepon</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" name="telepon" placeholder="Nomor Telepon">                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Pengajuan Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="pinjaman" name="pinjaman" onChange="calculateBobot()" placeholder="Jumlah Pinjaman contoh : 200000" data-validation="required" data-validation-error-msg="Jumlah Pinjaman is required">                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lama Pinjaman</label>
                            <div class="col-md-9">
                                <select class="form-control" id="tenor" name="tenor" onChange="calculateBobot()" placeholder="Lama pinjaman" data-validation="required" data-validation-error-msg="Lama Pinjaman is required">
                                    <option value="">Pilih Tenor</option>
                                    <option value="1">1 Bulan</option>
                                    <option value="3">3 Bulan</option>
                                    <option value="6">6 Bulan</option>
                                    <option value="12">12 Bulan</option>
                                    <option value="18">18 Bulan</option>
                                    <option value="24">24 Bulan</option>
                                    <option value="36">36 Bulan</option>
                                    <option value="72">72 Bulan</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Cicilan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="cicilan" name="cicilan" value="0.00" disabled>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tujuan Pinjaman</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="tujuan" placeholder="Tujuan Pinjaman" data-validation="required" data-validation-error-msg="Tujuan Pinjaman is required">

                                </textarea>                     
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Istri</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="istri" placeholder="Nama Istri" data-validation="required" data-validation-error-msg="Nama Istri is required">                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
               
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" name="btnSave" class="btn btn-primary">Ajukan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
             </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>

<div class="modal fade" id="modal_upload" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM UPLOAD BERKAS</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/member/appliance/upload_data" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">KTP (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile1" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">KK (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile2" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Slip Gaji (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile3" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Buku Tabungan (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile4" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Card (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile5" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Surat Kuasa (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile6" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lain - Lain (.pdf atau .jpg)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile7" placeholder="KTP (FORMAT .PDF atau .JPG)" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script type="text/javascript">
function calculateBobot(){
    var bobotCustomString = $('#pinjaman').val();
    var tenor = $('#tenor').val();
    if(tenor == 0){
      tenor = 1;
    }
    var tenorCustom = Number(tenor);
    var bobotCustom = Number(bobotCustomString);

    var hasil = (bobotCustom+(bobotCustom*tenorCustom/12*10/100))/tenorCustom;
    
    var bobotValue = 0;
    if(bobotCustom != ""){
        $('#cicilan').val(hasil.toFixed(2));
    }
    else{
        $('#pinjaman').val((bobotValue*100).toFixed(2));
    }
}

</script>