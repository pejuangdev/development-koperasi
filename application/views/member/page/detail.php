<?php  
    $bunga = $this->mod_archive->getBunga();
?>
<section class="content-header">
        <h1>
          <b>DETAIL PENGAJUAN APLIKASI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <button class="btn btn-warning" onclick="document.location.href='<?php echo base_url();?>index.php/member/appliance'"><i class="glyphicon glyphicon-chevron-left"></i> Kembali </button>
             <div class="box" style="margin-top:30px;">
                <div class="box-title container">
                    <h4>Informasi Detail Pengajuan</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <?php foreach ($applikasi as $key => $value) { ?>
                    <tr>
                        <td>ID Koperasi</td>
                        <td><?php echo $value->id_kop; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Anggota</td>
                        <td><?php echo $value->nama_anggota; ?></td>
                    </tr>
                    <tr>
                        <td>Instalasi</td>
                        <td><?php echo $value->nama_instalasi; ?></td>
                    </tr>
                    <tr>
                        <td>Jumlah Pengajuan</td>
                        <td><?php echo number_format($value->value_of); ?></td>
                    </tr>
                    <tr>
                        <td>Lama Pinjaman</td>
                        <td><?php echo $value->time_of; ?> Bulan</td>
                    </tr>
                    <tr>
                        <td>Tanggal Pengajuan</td>
                        <td><?php echo date('d-M-Y',strtotime($value->create_date)); ?></td>
                    </tr>
                    <tr>
                        <td>No Telepon </td>
                        <td><?php echo $value->phone_number; ?></td>
                    </tr>
                    <tr>
                        <td>Tujuan Pengajuan</td>
                        <td><?php echo $value->usages; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Istri / Suami</td>
                        <td><?php echo $value->wife; ?></td>
                    </tr>
                    <tr>
                        <td>Status Pengajuan</td>
                        <td>
                            <?php 
                                if($value->status_appliance == 0)
                                    {echo "<label class='label label-info'>Progress</label>";}
                                else if($value->status_appliance == 1)
                                    {echo "<label class='label label-success'>Diterima</label>";}
                                else if($value->status_appliance == 2)
                                    {echo "<label class='label label-danger'>Ditolak</label>";} 
                                else if($value->status_appliance == 3)
                                    {echo "<label class='label label-warning'>Kurang Berkas</label>";} 
                            ?>
                        </td>
                    </tr>  
                    <tr>
                        <td>Nilai Persetujuan</td>
                        <td><?php  
                                if($value->nilai_persetujuan == 0){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo number_format($value->nilai_persetujuan);
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Waktu Persetujuan</td>
                        <td><?php  
                                if($value->waktu_persetujuan == 0){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo $value->waktu_persetujuan . " Bulan";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Rencana Pencairan</td>
                        <td><?php  
                                if($value->rencana_pencairan == 0){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo date('d-M-Y',strtotime($value->rencana_pencairan));
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Nama yang Menyetujui</td>
                        <td><?php  
                                if($value->nama_menyetujui == NULL){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo $value->nama_menyetujui;
                                }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Cicilan</td>
                        <td>
                            <?php 
                                if($value->status_appliance == 0){
                                    echo number_format(($value->value_of+($value->value_of*$value->time_of/12*10/100))/$value->time_of) . " (Belum Disetujui)";
                                }else{
                                    echo number_format(($value->nilai_persetujuan+($value->nilai_persetujuan*$value->waktu_persetujuan/12*10/100))/$value->waktu_persetujuan) . " (Sudah Disetujui)";                                    
                                }
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
              </div>
            </div><!-- /.box -->
            <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Berkas Pemohon</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                <?php if($berkas >0){ 
                    foreach ($data_berkas as $key => $value) {
                ?>
                
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <tr>
                        <td>KTP</td>
                        <td>
                            <?php 
                                if(empty($value->ktp) || ($value->ktp=="NULL") || ($value->ktp=="null")){
                                    echo "<label class='label label-danger'>Belum Upload KTP</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/ktp/<?php echo $value->ktp;?>'>Lihat KTP</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>KK</td>
                        <td>
                            <?php 
                                if(empty($value->kk) || ($value->kk=="NULL") || ($value->kk=="null")){
                                    echo "<label class='label label-danger'>Belum Upload KK</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/kk/<?php echo $value->kk;?>'>Lihat KK</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Slip Gaji</td>
                        <td>
                            <?php 
                                if(empty($value->slip_gaji) || ($value->slip_gaji=="NULL") || ($value->slip_gaji=="null")){
                                    echo "<label class='label label-danger'>Belum Upload Slip Gaji</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/slip_gaji/<?php echo $value->kk;?>'>Lihat Slip Gaji</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Buku Tabungan</td>
                        <td>
                            <?php 
                                if(empty($value->buku_tabungan) || ($value->buku_tabungan=="NULL") || ($value->buku_tabungan=="null")){
                                    echo "<label class='label label-danger'>Belum Upload Buku Tabungan</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/buku_tabungan/<?php echo $value->buku_tabungan;?>'>Lihat Buku Tabungan</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>ID Card</td>
                        <td>
                            <?php 
                                if(empty($value->id_card) || ($value->id_card=="NULL") || ($value->id_card=="null")){
                                    echo "<label class='label label-danger'>Belum Upload ID Card</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/id_card/<?php echo $value->id_card;?>'>Lihat ID Card</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Surat Kuasa</td>
                        <td>
                            <?php 
                                if(empty($value->surat_kuasa) || ($value->surat_kuasa=="NULL") || ($value->surat_kuasa=="null")){
                                    echo "<label class='label label-danger'>Belum Upload Surat Kuasa</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/surat_kuasa/<?php echo $value->surat_kuasa;?>'>Lihat Surat Kuasa</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Lain - Lain</td>
                        <td>
                            <?php 
                                if(empty($value->npwp) || ($value->npwp=="NULL") || ($value->npwp=="null")){
                                    echo "<label class='label label-danger'>Belum Upload NPWP</label>";
                                }else{ ?>
                            <label class="label label-info"><a style='color:#fff' href='<?php echo base_url(); ?>public/file/anggota/archive/npwp/<?php echo $value->npwp;?>'>Lihat Berkas Lain</a></label>
                            <?php    }
                            ?>
                        </td>
                    </tr>
                </table>
              <?php }}else{echo "<strong>Belum ada berkas yang di upload</strong>";} ?>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

<script type="text/javascript">

var save_method; //for save method string

$(document).ready(function() {
    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });
});

function add_pengajuan()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Pengajuan Applikasi Pinjaman'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Cost Center'); // Set Title to Bootstrap modal title
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('app/company/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        document.formcost.id_cost.disabled=false;
        url = "<?php echo site_url('app/company/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Cost Center Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Cost Center Gagal '+ message +' ( Data Salah atau Kurang Lengkap )');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_pengajuan(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('member/appliance/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                alert('Data Pengajuan Berhasil Dihapus');
                $('#modal_form').modal('hide');
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>
<?php foreach ($applikasi as $key => $value) { ?>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM TAMBAH APPLIKASI PINJAMAN</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/member/appliance/approve_applikasi" method="post" id="form" name="formcost" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <input type="hidden" value="<?php echo $value->id_kop; ?>" name="id_anggota">
                    <input type="hidden" value="<?php echo $id_appliance; ?>" name="id_appliance">
                    
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID Koperasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="<?php echo $value->id_kop; ?>" name="id_kop" placeholder="Kode Cost Center" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NPK</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="<?php echo $value->npk; ?>" name="npk" placeholder="NPK" disabled>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Instalasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="<?php echo $value->nama_instalasi; ?>" name="npk" placeholder="NPK" disabled>                      
                             <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Pengajuan Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="pinjaman" disabled name="pinjaman" onChange="calculateBobot()" placeholder="Jumlah Pinjaman contoh : 200000" data-validation="required" data-validation-error-msg="Jumlah Pinjaman is required">                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lama Pinjaman</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="tenor" name="tenor" disabled>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nilai Persetujuan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="nilai" name="nilai" onChange="calculateBobot()" >                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Waktu Persetujuan</label>
                            <div class="col-md-9">
                                <select class="form-control" id="tenor_baru" name="tenor_baru" onChange="calculateBobot()" placeholder="Lama pinjaman" data-validation="required" data-validation-error-msg="Lama Pinjaman is required">
                                    <option value="0">Pilih Tenor</option>
                                    <option value="1">1 Bulan</option>
                                    <option value="3">3 Bulan</option>
                                    <option value="6">6 Bulan</option>
                                    <option value="12">12 Bulan</option>
                                    <option value="18">18 Bulan</option>
                                    <option value="24">24 Bulan</option>
                                    <option value="36">36 Bulan</option>
                                    <option value="72">72 Bulan</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Cicilan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" disabled id="cicilan" name="cicilan" >                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Rencana Pencairan</label>
                            <div class="col-md-9">
                                <input name="tgl_cair" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status Applikasi</label>
                            <div class="col-md-9">
                                <select class="form-control" id="status" name="status" placeholder="Status Applikasi" data-validation="required" data-validation-error-msg="Status Applikasi is required">
                                    <option value="">Pilih Status</option>
                                    <option value="0">Proses</option>
                                    <option value="1">Diterima</option>
                                    <option value="2">Ditolak</option>
                                    <option value="3">Kurang Berkas</option>
                                </select>                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Yang Menyetujui</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="nama" name="nama" >                      
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
               
            </div>
            <div class="modal-footer" style="margin-top: 50px;">
                <button type="submit" id="btnSave" name="btnSave" class="btn btn-primary">Approve</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
             </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>

<div class="modal fade" id="modal_upload" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FORM UPLOAD COST CENTER</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>index.php/app/company/upload_data" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Data Company (.XLS)</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" value="" name="userfile" placeholder="Data Company (HARUS FORMAT .XLS)" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script type="text/javascript">
function calculateBobot(){
    var bobotCustomString = $('#nilai').val();
    var tenor = $('#tenor_baru').val();
    if(tenor == 0){
      tenor = 1;
    }
    var tenorCustom = Number(tenor);
    var bobotCustom = Number(bobotCustomString);

    var hasil = (bobotCustom+(bobotCustom*tenorCustom/12*<?php echo $bunga; ?>/100))/tenorCustom;
    
    var bobotValue = 0;
    if(bobotCustom != ""){
        $('#cicilan').val(hasil.toFixed(2));
    }
    else{
        $('#nilai').val((bobotValue*100).toFixed(2));
    }
}

</script>