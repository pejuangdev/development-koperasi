<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
	width: 100%;
	left: 0px;
	right: 0px;
	bottom: 0px;
	text-align: left;
	padding-top: 5px;
	padding-left: 15%;
	padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
	margin-top: 10px;
	font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
	.transition-timer-carousel .carousel-caption .carousel-caption-header {
		font-size: 36px;
	}
}
.transition-timer-carousel .carousel-indicators {
	bottom: 0px;
	margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
	z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
	-moz-transition: width 4.25s linear;
	-o-transition: width 4.25s linear;
	transition: width 4.25s linear;
}


.carousel-caption{
	margin-top: 20%;
}
</style>

<div class="text-center gallery">
  <h3>FORM GANTI PASSWORD</h3>
</div>

<div class="container" style="margin-bottom:50px;">

<div class="row" style="margin-top:-20px;">
    <div class="col-md-3">

    </div>
	<div class="col-md-9">
      	<div class="col-md-12" style="padding:5px 2px 10px 5px">
      		<form role="form" action="<?php echo site_url(); ?>/member/profil/doChangePassword" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="label-color" for="">Password Lama</label>
                        <input type="text" class="form-control" value="<?php echo $this->session->userdata('password'); ?>" name="lastPassword" placeholder="Password Lama Anda" readonly>
                        <input type="hidden" name="immediately" value="<?php echo $this->session->userdata('immediately'); ?>">
                    </div>
                    <div class="form-group">
                      <label class="label-color" for="">Password Baru</label>
                        <input type="text" class="form-control" name="currentPass" placeholder="Password Baru anda" required>                      
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                    <a href="<?php echo base_url(); ?>index.php/member/profil" class="btn btn-warning btn-block btn-flat">Kembali</a>
                  </div>
        	</form>
      	</div>
	</div>
</div>

</div>
