<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>

<div class="text-center gallery">
  <h3>SIMPANAN KOPERASI</h3>
</div>
<div class="container" style="margin-bottom:50px;">

<div class="row" style="margin-top:-20px;">

  <div class="col-md-12 member-content">
    <div class="box-title" style="margin-left:10px;">
      <h4>Total Simpanan Anggota</h4>
    </div><!-- /.box-title -->
    <div class="box-body">
        <table class="table" style="table-border:5px solid #000;">
            <tr>
              <td>Simpanan Wajib</td>
              <td align="right">
                  <?php
                        echo number_format($this->mod_transaksi->hitung_total_member('simpanan_wajib',$id));
                  ?> 
              </td>
            </tr> 

             <tr>
              <td>Simpanan Pokok</td>
              <td align="right">
                  <?php
                        echo number_format($this->mod_transaksi->hitung_total_member('simpanan_pokok',$id));
                  ?> 
              </td>
            </tr> 

            <tr>
              <td>Simpanan Sukarela</td>
              <td align="right">
                  <?php
                        echo number_format($this->mod_transaksi->hitung_total_member('simpanan_sukarela',$id));
                  ?> 
              </td>
            </tr>
            <tr>
              <td>SHU</td>
              <td align="right">
                  <?php
                        echo number_format($this->mod_transaksi->hitung_total_member('SHU',$id));
                  ?> 
              </td>
            </tr> 
        </table>
    </div>
            <div class="box" style="margin-top:30px;">
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Simpanan Anggota Koperasi </h4>
                </div><!-- /.box-title -->
                <div class="box-body table-responsive">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Date</th>
                          <th>Simpanan Wajib</th>
                          <th>Simpanan Pokok</th>
                          <th>Simpanan Sukarela</th>
                          <th>SHU</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { 
                      if(empty($value->id_transaksi)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->nama_anggota; ?></td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                      <td><?php echo $this->mod_member->listBulan($value->bulan_transaksi) . " " . $value->tahun_transaksi;?></td>
                      <td><?php echo number_format($value->simpanan_wajib); ?></td>
                      <td><?php echo number_format($value->simpanan_pokok); ?></td>
                      <td><?php echo number_format($value->simpanan_sukarela); ?></td>
                      <td><?php echo number_format($value->SHU); ?></td>
                    </tr>
                    <?php }} ?>
                  </tbody>
              </table>
              </div>
            </div><!-- /.box -->
  </div>
</div>
</div>
