<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>

<div class="text-center gallery">
  <h3>DETAIL PENGAJUAN PINJAMAN</h3>
</div>

<div class="container" style="margin-bottom:50px;">

  <div class="row" style="margin-top:-20px;">

    <div class="col-md-12 member-content">
        <div class="col-md-12">
          <div class="box content-box" style="color: #fff;">
             <button class="btn btn-warning btn-red-btn" onclick="document.location.href='<?php echo base_url();?>index.php/member/appliance'"><i class="glyphicon glyphicon-chevron-left"></i> Kembali </button>
             <div class="box" style="margin-top:30px;">
                <div class="box-title">
                    <h4>Informasi Detail Pengajuan</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <?php foreach ($applikasi as $key => $value) { ?>
                    <tr>
                        <td>ID Koperasi</td>
                        <td><?php echo $value->id_kop; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Anggota</td>
                        <td><?php echo $value->nama_anggota; ?></td>
                    </tr>
                    <tr>
                        <td>Instalasi</td>
                        <td><?php echo $value->nama_instalasi; ?></td>
                    </tr>
                    <tr>
                        <td>Jumlah Pengajuan</td>
                        <td><?php echo number_format($value->value_of); ?></td>
                    </tr>
                    <tr>
                        <td>Lama Pinjaman</td>
                        <td><?php echo $value->time_of; ?> Bulan</td>
                    </tr>
                    <tr>
                        <td>Tanggal Pengajuan</td>
                        <td><?php echo date('d-M-Y',strtotime($value->create_date)); ?></td>
                    </tr>
                    <tr>
                        <td>No Telepon </td>
                        <td><?php echo $value->phone_number; ?></td>
                    </tr>
                    <tr>
                        <td>Tujuan Pengajuan</td>
                        <td><?php echo $value->usages; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Istri / Suami</td>
                        <td><?php echo $value->wife; ?></td>
                    </tr>
                    <tr>
                        <td>Status Pengajuan</td>
                        <td>
                            <?php 
                                if($value->status_appliance == 0)
                                    {echo "<label class='label label-info'>Progress</label>";}
                                else if($value->status_appliance == 1)
                                    {echo "<label class='label label-success'>Disetujui</label>";}
                                else if($value->status_appliance == 2)
                                    {echo "<label class='label label-danger'>Ditolak</label>";} 
                                else if($value->status_appliance == 3)
                                    {echo "<label class='label label-warning'>Kurang Berkas</label>";} 
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td><?php echo $value->keterangan; ?></td>
                    </tr>  
                    <tr>
                        <td>Nilai Persetujuan</td>
                        <td><?php  
                                if($value->nilai_persetujuan == 0){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo number_format($value->nilai_persetujuan);
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Waktu Persetujuan</td>
                        <td><?php  
                                if($value->waktu_persetujuan == 0){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo $value->waktu_persetujuan . " Bulan";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Rencana Pencairan</td>
                        <td><?php  
                                if($value->rencana_pencairan == 0){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo date('d-M-Y',strtotime($value->rencana_pencairan));
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Nama yang Menyetujui</td>
                        <td><?php  
                                if($value->nama_menyetujui == NULL){
                                    {echo "<label class='label label-info'>Progress</label>";}
                                }else{
                                    echo $value->nama_menyetujui;
                                }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Cicilan</td>
                        <td>
                            <?php 
                                if($value->status_appliance == 0){
                                    echo number_format(($value->value_of+($value->value_of*$value->time_of/12*10/100))/$value->time_of) . " (Belum Disetujui)";
                                }else{
                                    echo number_format(($value->nilai_persetujuan+($value->nilai_persetujuan*$value->waktu_persetujuan/12*10/100))/$value->waktu_persetujuan) . " (Sudah Disetujui)";                                    
                                }
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
              </div>
              </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
  </div>
</div>
