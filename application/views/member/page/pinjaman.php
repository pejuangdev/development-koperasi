
<section class="content-header">
        <h1>
          <b>DATA PINJAMAN</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title" style="margin-left:10px;">
                    <h4>Informasi Pinjaman Anggota</h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NPK</th>
                      <th>Nama</th>
                      <th>Instalasi</th>
                      <th>Tanggal Transaksi</th>
                      <th>Potongan Belanja</th>
                      <th>Potongan Sembako</th>
                      <th>Potongan OR</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { 
                      if(empty($value->id_transaksi)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->npk; ?></td>
                      <td><?php echo $value->nama_anggota; ?></td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                      <td><?php echo $this->mod_member->listBulan($value->bulan_transaksi) . " " . $value->tahun_transaksi;?></td>
                      <td><?php echo number_format($value->potongan_belanja); ?></td>
                      <td><?php echo number_format($value->potongan_sembako); ?></td>
                      <td><?php echo number_format($value->potongan_or); ?></td>
                    </tr>
                    <?php }} ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-title" style="margin-left:10px;">
                      <h4>Total Potongan Anggota</h4>
                    </div><!-- /.box-title -->
                    <div class="box-body">
                        <table class="table" style="table-border:5px solid #000;">
                            <tr>
                              <td>Potongan Belanja</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_member->hitung_total('potongan_belanja'));
                                  ?> 
                              </td>
                            </tr> 

                             <tr>
                              <td>Potongan Sembako</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_member->hitung_total('potongan_sembako'));
                                  ?> 
                              </td>
                            </tr> 

                            <tr>
                              <td>Potongan OR</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_member->hitung_total('potongan_or'));
                                  ?> 
                              </td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>