<section class="content-header">
        <h1>
          <b>DASHBOARD</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                <?php foreach ($data as $value) { ?>
                <table>
                  <tr>
                    <td><h5>NPK &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->npk; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Nama &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->nama_anggota; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Alamat &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->alamat_anggota; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Tanggal Lahir &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->tanggal_lahir; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Status &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php if ($value->status_anggota==1) echo "Aktif"; else echo "Tidak Aktif"; ?></h5></td>
                  </tr>
                </table>
                <a class="btn btn-warning" href="<?php echo site_url(); ?>/member/profil/changePass">Ubah Password</a>
                <a class="btn btn-success" href="<?php echo site_url(); ?>/member/profil/changeProfil">Ubah Profil</a>
                <?php } ?>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->

      <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>asset/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
      
      <script type="text/javascript">
        $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
              //waktu flash data :v
        $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
        });
      </script>