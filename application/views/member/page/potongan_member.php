<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>

<div class="row" style="margin-top:-20px;">

  <div class="col-md-12 member-content">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Potongan Utama</a></li>
    <li role="presentation"><a href="#lainnya" aria-controls="profile" role="tab" data-toggle="tab">Potongan Lainnya</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="box-body">
      <div class="box-title" style="margin-left:10px;">
        <h4>Total Potongan Utama Anggota</h4>
      </div><!-- /.box-title -->
            <table class="table" style="table-border:5px solid #000;">
                 <tr>
                  <td>Potongan Belanja</td>
                  <td align="right">
                      <?php
                            echo number_format($this->mod_transaksi->hitung_total_member('potongan_belanja',$id));
                      ?> 
                  </td>
                </tr> 

                <tr>
                  <td>Potongan Sembako</td>
                  <td align="right">
                      <?php
                            echo number_format($this->mod_transaksi->hitung_total_member('potongan_sembako',$id));
                      ?> 
                  </td>
                </tr>
                <tr>
                  <td>Potongan Handphone</td>
                  <td align="right">
                      <?php
                            echo number_format($this->mod_transaksi->hitung_total_member('potongan_icare',$id));
                      ?> 
                  </td>
                </tr> 
            </table>
        </div>
        <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Potongan Utama Anggota Koperasi </h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Date</th>
                          <th>Potongan Belanja</th>
                          <th>Potongan Sembako</th>
                          <th>Potongan Handphone</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php $no=0; foreach($data as $row => $value) { 
                      if(empty($value->id_transaksi)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->nama_anggota; ?></td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                      <td><?php echo $this->mod_member->listBulan($value->bulan_transaksi) . " " . $value->tahun_transaksi;?></td>
                      <td><?php echo number_format($value->potongan_belanja); ?></td>
                      <td><?php echo number_format($value->potongan_sembako); ?></td>
                      <td><?php echo number_format($value->potongan_icare); ?></td>
                    </tr>
                    <?php }} ?>
                  </tbody>
              </table>
              </div>
            </div><!-- /.box -->
    </div>
    <div role="tabpanel" class="tab-pane" id="lainnya">
      <div class="box">
                <div class="box-body">
                    <div class="box-title" style="margin-left:10px;">
                      <h4>Total Potongan Lainnya Anggota</h4>
                    </div><!-- /.box-title -->
                    <div class="box-body">
                        <table class="table" style="table-border:5px solid #000;">
                            <tr>
                              <td>Potongan E-Money</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_emoney',$id));
                                  ?> 
                              </td>
                            </tr> 

                             <tr>
                              <td>Potongan Motor</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_motor',$id));
                                  ?> 
                              </td>
                            </tr> 

                            <tr>
                              <td>Potongan OR</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_or',$id));
                                  ?> 
                              </td>
                            </tr>
                            <tr>
                              <td>Potongan Lainnya</td>
                              <td align="right">
                                  <?php
                                        echo number_format($this->mod_transaksi->hitung_total_member('potongan_other',$id));
                                  ?> 
                              </td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
            <div class="box" style="margin-top:30px;">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Potongan Lainnya Anggota Koperasi </h4>
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Date</th>
                          <th>Potongan E-Money</th>
                          <th>Potongan Motor</th>
                          <th>Potongan OR</th>
                          <th>Potongan Lainnya</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data as $row => $value) { 
                      if(empty($value->id_transaksi)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $value->nama_anggota; ?></td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                      <td><?php echo $this->mod_member->listBulan($value->bulan_transaksi) . " " . $value->tahun_transaksi;?></td>
                      <td><?php echo number_format($value->potongan_emoney); ?></td>
                      <td><?php echo number_format($value->potongan_motor); ?></td>
                      <td><?php echo number_format($value->potongan_or); ?></td>
                      <td><?php echo number_format($value->potongan_other); ?></td>
                    </tr>
                    <?php }} ?>
                  </tbody>
              </table>
              </div>
            </div><!-- /.box -->

    </div>
  </div>
</div>  

  <!-- <div class="col-md-10 member-content">
    <div class="box-body">
        <div class="box-title" style="margin-left:10px;">
          <h4>Total Potongan Utama Anggota</h4>
        </div><!-- /.box-title -->
        <!-- <div class="box-body">
            <table class="table" style="table-border:5px solid #000;">
                 <tr>
                  <td>Potongan Belanja</td>
                  <td align="right">
                      <?php
                            echo number_format($this->mod_transaksi->hitung_total_member('potongan_belanja',$id));
                      ?> 
                  </td>
                </tr> 

                <tr>
                  <td>Potongan Sembako</td>
                  <td align="right">
                      <?php
                            echo number_format($this->mod_transaksi->hitung_total_member('potongan_sembako',$id));
                      ?> 
                  </td>
                </tr>
                <tr>
                  <td>Potongan Handphone</td>
                  <td align="right">
                      <?php
                            echo number_format($this->mod_transaksi->hitung_total_member('potongan_icare',$id));
                      ?> 
                  </td>
                </tr> 
            </table>
        </div> -->
    <!-- </div> -->
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('member/transaksi/ajax_list_potongan')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1] }],
        "aaSorting": []

    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    var sudahsubmit = false;
    $.validate({
      form : '#form, #form-edit, #form-add',
      onSuccess : function() {
        if(sudahsubmit){
          return false;
        }
        else{
          sudahsubmit = true;
          return true;
        }
         // Will stop the submission of the form
      }
    });

});



function add_person()
{   
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Transaksi'); // Set Title to Bootstrap modal title
}

function upload()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_upload').modal('show'); // show bootstrap modal
    $('.modal-title').text('Upload Transaksi'); // Set Title to Bootstrap modal title
}

function edit_transaksi(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('app/transaksi/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="npk"').val(data.id_kop);
            $('[name="id_transaksi"').val(data.id_transaksi);
            $('[name="instalasi"]').val(data.id_instalasi);
            $('[name="bulan"]').val(data.bulan_transaksi);
            $('[name="tahun"]').val(data.tahun_transaksi);
            $('[name="sim_jip"]').val(data.simpanan_wajib);
            $('[name="sim_pok"]').val(data.simpanan_pokok);
            $('[name="sim_suk"]').val(data.simpanan_sukarela);
            $('[name="pinjaman"]').val(data.pinjaman);
            $('[name="pot_bel"]').val(data.potongan_belanja);
            $('[name="pot_sem"]').val(data.potongan_sembako);
            $('[name="pot_or"]').val(data.potongan_or);
            $('[name="shu"]').val(data.SHU);
            $('[name="icare"]').val(data.potongan_icare);
            $('[name="emoney"]').val(data.potongan_emoney);
            $('[name="motor"]').val(data.potongan_or);
            $('[name="other"]').val(data.potongan_other);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var message;

    if(save_method == 'add') {
        url = "<?php echo site_url('app/transaksi/ajax_add')?>";
        message = " Ditambahkan";
    } else {
        url = "<?php echo site_url('app/transaksi/ajax_update')?>";
        message = " Diupdate";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                alert('Data Transaksi Berhasil '+message);
                reload_table();
            }
            else
            { 
                alert('Data Transaksi Gagal '+ message + ' ( Data Salah atau Kurang lengkap ) ');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_transaksi(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('app/transaksi/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                alert('Data Transaksi Berhasil Dihapus');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>