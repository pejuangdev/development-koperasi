<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>

<div class="text-center gallery">
  <h3>PINJAMAN KOPERASI</h3>
</div>
<div class="container" style="margin-bottom:50px;">

<div class="row" style="margin-top:-20px;">
  <div class="col-md-12 member-content">
            <div class="box" style="margin-top:30px;">
               <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  <h4 style="margin-left:10px;">List Data Pinjaman</h4>
                </div><!-- /.box-title -->
                <div class="box-body table-responsive">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>ID Loan</th>
                          <th>Nama</th>
                          <th>Instalasi</th>
                          <th>Kategori Pinjaman</th>
                          <th>Jumlah Pinjaman</th>
                          <th>Tenor (bulan)</th>
                          <th>Awal Pinjaman</th>
                          <th>Status Pinjaman</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                     <?php $no=0; foreach($data as $row => $value) { 
                      if(empty($value->id_pinjaman)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $value->id_pinjaman; ?></td>
                      <td><?php echo $value->nama_anggota; ?></td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                      <td><?php echo $value->kategori; ?></td>
                      <td><?php echo number_format($value->jumlah_pinjaman); ?></td>
                      <td><?php echo $value->tenor; ?></td>
                      <td>
                      <?php

                      if($value->bulan_pinjaman == 1 || $value->bulan_pinjaman == "01"){
                        $setBulan = "Januari";
                      }
                      elseif($value->bulan_pinjaman == 2 || $value->bulan_pinjaman == "02"){
                        $setBulan = "Februari";
                      }
                      elseif($value->bulan_pinjaman == 3 || $value->bulan_pinjaman == "03"){
                        $setBulan = "Maret";
                      }
                      elseif($value->bulan_pinjaman == 4 || $value->bulan_pinjaman == "04"){
                        $setBulan = "April";
                      }
                      elseif($value->bulan_pinjaman == 5 || $value->bulan_pinjaman == "05"){
                        $setBulan = "Mei";
                      }
                      elseif($value->bulan_pinjaman == 6 || $value->bulan_pinjaman == "06"){
                        $setBulan = "Juni";
                      }
                      elseif($value->bulan_pinjaman == 7 || $value->bulan_pinjaman == "07"){
                        $setBulan = "Juli";
                      }
                      elseif($value->bulan_pinjaman == 8 || $value->bulan_pinjaman == "08"){
                        $setBulan = "Agustus";
                      }
                      elseif($value->bulan_pinjaman == 9 || $value->bulan_pinjaman == "09"){
                        $setBulan = "September";
                      }
                      elseif($value->bulan_pinjaman == 10 || $value->bulan_pinjaman == "10"){
                        $setBulan = "Oktober";
                      }
                      elseif($value->bulan_pinjaman == 11 || $value->bulan_pinjaman == "11"){
                        $setBulan = "November";
                      }
                      elseif($value->bulan_pinjaman == 12 || $value->bulan_pinjaman == "12"){
                        $setBulan = "Desember";
                      }
                      echo $setBulan.", ".($value->tahun_pinjaman); 
                      ?>
                        

                      </td>
                      <td>
                        <?php if($value->status_pinjaman == 0){
                          echo "<label class='label label-danger'>Belum Lunas</label>";
                        }else{
                          echo "<label class='label label-success'>Lunas</label>";
                        } ?>
                      </td>
                      <td>
                        <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>/index.php/member/pinjaman/detail_angsuran/<?php echo $value->id_pinjaman; ?>" title="Detail"><i class="fa fa-list"></i> Detail</a>
                      </td>
                    </tr>
                    <?php }} ?>
                  </tbody>
              </table>
              </div>
            </div><!-- /.box -->
  </div>
</div>

</div>