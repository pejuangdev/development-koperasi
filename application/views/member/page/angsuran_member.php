<?php 
   $bunga = $this->mod_archive->getBunga();
 ?>

<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>

<div class="text-center gallery">
  <h3>DETAIL PINJAMAN KOPERASI</h3>
</div>
<div class="container" style="margin-bottom:50px;">

<div class="row" style="margin-top:-20px;">

  <div class="col-md-12 member-content">
          <button class="btn btn-warning btn-red-btn" onclick="document.location.href='<?php echo base_url();?>index.php/member/pinjaman'"><i class="glyphicon glyphicon-chevron-left"></i> Kembali </button>
                
          <div class="box">
                <h4><strong>Detail Pinjaman</strong></h4>
                <div class="box-body">
                  <?php foreach ($info as $key => $value) { 
                   $sisa = $value->jumlah_pinjaman; 
                  ?>
                 <table class="table table-borderd">
                    
                    <tr>
                      <td>ID Pinjaman</td>
                      <td><?php echo $value->id_pinjaman; ?></td>
                    </tr>
                    <tr>
                      <td>Nama Anggota</td>
                      <td><?php echo $value->nama_anggota; ?></td>
                    </tr>
                    <tr>
                      <td>NPK</td>
                      <td><?php echo $value->npk; ?></td>
                    </tr>
                    <tr>
                      <td>Instalasi</td>
                      <td><?php echo $value->nama_instalasi; ?></td>
                    </tr>
                    <tr>
                      <td>ID Koperasi</td>
                      <td><?php echo $value->id_kop; ?></td>
                    </tr>
                    <tr>
                      <td>Kategori Pinjaman</td>
                      <td><?php echo $value->kategori; ?></td>
                    </tr>
                    <tr>
                      <td>Pinjaman</td>
                      <td><?php echo number_format($value->jumlah_pinjaman); ?></td>
                    </tr>
                    <tr>
                      <td>Tenor(bulan)</td>
                      <td><?php echo $value->tenor . " Bulan"; ?></td>
                    </tr>
                    <tr>
                      <td>Bunga</td>
                      <td><?php echo $bunga . " % "; ?></td>
                    </tr>
                    <tr>
                      <td>Status Pinjaman</td>
                      <td>
                        <?php if($value->status_pinjaman == 0){
                          echo "<label class='label label-danger'>Belum Lunas</label>";
                        }else{
                          echo "<label class='label label-success'>Lunas</label>";
                        } ?>
                      </td>
                    </tr>
                    <!--
                    <tr>
                      <td>Angsuran</td>
                      <td><?php echo number_format(($value->jumlah_pinjaman + ($value->jumlah_pinjaman*$value->tenor/12*$bunga/100))/$value->tenor); ?></td>
                    </tr>
                    -->

                 </table>

                 <h4><strong>Detail Angsuran</strong></h4>
                 <div class="table-responsive">
                 <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Tanggal Angsuran</th>
                          <th>Sisa Pokok</th>
                          <!--
                          <th>Angsuran Pokok</th>
                          <th>Angsuran Bunga</th>
                          -->
                          <th>Angsuran Per Bulan</th>
                          <th>Saldo Pokok</th>
                          <th>Keterangan</th>
                      </tr>
                  </thead>
                  <tbody>
                     <?php $no=0; foreach($angsuran as $row => $x) { 
                      if(empty($value->id_pinjaman)){
                          echo "<tr><td>Data yang anda cari tidak ada..!</td></tr>";break;
                        }else{$no++ 
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td>25/<?php echo $x->bulan_angsuran . '/' . $x->tahun_angsuran; ?></td>
                      <td><?php echo number_format($sisa); ?></td>
                      <!--
                      <td><?php echo number_format($value->jumlah_pinjaman/$value->tenor); ?></td>
                      <td><?php echo number_format(($value->jumlah_pinjaman/$value->tenor)*$bunga/100); ?></td>
                      -->
                      <td><?php echo number_format($x->jumlah_angsuran); ?></td>
                      <td><?php 
                        $sisa -= ($value->jumlah_pinjaman/$value->tenor);
                        echo number_format($sisa); ?></td>
                      <td><?php echo $x->keterangan; ?></td>
                    </tr>
                    <?php }} ?>
                  </tbody>
              </table>
              <?php } ?>
              </div>
              </div>
            </div><!-- /.box -->
  </div>
</div>
</div>