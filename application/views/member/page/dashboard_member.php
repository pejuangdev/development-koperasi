<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
  width: 100%;
  left: 0px;
  right: 0px;
  bottom: 0px;
  text-align: left;
  padding-top: 5px;
  padding-left: 15%;
  padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
  margin-top: 10px;
  font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
  .transition-timer-carousel .carousel-caption .carousel-caption-header {
    font-size: 36px;
  }
}
.transition-timer-carousel .carousel-indicators {
  bottom: 0px;
  margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
  z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
  -moz-transition: width 4.25s linear;
  -o-transition: width 4.25s linear;
  transition: width 4.25s linear;
}


.carousel-caption{
  margin-top: 20%;
}
</style>
<div class="text-center gallery">
  <h3>DASHBOARD MEMBER</h3>
</div>
<div class="container" style="margin-bottom:50px;">
<div class="row">
<?php foreach ($data as $value) { ?>
<div class="col-md-4">
      <div class="box-header">
          <h4 class="box-title">SELAMAT DATANG, <?php echo strtoupper($this->session->userdata('nama'));?></h4>
        </div>
        <div class="col-md-12" style="padding:5px 2px 10px 5px">
          <div class="box-body text-center">
             <img class="img-responsive" src="<?php echo base_url(); ?>/public/img/member/<?php echo $value->foto; ?>" width="200" height="200">     
          </div>
        </div>
  </div>
  <div class="col-md-4">
    <div class="box-header">
          <h4 class="box-title">PROFIL USER</h4>
        </div>
        <div class="col-md-12" style="padding:5px 2px 10px 5px">
          <div class="box-body">
                
                <table>
                  <tr>
                    <td><h5>ID Koperasi &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->id_kop; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>NPK &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->npk; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Nama &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->nama_anggota; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Alamat &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->alamat_anggota; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Tanggal Lahir &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php echo $value->tanggal_lahir; ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Status &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5><?php if ($value->status_anggota==1) echo "Aktif"; else echo "Tidak Aktif"; ?></h5></td>
                  </tr>
                </table>
                <a class="btn btn-warning btn-change-password-front" href="<?php echo site_url(); ?>/member/profil/changePass">Ubah Password</a>
                <a class="btn btn-success btn-change-profile-front btn-green-btn" href="<?php echo site_url(); ?>/member/profil/changeProfil">Ubah Profil</a>
                
              </div>
        </div>
  </div>
  <?php } ?>
  <div class="col-md-4">
      <div class="box-header">
          <h4 class="box-title">INFORMASI KOPERASI</h4>
        </div>
        <div class="col-md-12" style="padding:5px 2px 10px 5px">
          <div class="box-body text-center">
             <table class="table">
                  <tr>
                    <td><h5>Total Simpanan &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5>Rp <?php echo number_format($this->mod_member->getTotalSimpanan($this->session->userdata('id_kop'))); ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Total Pinjaman &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5>Rp <?php echo number_format($this->mod_member->getTotalPinjaman($this->session->userdata('id_kop'))); ?></h5></td>
                  </tr>
                  <tr>
                    <td><h5>Total Asset &nbsp;</h5></td>
                    <td><h5>: &nbsp;</h5></td>
                    <td><h5>Rp <?php echo number_format(($this->mod_member->getTotalSimpanan($this->session->userdata('id_kop')) - $this->mod_member->getTotalPinjaman($this->session->userdata('id_kop')))) ?></h5></td>
                  </tr>
                </table>     
          </div>
        </div>
  </div>
</div>
</div>
