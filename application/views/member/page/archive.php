<!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>UPLOAD DATA PENGAJUAN</b>
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM UPLOAD BERKAS </h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">

                  <form role="form" action="<?php echo site_url(); ?>/member/archive/upload_data" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                      <div class="form-group">
                        
                        <label for="">KTP (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="KTP dalam format JPG atau PDF" readonly>
      
                      </div>
                      <div class="form-group">
                        <label for="">Kartu Keluarga (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="Data Cost Center (HARUS FORMAT XLXS)" readonly>
                      </div>
                      <div class="form-group">
                        <label for="">Slip Gaji (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="Data Cost Center (HARUS FORMAT XLXS)" readonly>
                      </div>
                      <div class="form-group">
                        <label for="">Buku Tabungan (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="Data Cost Center (HARUS FORMAT XLXS)" readonly>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="">ID Card (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="Data Cost Center (HARUS FORMAT XLXS)" readonly>
                      </div>
                      <div class="form-group">
                        <label for="">Surat Kuasa (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="Data Cost Center (HARUS FORMAT XLXS)" readonly>
                      </div>
                      <div class="form-group">
                        <label for="">NPWP (.JPG ATAU .PDF)</label>
                          <input type="file" class="form-control" value="" name="userfile[]" placeholder="Data Cost Center (HARUS FORMAT XLXS)" readonly>
                      </div>
                    </div>
                </div><!-- /.item -->

                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>index.php/member/profil" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->