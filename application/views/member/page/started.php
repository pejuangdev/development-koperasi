<style type="text/css">
.transition-timer-carousel .carousel-caption {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
	width: 100%;
	left: 0px;
	right: 0px;
	bottom: 0px;
	text-align: left;
	padding-top: 5px;
	padding-left: 15%;
	padding-right: 15%;
}
.transition-timer-carousel .carousel-caption .carousel-caption-header {
	margin-top: 10px;
	font-size: 24px;
}

@media (min-width: 970px) {
    /* Lower the font size of the carousel caption header so that our caption
    doesn't take up the full image/slide on smaller screens */
	.transition-timer-carousel .carousel-caption .carousel-caption-header {
		font-size: 36px;
	}
}
.transition-timer-carousel .carousel-indicators {
	bottom: 0px;
	margin-bottom: 5px;
}
.transition-timer-carousel .carousel-control {
	z-index: 11;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar {
    height: 5px;
    background-color: #5cb85c;
    width: 0%;
    margin: -5px 0px 0px 0px;
    border: none;
    z-index: 11;
    position: relative;
}
.transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
    /* We make the transition time shorter to avoid the slide transitioning
    before the timer bar is "full" - change the 4.25s here to fit your
    carousel's transition time */
    -webkit-transition: width 4.25s linear;
	-moz-transition: width 4.25s linear;
	-o-transition: width 4.25s linear;
	transition: width 4.25s linear;
}


.carousel-caption{
	margin-top: 20%;
}
</style>

<div class="text-center gallery">
  <h2>MEMBER AREA</h2>
</div>
  
<div class="container" style="margin-bottom:50px;">
<div class="row">
	<div class="col-md-3">
		<?php 
      if($status==1){
        foreach ($anggota as $key => $value) {
        echo "<h4 class='text-center'>Informasi</h4><hr>";
        echo "<h5 class='text-center' style='color:#1fa51f;'>Halo ". $value->nama_anggota ."<br><br> ID Koperasi Anda ( <strong>". $value->id_kop ."</strong> )";
        }
      }else if($status ==0){
        echo "<h4 class='text-center'>Informasi</h4><hr>";
        echo "<h5 class='text-center' style='color:#f33;'>ID Koperasi Anda tidak ditemukan!<br><br> Periksa Kembali NPK Anda</h5>";
      }
      else if($status ==3){
        echo "<h4 class='text-center'>Informasi</h4><hr>";
        echo "<h5 class='text-center' style='color:#1fa51f;'>Forgot Password Berhasil!<br><br>Sedang di proses oleh admin</h5>";
      }
      else if($status ==4){
        echo "<h4 class='text-center'>Informasi</h4><hr>";
        echo "<h5 class='text-center' style='color:#f33;'>ID Koperasi Anda tidak ditemukan!<br><br> Periksa Kembali ID Koperasi Anda</h5>";
      }
    ?>
	</div>
	<div class="col-md-6">
		<p class="text-justify">Ini adalah member area anggota koperasi karyawan sigap silahkan login untuk dapat masuk ke dalam member area

Dengan menggunakan id koperasi anda dan password tahun bulan dan tanggal lahir anda contoh : jika tanggal lahir anda 1 januari 1998 maka 19980101

Dan setelah login segera lakukan penggantian password.</p>
	</div>
	<div class="col-md-3">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				   <h3 class="panel-title text-center login-panel">LOGIN ANGGOTA</h3>
				</div>
			  <div class="panel-body text-center">
			    <form method="POST" action="<?php echo base_url(); ?>index.php/member/login/proseslog" class="form-horizontal">
				  <div class="form-group">
				    <div class="col-sm-12">
				      <input type="text" class="form-control" name="idTxt" placeholder="ID Kop">
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-12">
				      <input type="password" class="form-control" name="passTxt" placeholder="Password">
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-12">
				      <button type="submit" style="width:100%; color:#fff;" class="btn btn-default">Login</button>
				    </div>
				  </div>
				</form>
				<a style="text-align: center" data-toggle="modal" data-target="#forgot" href="#forgot">Lupa Password (Klik) </a>
                <br>
                <a style="text-align: center" data-toggle="modal" href="#myModal">Cek ID Koperasi (Klik) </a>
			  </div>
			</div>
		</div>

                    <div class="modal fade" id="myModal">
                      <div class="modal-dialog modal-sm">
                          <div class="modal-content modal-content-custom">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              <h4 class="modal-title">CHECK ID KOPERASI</h4>
                            </div><div class="container"></div>
                            <div class="modal-body">
                              <form method="post" action="<?php echo base_url();?>index.php/member/login/checkId">
                                <div class="form-group">
                                  <label for="">NPK</label>
                                  <input style="border: 1px solid #afafaf" type="text" class="form-control" value="" id="" name="id" placeholder="Masukan NPK" required>
                                </div>
                                <div class="form-group">
                                  <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat btn-change-password-front">Check</button>
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                            </div>
                          </div>
                        </div>
                    </div>
            <!-- Modal -->
                    <div class="modal fade" id="forgot" role="dialog">
                      <div class="modal-dialog modal-sm">
                      
                        <!-- Modal content-->
                        <div class="modal-content modal-content-custom">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">LUPA PASSWORD</h4>
                          </div>
                          <div class="modal-body">
                            <form name="form2" action="<?php echo site_url(); ?>/member/login/forgotPass" method="POST" enctype="multipart/form-data">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="">ID Koperasi</label>
                                  <input style="border: 1px solid #afafaf" type="text" class="form-control" value="" id="" name="id" placeholder="Masukan ID Koperasi" required>
                                                 
                              </div>
                              <div class="form-group">
                                <label for="">No HP</label>
                                  <input style="border: 1px solid #afafaf" type="text" class="form-control" value="" id="" name="no_hp" placeholder="Masukan no yang dapat dihubunig">
                                                 
                              </div>
                              <div class="form-group">
                                <label for="">Email</label>
                                  <input style="border: 1px solid #afafaf" type="text" class="form-control" value="" id="" name="email" placeholder="Masukan email anda">
                                                 
                              </div>
                              <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-flat btn-change-password-front">Kirim</button>
                              </div>
                              </form>
                          </div>
                          <div class="modal-footer">
                            
                          </div>
                        </div>
                        
                      </div>
                    </div>
            <!--/#login.form-action-->
    			</div>
	</div>
</div>
</div>