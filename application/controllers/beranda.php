<?php
	Class beranda Extends CI_Controller{ 

		public function index(){
			
			$slider = $this->db->query("SELECT * FROM tbl_slider");
			$jml_slider = $this->db->query("SELECT * FROM tbl_slider")->num_rows();
			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			if($this->session->userdata('id_kop')==NULL){
				$data['header'] = $this->load->view("front/tools/header",array("about"=>$about->result()),true); 
			}else{
				$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);
			}

			$body = $this->load->view('front/page/beranda',array("slider"=>$slider->result(),"jumlah"=>$jml_slider,"about"=>$about->result()),true);
			$data['body'] = $this->load->view("front/tools/body",array("body"=>$body),true);
			$data['footer'] = $this->load->view("front/tools/footer",array("about"=>$about->result(),"random_news"=>$random_news->result()),true); 

			$this->load->view("front/master",array("data"=>$data)); 

		}

		

	}
?>