<?php
	Class Team Extends CI_Controller{


		public function __construct(){
			parent::__construct();
			$this->load->model("mod_team");
		}


		public function index(){
			$jenis_usaha = $this->db->query("SELECT * FROM tbl_usaha");
			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			if($this->session->userdata('id_kop')==NULL){
				$data['header'] = $this->load->view("front/tools/header",array("about"=>$about->result()),true); 
			}else{
				$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);
			}

			$pengawas = $this->mod_team->getPengawas();
			$pengurus = $this->mod_team->getPengurus();
			$pelaksana = $this->mod_team->getPelaksana();
			$body = $this->load->view('front/page/team',array("jenis_usaha"=>$jenis_usaha->result(),"pengawas"=>$pengawas->result(),"pengurus"=>$pengurus->result(),"pelaksana"=>$pelaksana->result(),"about"=>$about->result()),true);



			$data['body'] = $this->load->view("front/tools/body",array("body"=>$body),true);
			$data['footer'] = $this->load->view("front/tools/footer",array("about"=>$about->result(),"random_news"=>$random_news->result()),true); 

			$this->load->view("front/master",array("data"=>$data)); 
		}


	}
	
?>