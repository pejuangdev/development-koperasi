<?php
	Class kalkulator Extends CI_Controller{
		public function index(){
			$jenis_usaha = $this->db->query("SELECT * FROM tbl_usaha");
			$slider = $this->db->query("SELECT * FROM tbl_slider");
			$jml_slider = $this->db->query("SELECT * FROM tbl_slider")->num_rows();
			$about = $this->db->query("SELECT * FROM tbl_about");
			$news = $this->db->query("SELECT * FROM tbl_news where status_news='1' order by tanggal_news desc limit 0,6");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			if($this->session->userdata('id_kop')==NULL){
				$data['header'] = $this->load->view("front/tools/header",array("about"=>$about->result()),true); 
			}else{
				$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);
			}

			$body = $this->load->view('front/page/kalkulator',array("jenis_usaha"=>$jenis_usaha->result(),"slider"=>$slider->result(),"jumlah"=>$jml_slider,"about"=>$about->result(),"news"=>$news->result()),true);
			$data['body'] = $this->load->view("front/tools/body",array("body"=>$body),true);
			$data['footer'] = $this->load->view("front/tools/footer",array("about"=>$about->result(),"random_news"=>$random_news->result()),true); 

			$this->load->view("front/master",array("data"=>$data)); 
		} 

		public function hitung(){
			$bunga = 0;$hasil =0;
			$get_bunga = $this->db->query("SELECT bunga from tbl_about")->result();
			foreach ($get_bunga as $key => $value) {
				$bunga = $value->bunga;
			}

			$pinjaman = mysql_real_escape_string($this->input->post('pinjaman'));
			$tenor = mysql_real_escape_string($this->input->post('tenor'));

			$hasil = ($pinjaman + ($pinjaman*$tenor/12*$bunga/100))/$tenor;
			$pendapatan_minimal = $hasil/0.3;

			$jenis_usaha = $this->db->query("SELECT * FROM tbl_usaha");
			$about = $this->db->query("SELECT * FROM tbl_about");
			$news = $this->db->query("SELECT * FROM tbl_news where status_news='1' order by tanggal_news desc limit 0,6");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,6");
			$slider = $this->db->query("SELECT * FROM tbl_slider");
			$jml_slider = $this->db->query("SELECT * FROM tbl_slider")->num_rows();
			

			if($this->session->userdata('id_kop')==NULL){
				$data['header'] = $this->load->view("front/tools/header",array("about"=>$about->result()),true); 
			}else{
				$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);
			}
			
			$body = $this->load->view('front/page/kalkulator',array("jenis_usaha"=>$jenis_usaha->result(),"slider"=>$slider->result(),"jumlah"=>$jml_slider,"about"=>$about->result(),"news"=>$news->result(),"hasil"=>$hasil,"pendapatan"=>$pendapatan_minimal,"pinjaman"=>$pinjaman,"tenor"=>$tenor),true);
			$data['body'] = $this->load->view("front/tools/body",array("body"=>$body),true);
			$data['footer'] = $this->load->view("front/tools/footer",array("about"=>$about->result(),"random_news"=>$random_news->result()),true); 

			$this->load->view("front/master",array("data"=>$data)); 


		} 
		

	}
?>