<?php
class Angsuran extends CI_Controller {

	var $bulan = "";
	var $tahun = "";

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_angsuran');
		$this->load->model('mod_pinjaman');
		$this->load->library("Excel_reader");
	}

	public function index(){
		
		$type = $this->input->get('type');
		if(empty($type)){
			$type = "simpanan";
		}

		$month = $this->input->get('bulan');
		$year = $this->input->get('tahun');

		if(!empty($month)&&!empty($year)){
			$this->bulan = $month;
			$this->tahun = $year;
		}else{
			$data = $this->mod_angsuran->getMax();
		 	foreach ($data as $key => $value) {
		 		$this->bulan = $value->bulan;
		 		$this->tahun = $value->tahun;
		 	}
		}

		$data_pinjaman = $this->mod_pinjaman->getLoan();

		$data['header'] = $this->load->view('loan/layout/header','',true);

		$body = $this->load->view('loan/page/angsuran', array('pinjaman'=>$data_pinjaman,'bulan'=>$this->bulan,'tahun'=>$this->tahun),true);
		
		$data['body'] = $this->load->view('loan/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('loan/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));			
	}

    public function remove_all($month="",$year=""){
        $this->db->where(array('bulan_angsuran'=>$month,'tahun_angsuran'=>$year));
        $this->db->delete('tbl_angsuran');
        redirect(site_url()."/loan/angsuran");
    }

	public function ajax_list($month="",$year="")
	{	
		
		$this->mod_angsuran->setDate($month,$year);
		$list = $this->mod_angsuran->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $angsuran) {
			
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $angsuran->id_pinjaman;
			$row[] = $angsuran->npk;
			$row[] = $angsuran->nama_anggota;
			$row[] = number_format($angsuran->jumlah_angsuran);
			$row[] = $this->mod_angsuran->listBulan($angsuran->bulan_angsuran) . "-" . $angsuran->tahun_angsuran;
			$row[] = $angsuran->keterangan;
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_angsuran('."'".$angsuran->id_angsuran."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_angsuran('."'".$angsuran->id_angsuran."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_angsuran->count_all(),
						"recordsFiltered" => $this->mod_angsuran->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_angsuran->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->form_validation->set_rules('id_pinjaman', 'id_pinjaman', 'trim|required|xss_clean');
        $this->form_validation->set_rules('jumlah', 'Angsuran', 'number|required|xss_clean');
        $this->form_validation->set_rules('bulan','Bulan','required|number');
        $this->form_validation->set_rules('tahun','Tahun','required|number');
        if ($this->form_validation->run() != FALSE)
        {
        $id_pinjaman = $this->input->post('id_pinjaman');
		$jumlah = $this->input->post('jumlah');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$keterangan = $this->input->post('keterangan');
		
		$data = array(
					"id_angsuran"=> "",
					"id_pinjaman"=> $id_pinjaman,
					"jumlah_angsuran"=> $jumlah,
					"bulan_angsuran"=>$bulan,
					"tahun_angsuran"=>$tahun,
					"keterangan" =>$keterangan
					);

		$insert = $this->mod_angsuran->addAngsuran($data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_update()
	{
		$this->form_validation->set_rules('id_pinjaman', 'id_pinjaman', 'trim|required|xss_clean');
        $this->form_validation->set_rules('jumlah', 'Angsuran', 'number|required|xss_clean');
        $this->form_validation->set_rules('bulan','Bulan','required|number');
        $this->form_validation->set_rules('tahun','Tahun','required|number');
        if ($this->form_validation->run() != FALSE)
        {
        $id_pinjaman = $this->input->post('id_pinjaman');
		$jumlah = $this->input->post('jumlah');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$keterangan = $this->input->post('keterangan');
		
		$data = array(
					"id_angsuran"=> "",
					"id_pinjaman"=> $id_pinjaman,
					"jumlah_angsuran"=> $jumlah,
					"bulan_angsuran"=>$bulan,
					"tahun_angsuran"=>$tahun,
					"keterangan" =>$keterangan
					);

		$this->mod_angsuran->update(array('id_angsuran' => $this->input->post('id_angsuran')), $data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_delete($id)
	{
		$this->mod_angsuran->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function upload(){
		
		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/transact_upload', array(),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));
	}

	public function upload_data(){

			$tahun = $this->input->post("tahun");  
			$bulan = $this->input->post("bulan");

			$name = $_FILES["userfile"]["name"];
	        $name_path = $_FILES["userfile"]["tmp_name"];

	        $this->excel_reader->read($name_path);

	        $worksheet = $this->excel_reader->sheets[0];
	        $baris = $worksheet['numRows'];


	        $numRows = $worksheet['numRows']; // ex: 14
	        $numCols = $worksheet['numCols']; // ex: 4
	        $cells = $worksheet['cells']; // the 1st row are usually the field's name

	        $x = 0;

	        for ($i=2; $i <=$numRows ; $i++) { 
	        	$x +=1;

				$id_pinjaman = $cells[$i][1];
				$jumlah_angsuran = $cells[$i][2];
				$keterangan = $cells[$i][3];
				
				$data = array(
					"id_angsuran"=> "",
					"id_pinjaman"=>$id_pinjaman,
					"jumlah_angsuran"=>$jumlah_angsuran,
					"bulan_angsuran" =>$bulan,
					"tahun_angsuran"=>$tahun,
					"keterangan"=>$keterangan);

				$insert = $this->mod_angsuran->addAngsuran($data);
			}
			if($insert){
				echo "<script>alert('Berhasil Input Angsuran');document.location.href='".site_url()."/loan/angsuran';</script>";
			}else{
				echo "<script>alert('Gagal Input Angsuran');document.location.href='".site_url()."/loan/angsuran';</script>";
			}
			
		} 

	// public function upload_data(){

	// 	$tahun = $this->input->post("tahun");  
	// 	$bulan = $this->input->post("bulan");	

	// 	$this->load->library('upload');
		    
 //        $dt = $_FILES['userfile']['name'];
 //        $cx = $_FILES['userfile']['tmp_name'];
	//     $file_ext = explode(".", $dt);
 //        $file_ext = $file_ext[1];
 //    	$filename = time().".".$file_ext;
 //    	move_uploaded_file($cx, "public/file/transaksi/".$filename);

 //    	$excel = 'public/file/transaksi/'.$filename;

 //    	$this->load->library('excel');

 //    	$objPHPExcel = PHPExcel_IOFactory::load($excel);

 //    	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	// 		$highrow = $worksheet->getHighestRow();
	// 		for($row=2;$row<=$highrow;$row++){
	// 			$npk = $worksheet->getCellByColumnAndRow(2,$row)->getValue();
	// 			$instalasi = $worksheet->getCellByColumnAndRow(3,$row)->getValue();
	// 			$sim_jip = $worksheet->getCellByColumnAndRow(4,$row)->getValue();
	// 			$sim_kok = $worksheet->getCellByColumnAndRow(5,$row)->getValue();
	// 			$sim_suk = $worksheet->getCellByColumnAndRow(6,$row)->getValue();
	// 			$pinjaman = $worksheet->getCellByColumnAndRow(7,$row)->getValue();
	// 			$pot_bel = $worksheet->getCellByColumnAndRow(8,$row)->getValue();
	// 			$pot_sem = $worksheet->getCellByColumnAndRow(9,$row)->getValue();
	// 			$pot_or = $worksheet->getCellByColumnAndRow(10,$row)->getValue();
							

	// 			$data = array(
	// 				"id_transaksi"=> "",
	// 				"bulan_transaksi"=>$bulan,
	// 				"tahun_transaksi"=>$tahun,
	// 				"npk" =>$npk,
	// 				"id_instalasi"=>$instalasi,
	// 				"simpanan_wajib"=>$sim_jip,
	// 				"simpanan_pokok"=>$sim_kok,
	// 				"simpanan_sukarela"=>$sim_suk,
	// 				"pinjaman"=>$pinjaman,
	// 				"potongan_belanja"=>$pot_bel,
	// 				"potongan_sembako"=>$pot_sem,
	// 				"potongan_or"=>$pot_or
	// 				);

	// 			$insert = $this->mod_angsuran->addTrans($data);
	// 		}
	// 	}
	// 	unlink($excel);
	// 	if($insert){
	// 		echo "<script>alert('Berhasil Input Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
	// 	}else{
	// 		echo "<script>alert('Gagal Input Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
	// 	}


    	
	// }

	public function save_data(){
		$npk = $this->input->post('npk');
		$instalasi = $this->input->post('instalasi');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sim_jip = $this->input->post('sim_jip');
		$sim_kok = $this->input->post('sim_kok');
		$sim_suk = $this->input->post('sim_suk');
		$pot_bel = $this->input->post('pot_bel');
		$pot_sem = $this->input->post('pot_sem');
		$pot_or = $this->input->post('pot_or');

		$data = array(
					"id_transaksi"=> "",
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"npk" =>$npk,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or
					);

		$insert = $this->mod_angsuran->addTrans($data);

		if($insert){
			echo "<script>alert('Berhasil Input Data Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}else{
			echo "<script>alert('Gagal Input Data Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}

	}

	public function edit($id){
		$data_anggota = $this->mod_angsuran->getListMember();
		$data_instalasi = $this->mod_instalasi->getListInstalasi();
		$data_transaksi = $this->mod_angsuran->getWhereTrans($id);

		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/edit_transaksi', array('data'=>$data_transaksi,'anggota'=>$data_anggota,'instalasi'=>$data_instalasi),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));
	}

	public function update_data($id){
		$npk = $this->input->post('npk');
		$instalasi = $this->input->post('instalasi');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sim_jip = $this->input->post('sim_jip');
		$sim_kok = $this->input->post('sim_kok');
		$sim_suk = $this->input->post('sim_suk');
		$pot_bel = $this->input->post('pot_bel');
		$pot_sem = $this->input->post('pot_sem');
		$pot_or = $this->input->post('pot_or');

		$data = array(
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"npk" =>$npk,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or
					);

		$update = $this->mod_angsuran->updateTrans($data,$id);

		if($update){
			echo "<script>alert('Berhasil Update Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}else{
			echo "<script>alert('Gagal Update Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}
	}

	public function delete($id){
		$delete = $this->mod_angsuran->delTrans($id);
		if($delete){
			echo "<script>alert('Berhasil Hapus Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}else{
			echo "<script>alert('Gagal Hapus Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}
	}

}

?>