<?php
class Appliance extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_appliance');
		$this->load->model('mod_cost_center');
		$this->load->model('mod_archive');
	}

	public function index(){

		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('loan/page/appliance', array(),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('loan/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));				

	}

	public function ajax_list()
	{
		$list = $this->mod_appliance->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $appliance) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $appliance->id_kop;
			$row[] = $appliance->nama_anggota;
			$row[] = $appliance->nama_instalasi;
			$row[] = number_format($appliance->value_of);
			$row[] = $appliance->time_of;
			$row[] = $appliance->create_date;
			$row[] = $this->convertStatus($appliance->status_appliance);
			
			//add html for action
			$row[] = 
				  '<a class="btn btn-sm btn-success" href="'. base_url() .'index.php/loan/appliance/detail_appliance/'.$appliance->id_appliance.'" title="Detail"><i class="fa fa-list"></i></a>
				  <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_cost('."'".$appliance->id_appliance."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_cost('."'".$appliance->id_appliance."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_appliance->count_all(),
						"recordsFiltered" => $this->mod_appliance->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function detail_appliance($id){
		$id_kop = $this->session->userdata('id_kop');
		$data_aplikasi = $this->mod_archive->getDetailPengajuan($id);
		$berkas = $this->mod_archive->checkBerkas($id_kop);
		$data_berkas = $this->mod_archive->getBerkas($id_kop);

		$data_anggota = $this->mod_archive->getInfo($id_kop);

		$data['header'] = $this->load->view('member/layout/header','',true);

		$body = $this->load->view('loan/page/detail', array('berkas'=>$berkas,'data_berkas'=>$data_berkas,'applikasi'=>$data_aplikasi,'id_appliance'=>$id),true);

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('loan/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_appliance->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_delete($id)
	{
		$this->mod_archive->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function approve_applikasi(){
		$id_appliance = $this->input->post('id_appliance');
		$nilai = $this->input->post('nilai');
		$tenor = $this->input->post('tenor_baru');
		$tgl_cair = $this->input->post('tgl_cair');
		$status = $this->input->post('status');
		$nama = $this->input->post('nama');

		$data = array(
				'nilai_persetujuan' => $nilai ,
				'waktu_persetujuan' => $tenor,
				'rencana_pencairan' => $tgl_cair,
				'nama_menyetujui' => $nama,
				'status_appliance' => $status 
				);
		$update = $this->mod_archive->update(array('id_appliance' => $id_appliance), $data);

		if($update){
			echo "<script>alert('Pengajuan berhasil di Approve!');document.location.href='".site_url()."/loan/appliance';</script>";
		}else{
			echo "<script>alert('Pengajuan gagal di Approve!');document.location.href='".site_url()."/loan/appliance';</script>";
		}
	}

	public function convertStatus($status){
		$result = "";
		if($status == 0)
	    { $result = "<label class='label label-info'>Progress</label>";}
	    else if($status == 1)
	    { $result = "<label class='label label-success'>Diterima</label>";}
	    else if($status == 2)
	    { $result = "<label class='label label-danger'>Ditolak</label>";} 
	    else if($status == 3)
        { $result = "<label class='label label-warning'>Kurang Berkas</label>";}
		return $result;
	}

}

?>