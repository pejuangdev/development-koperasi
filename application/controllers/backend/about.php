<?php
class About extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataAbout = $this->mod_about->getAbout();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/about', array('data'=>$dataAbout),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_slider', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function edit($id){

		$dataEdit = $this->mod_about->getWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_about',array("data"=>$dataEdit,"id"=>$id),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));		
	}

	public function do_edit(){

		$filename = "";

		$id = $this->input->post('id');
		$visi = $this->input->post('visi');
		$misi = $this->input->post('misi');
		$sejarah = $this->input->post('sejarah');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$fax = $this->input->post('fax');
		$alamat = $this->input->post('alamat');
		$deskripsi = $this->input->post('desk');
		$bunga = $this->input->post('bunga');
		$files = $_FILES;

		$this->load->library('upload');

		if($_FILES["userfile"]["error"]==0){

			$file_ext = explode(".", $files['userfile']['name']);
	        $file_ext = $file_ext[1];

        	$filename = time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'], "public/icon/".$filename);
		}

		else{
			$filename = $this->input->post('foto');
		}

		$data = array(
			"deskripsi_about"=>$deskripsi,
			"email_about"=>$email,
			"phone_about"=>$phone,
			"fax_about"=>$fax,
			"address_about"=>$alamat,
			"logo_about"=>$filename,
			"sejarah_about"=>$sejarah,
			"visi_about"=>$visi,
			"misi_about"=>$misi,
			"bunga"=>$bunga
		);

		$updateData = $this->mod_about->updateAbout($data,$id);

		if($updateData){
			echo  "<script language=\"javascript\">alert('Berhasil Edit About !');location='".base_url()."index.php/backend/about';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/staff';</script>";
    	}
	}

	public function delete($id){
		$this->mod_about->deleteAbout($id);
		redirect("backend/delete");
	}

	public function add_misi(){
		$id = $this->input->post('id');
		$deskripsi = $this->input->post('deskripsi');
		$this->mod_about->add_misi(array("id_misi"=>'',"deskripsi_misi"=>$deskripsi));
		redirect("backend/about/edit/$id");
	}


}

?>