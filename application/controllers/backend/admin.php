<?php
class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataAdmin = $this->mod_admin->getAdmin();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/admin', array('data'=>$dataAdmin),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_admin', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function save_data(){
		$nama = $this->input->post("nama");
		$email = $this->input->post("email");
		$telp = $this->input->post("phone");
		$username = $this->input->post("uname");
		$pass = $this->input->post("password");
		$alamat = $this->input->post("alamat");
		$status = $this->input->post("status");

		$data = array(
			"id_user"=>"",
			"user_name"=>$nama,
			"user_email"=>$email,
			"user_phone"=>$telp,
			"user_address"=>$alamat,
			"username"=>$username,
			"password"=>$pass,
			"create_by"=>$this->session->userdata("id_user"),
			"status"=>$status
			);
		$x = $this->mod_admin->addAdmin($data);

		if($x){
			echo "<script language=\"javascript\">alert('Berhasil Menambah Admin');location='".base_url()."index.php/backend/admin';</script>";
        }else{
        	echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/admin';</script>"; 
        }
	}

	public function edit($id){

		$dataEdit = $this->mod_admin->getWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_admin',array("data"=>$dataEdit),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));
	}

	public function do_edit(){

		$id = $this->input->post('id');
		$nama = $this->input->post("nama");
		$email = $this->input->post("email");
		$telp = $this->input->post("phone");
		$username = $this->input->post("uname");
		$pass = $this->input->post("password");
		$alamat = $this->input->post("alamat");
		$status = $this->input->post("status");

		$data = array(
			"user_name"=>$nama,
			"user_email"=>$email,
			"user_phone"=>$telp,
			"user_address"=>$alamat,
			"username"=>$username,
			"password"=>$pass,
			"update_date"=>date('Y-m-d H:i:s'),
    		"update_by"=>$this->session->userdata("id_user"),
    		"status"=>$status
		);


		$updateData = $this->mod_admin->editAdmin($data,$id);
		
		if ($updateData) {
			echo  "<script language=\"javascript\">alert('Berhasil Edit Admin !');location='".base_url()."index.php/backend/admin';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/admin';</script>";
    	}

	}

	public function delete($id){
		$this->mod_admin->deleteAdmin($id);
		redirect("backend/admin");
	}


}

?>