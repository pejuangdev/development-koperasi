<?php
class Product extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index($offset=0){

		$config['uri_segment'] = 4;
		$config['base_url'] = base_url()."index.php/backend/product/index"; 
		$config['total_rows'] = $this->mod_product->count_all();
		$config['per_page'] = 20;

		$dataProduct = $this->mod_product->getProductAdmin($config['per_page'], $offset);

		 // CSS Bootstrap               
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';            
		$config['prev_link'] = '«';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '»';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
         // Akhir CSS

		$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );           
		$this->pagination->initialize($config);
		
		$pages = $this->pagination->create_links();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/product', array('data'=>$dataProduct,"pages"=>$pages),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$dataCategory = $this->mod_category->getCategoryAdmin();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_product', array("data"=>$dataCategory),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function edit($id){

		$dataEditProduct = $this->mod_product->getEditProductAdmin($id);

		//$dataEditProduct = $this->mod_product->getEditProduct($id);

		$id_prod = array();
		foreach($this->mod_product->GetProduk($id)->result_array() as $prod){
			$id_prod[] = $prod['id_cake_category'];
		}


		$dataCategory = $this->mod_category->getCategoryAll();

		$otherData = array(
			"id_produk" => $dataEditProduct[0]["id_product"],
			"nama_produk"=> $dataEditProduct[0]["product_name"],
			"price"=> $dataEditProduct[0]["price"],
			"id_cat"=>$id_prod,
			"cat"=> $dataEditProduct[0]["cake_category_title"],
			"allCat" => $this->mod_category->getCategoryAll(),
			"status"=> $dataEditProduct[0]["status"],
			"main_img"=>$dataEditProduct[0]["product_image"],
			"first_img"=>$dataEditProduct[0]["img_thumb_first"],
			"second_img"=>$dataEditProduct[0]["img_thumb_second"],
			"deskripsi"=>$dataEditProduct[0]["product_description"]
		);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_product', $otherData, true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));

	}

	public function save_data(){
			$id = $this->input->post('id');
			$nama_produk = $this->input->post('nama');
			$harga = $this->input->post('harga');
			$kategori = $this->input->post('kategori');
			$status = $this->input->post('status');
			$konten = $this->input->post('konten');
			//$foto_temporary = $_FILES['userfile[]']['name'];

			$files = $_FILES;
		    $cpt = count($_FILES['userfile']['name']);

		    echo $cpt;

		    $gambar1 = "";
		    $gambar2 = "";
			
			$this->load->library('upload');

		    for($i=0; $i<$cpt; $i++)
		    {
	            $dt[] = $files['userfile']['name'][$i];
		        $cx[] = $files['userfile']['tmp_name'][$i];
			    $file_ext = explode(".", $dt[$i]);
                $file_ext = $file_ext[1];
                if($i==1){
                	$filename[$i] = time()."-".time().".".$file_ext;
                	move_uploaded_file($cx[$i], "asset/la_product/img_thumb/".$filename[$i]);
                	$dt[$i] = $files['userfile']['name'][$i];
                }
                elseif($i==2){
                	$filename[$i] = time()."-".time()."-".time().".".$file_ext;
                	move_uploaded_file($cx[$i], "asset/la_product/img_thumb/".$filename[$i]);
                	$dt[$i] = $files['userfile']['name'][$i];
                }
                else{
                	$filename[$i] = time().".".$file_ext;
                	move_uploaded_file($cx[$i], "asset/la_product/".$filename[$i]);
                	$dt[$i] = $files['userfile']['name'][$i];
                }
		    }

        	$a = $this->mod_product->addProductData(array(
                        "id_product"=>$id,    
                        "product_name"=>$nama_produk,
                        "product_description"=>$konten,
                        "price"=>$harga,
                        "product_image"=>$filename[0],
                        "img_thumb_first"=>$filename[1],
                        "img_thumb_second"=>$filename[2],
                        "id_cake_category"=>$kategori,
                        "create_by"=>$this->session->userdata('id_user'),
                        "status"=>$status));

        	if($a){
        		echo "<script language=\"javascript\">alert('Berhasil Tambah Produk Baru!');location='".base_url()."index.php/backend/product';</script>";
        	}
        	else{
        		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/news';</script>";
        	}
	}

	public function delete($id){
		$this->mod_product->deleteProduct($id);
		redirect("backend/product");
	}

	public function do_edit(){

		$filename[] = array();

		$idFix = $this->input->post('idFixed');
		$produk = $this->input->post('nama');
		$harga = $this->input->post('harga');
		$kategori = $this->input->post('kategori');
		$status = $this->input->post('status');
		$deskripsi = $this->input->post('konten');
		
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);

		//$xpt = count($this->input->post('foto'));

	    
		
		$this->load->library('upload');

		if($_FILES['userfile']["error"][0] != 0 && $_FILES['userfile']["error"][1] != 0 && $_FILES['userfile']["error"][2] != 0){
			$filename[0] = $this->input->post('foto')[0];
			$filename[1] = $this->input->post('foto')[1];
			$filename[2] = $this->input->post('foto')[2];			
		}

		elseif ($_FILES['userfile']["error"][0] == 0 && $_FILES['userfile']["error"][1] != 0 && $_FILES['userfile']["error"][2] != 0){
			//$file1 = $files['userfile']['name'][0]."<br>";
			//echo "ada file 1"."<br>";
			
			$file_ext = explode(".", $files['userfile']['name'][0]);
	        $file_ext = $file_ext[1];

        	$filename[0] = time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'][0], "asset/la_product/".$filename[0]);
        	

        	$filename[1] = $this->input->post('foto')[1];
			$filename[2] = $this->input->post('foto')[2];
        	//$dt[$i] = $files['userfile']['name'][$i];
		}

		elseif ($_FILES['userfile']["error"][0] != 0 && $_FILES['userfile']["error"][1] == 0 && $_FILES['userfile']["error"][2] != 0){
			$file_ext = explode(".", $files['userfile']['name'][1]);
	        $file_ext = $file_ext[1];

        	$filename[1] = time()."-".time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'][1], "asset/la_product/img_thumb/".$filename[1]);
        	

        	$filename[0] = $this->input->post('foto')[0];
			$filename[2] = $this->input->post('foto')[2];
		}

		elseif ($_FILES['userfile']["error"][0] != 0 && $_FILES['userfile']["error"][1] != 0 && $_FILES['userfile']["error"][2] == 0){
			$file_ext = explode(".", $files['userfile']['name'][2]);
	        $file_ext = $file_ext[1];

        	$filename[2] = time()."-".time()."-".time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'][2], "asset/la_product/img_thumb/".$filename[2]);
        	

        	$filename[0] = $this->input->post('foto')[0];
			$filename[1] = $this->input->post('foto')[1];
		}

		elseif ($_FILES['userfile']["error"][0] == 0 && $_FILES['userfile']["error"][1] == 0 && $_FILES['userfile']["error"][2] == 0){
			
				$file_ext0 = explode(".", $files['userfile']['name'][0]);
		        $file_ext0 = $file_ext0[1];

		        $filename[0] = time().".".$file_ext0;
	        	move_uploaded_file($files['userfile']['tmp_name'][0], "asset/la_product/".$filename[0]);

				$file_ext1 = explode(".", $files['userfile']['name'][1]);
		        $file_ext1 = $file_ext1[1];

		        $filename[1] = time()."-".time().".".$file_ext1;
        		move_uploaded_file($files['userfile']['tmp_name'][1], "asset/la_product/img_thumb/".$filename[1]);

				$file_ext2 = explode(".", $files['userfile']['name'][2]);
		        $file_ext2 = $file_ext2[1];

		        $filename[2] = time()."-".time()."-".time().".".$file_ext2;
        		move_uploaded_file($files['userfile']['tmp_name'][2], "asset/la_product/img_thumb/".$filename[2]);	
        	
		}

		elseif ($_FILES['userfile']["error"][0] == 0 && $_FILES['userfile']["error"][1] == 0 && $_FILES['userfile']["error"][2] != 0){

				$file_ext0 = explode(".", $files['userfile']['name'][0]);
		        $file_ext0 = $file_ext0[1];

		        $filename[0] = time().".".$file_ext0;
	        	move_uploaded_file($files['userfile']['tmp_name'][0], "asset/la_product/".$filename[0]);

				$file_ext1 = explode(".", $files['userfile']['name'][1]);
		        $file_ext1 = $file_ext1[1];

		        $filename[1] = time()."-".time().".".$file_ext1;
        		move_uploaded_file($files['userfile']['tmp_name'][1], "asset/la_product/img_thumb/".$filename[1]);

        	$filename[2] = $this->input->post('foto')[2];        	
		}

		elseif ($_FILES['userfile']["error"][0] == 0 && $_FILES['userfile']["error"][1] != 0 && $_FILES['userfile']["error"][2] == 0){

			
				$file_ext0 = explode(".", $files['userfile']['name'][0]);
		        $file_ext0 = $file_ext0[1];

		        $filename[0] = time().".".$file_ext0;
	        	move_uploaded_file($files['userfile']['tmp_name'][0], "asset/la_product/".$filename[0]);
			

				$file_ext2 = explode(".", $files['userfile']['name'][2]);
		        $file_ext2 = $file_ext2[1];

		        $filename[2] = time()."-".time()."-".time().".".$file_ext2;
        		move_uploaded_file($files['userfile']['tmp_name'][2], "asset/la_product/img_thumb/".$filename[2]);

        	$filename[1] = $this->input->post('foto')[1];        	
		}

		elseif ($_FILES['userfile']["error"][0] != 0 && $_FILES['userfile']["error"][1] == 0 && $_FILES['userfile']["error"][2] == 0){

				$ext = explode(".", $files['userfile']['name'][1]);
		        $file_ext[1] = $ext[1];   

		        $filename[1] = time()."-".time().".".$file_ext[1];
        		move_uploaded_file($files['userfile']['tmp_name'][1], "asset/la_product/img_thumb/".$filename[1]);   

				$ext2 = explode(".", $files['userfile']['name'][2]);
		        $file_ext[2] = $ext2[1];

		        $filename[2] = time()."-".time()."-".time().".".$file_ext[2];
        		move_uploaded_file($files['userfile']['tmp_name'][2], "asset/la_product/img_thumb/".$filename[2]);

				$filename[0] = $this->input->post('foto')[0]; 

		}

		$updt = $this->mod_product->editProduct(
	    	array(
	    		"product_name"=>$produk,
	    		"product_description"=>$deskripsi,
	    		"price"=>$harga,
	    		"product_image"=>$filename[0],
	    		"img_thumb_first"=>$filename[1],
	    		"img_thumb_second"=>$filename[2],
	    		"id_cake_category"=>$kategori,
	    		"update_date"=>date('Y-m-d H:i:s'),
	    		"update_by"=>$this->session->userdata("id_user"),
	    		"status"=>$status),$idFix);

	    if($updt){
	    	echo  "<script language=\"javascript\">alert('Berhasil Edit Produk !');location='".base_url()."index.php/backend/product';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/news';</script>";
    	}	    
	}

}

?>