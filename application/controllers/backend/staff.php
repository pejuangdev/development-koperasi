<?php
class Staff extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataFaq = $this->mod_staff->getStaff();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/staff', array('data'=>$dataFaq),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){
		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_staff','',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));
	}

	public function edit($id){

		$dataEdit = $this->mod_staff->getWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_staff',array("data"=>$dataEdit),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));
	}

	public function save_data(){
		$nama = $this->input->post('nama');
		$jabatan = $this->input->post('jabatan');
		$email = $this->input->post('email');
		$deskripsi = $this->input->post('deskripsi');
		$status = $this->input->post('status');
		$files = $_FILES;
		$filename = $_FILES['userfile']['name'];

		if(empty($filename)){
			$filename = "default.png";
		}else{
			$file_ext = explode(".", $files['userfile']['name']);
	        $file_ext = $file_ext[1];

	    	$filename = time().".".$file_ext;
	    	move_uploaded_file($files['userfile']['tmp_name'], "public/img/staff/".$filename);
	    }

    	$data = array(
			"nama_staff"=>$nama,
			"jabatan"=>$jabatan,
			"deskripsi_staff"=>$deskripsi,
			"email_staff"=>$email,
			"foto_staff"=>$filename,
			"status"=>$status
		);

		$saveData = $this->mod_staff->addStaff($data);

		if($saveData){
			echo  "<script language=\"javascript\">alert('Berhasil Tambah Staff !');location='".base_url()."index.php/backend/staff';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/staff';</script>";
    	}

	}

	public function do_edit(){

		$filename = "";

		$id=$this->input->post('id');
		$nama = $this->input->post('nama');
		$jabatan = $this->input->post('jabatan');
		$email = $this->input->post('email');
		$deskripsi = $this->input->post('deskripsi');
		$status = $this->input->post('status');

		$files = $_FILES;

		$this->load->library('upload');

		if($_FILES["userfile"]["error"]==0){

			$file_ext = explode(".", $files['userfile']['name']);
	        $file_ext = $file_ext[1];

        	$filename = time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'], "public/img/staff/".$filename);
		}

		else{
			$filename = $this->input->post('foto');
		}

		$data = array(
			"nama_staff"=>$nama,
			"jabatan"=>$jabatan,
			"deskripsi_staff"=>$deskripsi,
			"email_staff"=>$email,
			"foto_staff"=>$filename,
			"status"=>$status
		);


		$updateData = $this->mod_staff->editStaff($data,$id);
		
		if ($updateData) {
			echo  "<script language=\"javascript\">alert('Berhasil Edit Staff !');location='".base_url()."index.php/backend/staff';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/staff';</script>";
    	}

	}

	public function delete($id){
		$this->mod_staff->deleteStaff($id);
		redirect("backend/staff");
	}


}

?>