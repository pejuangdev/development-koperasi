<?php
class Contact extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataContact = $this->mod_contact->getContactAdmin();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/contact', array('data'=>$dataContact),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function detail($id){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$data_outbox = $this->db->query("SELECT * FROM tbl_outbox where id_inbox='$id'")->result();

		$dataWhere = $this->mod_contact->getWhere($id);

		$body = $this->load->view('backend/page/detail_contact', array("data"=>$dataWhere,"outbox"=>$data_outbox),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function send_mail(){
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$message = $this->input->post('message');

		$data  = array(
			"id_outbox"=>"",
			"id_inbox"=>$id,
			"outbox"=>$message
		);

		$save = $this->mod_contact->saveMail($data);
		
		if($save){
			$this->load->library('email');

            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");
            $this->email->from('info@koperasi-sigap.com'); // change it to yours
            $this->email->to($email);// change it to yours
            $this->email->subject('[KOPERASI SIGAP]');
            $this->email->message($message);
            $this->email->send();

            echo  "<script language=\"javascript\">alert('Berhasil Kirim Email Ke $email !');location='".base_url()."index.php/backend/contact';</script>";
		}else{

			echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/contact';</script>";
		}


	}


}

?>