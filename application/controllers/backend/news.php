<?php
class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index($offset=0){

		$config['uri_segment'] = 4;
		$config['base_url'] = base_url()."index.php/backend/news/index"; 
		$config['total_rows'] = $this->mod_news->count_all();
		$config['per_page'] = 10;

		$dataNews = $this->mod_news->getNewsAdmin($config['per_page'], $offset);

		// CSS Bootstrap               
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';            
		$config['prev_link'] = '«';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '»';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
         // Akhir CSS

		$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );           
		$this->pagination->initialize($config);

		$pages = $this->pagination->create_links();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/news', array('data'=>$dataNews,"pages"=>$pages),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_news', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function save_data(){
			$judul = $this->input->post('judul');
			$highlight = $this->input->post('highlight');
			$konten = $this->input->post('konten');
			$status = $this->input->post('status');
			//$foto_temporary = $_FILES['userfile[]']['name'];
			$files = $_FILES;
			$filename = $_FILES['userfile']['name'];

			if(!empty($filename)){
		   // $cpt = count($_FILES['userfile']['name']);
			
				$this->load->library('upload');
			    
	            $dt = $files['userfile']['name'];
		        $cx = $files['userfile']['tmp_name'];
			    $file_ext = explode(".", $dt);
	            $file_ext = $file_ext[1];
	        	$filename = time().".".$file_ext;
	        	move_uploaded_file($cx, "public/img/news/".$filename);
	        	$dt = $files['userfile']['name'];
	        }else{
	        	$filename = "default.jpg";
	        }

        	$a = $this->mod_news->addNews(array(
                        "id_news"=>'',    
                        "judul_news"=>$judul,
                        "deskripsi_news"=>$highlight,
                        "main_news"=>$konten,
                        "create_news"=>$this->session->userdata('nama'),
                        "image_news"=>$filename,
                        "status_news"=>$status));


        	if($a){
        		echo "<script language=\"javascript\">alert('Berhasil tambah berita!');location='".base_url()."index.php/backend/news';</script>";
        	}
        	else{
        		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/news';</script>"; 
        	}
	}


	public function edit($id){
		
		$dataEdit = $this->mod_news->getNewsWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_news', array('data'=>$dataEdit),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));		

	}

	public function do_edit(){
		$filename = "";

		$id = $this->input->post('id');
		$judul = $this->input->post('judul');
		$highlight = $this->input->post('highlight');
		$content = $this->input->post('konten');
		$status = $this->input->post('status');

		$files = $_FILES;

		$this->load->library('upload');

		if($_FILES["userfile"]["error"]==0){

			$file_ext = explode(".", $files['userfile']['name']);
	        $file_ext = $file_ext[1];

        	$filename = time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'], "public/img/news/".$filename);
		}

		else{
			$filename = $this->input->post('foto');
		}

		$updateData = $this->mod_news->editNews(
			array(
			"judul_news"=>$judul,
			"deskripsi_news"=>$highlight,
			"main_news"=>$content,
			"image_news"=>$filename,
			"status_news"=>$status),$id);

		if($updateData){
			echo "<script language=\"javascript\">alert('Berhasil edit berita!');location='".base_url()."index.php/backend/news';</script>";
        }
        else{
        	echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/news';</script>"; 
        }
	}

	public function delete($id){
		$data = $this->db->get_where("tbl_news",array("id_news"=>$id))->result();
		foreach ($data as $key => $value)
			$img = $value->image_news;

		$this->mod_news->deleteNews($id);
		unlink(FCPATH."public/img/news/".$img);
		redirect("backend/news");
	}
}

?>