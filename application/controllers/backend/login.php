<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model("mod_login");
		}

	public function index(){
		$this->load->view('backend/login');
	}

	public function proseslog() {
		$data = array(
			'username' => $this->input->post('usernameTxt', TRUE),
			'password' => $this->input->post('passTxt', TRUE),
			);
		
		$hasil = $this->mod_login->GetUser($data);

		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				// $sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['id_user'] = $sess->id_user;
				$sess_data['nama'] = $sess->nama;
				$sess_data['email'] = $sess->email;
				$sess_data['status'] = $sess->status;
				$sess_data['password'] = $sess->password;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('status')=='1') {
				$this->session->set_userdata('useradmin', $sess_data);
				redirect(base_url()."index.php/backend/about");
			}
			else{
				$this->session->set_userdata('adminfailed', $sess_data);
				redirect(base_url());
			}		
		}
		else {
			$info='<div style="color:red">PERIKSA KEMBALI NAMA PENGGUNA DAN PASSWORD ANDA!</div>';
			$this->session->set_userdata('info',$info);
                        

			redirect(base_url().'index.php/backend/login');
		}
	}

	public function logout(){ 
			$this->session->sess_destroy();
			$this->session->set_flashdata(array("messages"=>"Udah Logout"));
			redirect("backend/login"); 
	}
}
