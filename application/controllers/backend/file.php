<?php
class File extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataSlider = $this->mod_file->getFile();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/file', array('data'=>$dataSlider),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_file', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function save_data(){
		$filename = "";

		$nama = $this->input->post('nama');
		$deskripsi = $this->input->post('deskripsi');

		$files = $_FILES;

		$this->load->library('upload');


		$file_ext = explode(".", $files['userfile']['name']);
        $file_ext = $file_ext[1];

    	$filename = time().".".$file_ext;
    	move_uploaded_file($files['userfile']['tmp_name'], "public/file/".$filename);


		$data = array(
			"id_file"=>"",
			"nama_file"=>$nama,
			"deskripsi_file"=>$deskripsi,
			"source_file"=>$filename
		);

		$updateData = $this->mod_file->addFile($data);

		if($updateData){
			echo  "<script language=\"javascript\">alert('Berhasil Tambah File !');location='".base_url()."index.php/backend/file';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/file';</script>";
    	}
		
	}

	public function edit($id){

		$dataEdit = $this->mod_file->getWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_file',array("data"=>$dataEdit),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));		
	}

        public function delete($id){
               $delete = $this->mod_file->deleteFile($id);
               if($delete){echo  "<script language=\"javascript\">alert('Berhasil Hapus File !');location='".base_url()."index.php/backend/file';</script>"; }
               else{
                   echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/file';</script>";
               }
        }

	public function do_edit(){

		$filename = "";

		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$deskripsi = $this->input->post('deskripsi');

		$files = $_FILES;

		$this->load->library('upload');

		if($_FILES["userfile"]["error"]==0){

			$file_ext = explode(".", $files['userfile']['name']);
	        $file_ext = $file_ext[1];

        	$filename = time().".".$file_ext;
        	move_uploaded_file($files['userfile']['tmp_name'], "public/file/".$filename);
		}

		else{
			$filename = $this->input->post('foto');
		}

		$data = array(
			"nama_file"=>$nama,
			"deskripsi_file"=>$deskripsi,
			"source_file"=>$filename
		);

		$updateData = $this->mod_file->updateFile($data,$id);

		if($updateData){
			echo  "<script language=\"javascript\">alert('Berhasil Edit File !');location='".base_url()."index.php/backend/file';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/file';</script>";
    	}
		
	}

}

?>