<?php
class Slider extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataSlider = $this->mod_slider->getSliderAdmin();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/slider_1', array('data'=>$dataSlider),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_slider', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function save_data(){

		$judul = $this->input->post('judul');
		$status = $this->input->post('status');
		//$foto_temporary = $_FILES['userfile[]']['name'];
		$deskripsi = $this->input->post('deskripsi');
		$files = $_FILES;
	   // $cpt = count($_FILES['userfile']['name']);

		$periksa = $this->mod_slider->getCheck()->num_rows();

		if($periksa AND $status==1){
			echo  "<script language=\"javascript\">alert('Image Banner First Sudah Ada, Mohon Edit STATUS !');location='".base_url()."index.php/backend/slider/add';</script>";
		}
		else{

			$this->load->library('upload');
		    
	        $dt = $files['userfile']['name'];
	        $cx = $files['userfile']['tmp_name'];
		    $file_ext = explode(".", $dt);
	        $file_ext = $file_ext[1];
	    	$filename = time().".".$file_ext;
	    	move_uploaded_file($cx, "public/img/slider/".$filename);
	    	$dt = $files['userfile']['name'];

	    	$a = $this->mod_slider->addSlider(array(
	                    "id_slider"=>'',    
	                    "judul_slider"=>$judul,
	                    "deskripsi_slider"=>$deskripsi,
	                    "image_slider"=>$filename,
	                    "status_slider"=>$status));
	    	if($a){
	    		echo "<script language=\"javascript\">alert('Berhasil tambah slider banner!');location='".base_url()."index.php/backend/slider';</script>";
	    	}
	    	else{
	    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/slider';</script>"; 
	    	}
    	}

	}

	public function edit($id){


		$dataEdit = $this->mod_slider->getWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_slider',array("data"=>$dataEdit),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));		
	}

	public function do_edit(){

		$filename = "";

		$id = $this->input->post('id');
		$title = $this->input->post('judul');
		$status = $this->input->post('status');
		$deskripsi = $this->input->post('deskripsi');

		$oldStatus = $this->input->post('oldStatus');

		$identity = $this->uri->segment('4');

		if($oldStatus==1){
			$files = $_FILES;

			$this->load->library('upload');

			if($_FILES["userfile"]["error"]==0){

				$file_ext = explode(".", $files['userfile']['name']);
		        $file_ext = $file_ext[1];

	        	$filename = time().".".$file_ext;
	        	move_uploaded_file($files['userfile']['tmp_name'], "public/img/slider/".$filename);
			}

			else{
				$filename = $this->input->post('foto');
			}


			$data = array(
				"judul_slider"=>$title,
				"deskripsi_slider"=>$deskripsi,
				"image_slider"=>$filename,
				"status_slider"=>$status
			);

			$updateData = $this->mod_slider->updateSlider($data,$id);

			if($updateData){
				echo  "<script language=\"javascript\">alert('Berhasil Edit Slider !');location='".base_url()."index.php/backend/slider';</script>";
		    }
	    	else{
	    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/slider';</script>";
	    	}
		}

		else{
			$periksa = $this->mod_slider->getCheck($id)->num_rows();

			if($status==1){
				echo  "<script language=\"javascript\">alert('Image Banner First Sudah Ada, Mohon Edit STATUS !');location='".base_url()."index.php/backend/slider/';</script>";
			}

			else{

				$files = $_FILES;

				$this->load->library('upload');

				if($_FILES["userfile"]["error"]==0){

					$file_ext = explode(".", $files['userfile']['name']);
			        $file_ext = $file_ext[1];

		        	$filename = time().".".$file_ext;
		        	move_uploaded_file($files['userfile']['tmp_name'], "public/img/slider/".$filename);
				}

				else{
					$filename = $this->input->post('foto');
				}

				$data = array(
					"judul_slider"=>$title,
					"deskripsi_slider"=>$deskripsi,
					"image_slider"=>$filename,
					"status_slider"=>$status
				);

				$updateData = $this->mod_slider->updateSlider($data,$id);

				if($updateData){
					echo  "<script language=\"javascript\">alert('Berhasil Edit Slider !');location='".base_url()."index.php/backend/slider';</script>";
			    }
		    	else{
		    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/slider';</script>";
		    	}
		    }
		}

		
	}

	public function delete($id){
		$this->mod_slider->deleteSlider($id);
		redirect("backend/slider");
	}


}

?>