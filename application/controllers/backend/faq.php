<?php
class Faq extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataFaq = $this->mod_faq->getFaqAdmin();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/faq', array('data'=>$dataFaq),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_faq', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function save_data(){
		$judul = $this->input->post("judul");
		$question = $this->input->post("question");
		$answer = $this->input->post("answer");
		$status = $this->input->post("status");

		$data = array(
			"id_faq"=>"",
			"judul"=>$judul,
			"pertanyaan"=>$question,
			"jawaban"=>$answer,
			"status"=>$status
			);
		$x = $this->mod_faq->addFaqData($data);

		if($x){
			echo "<script language=\"javascript\">alert('Berhasil Menambah FAQ');location='".base_url()."index.php/backend/faq';</script>";
        }else{
        	echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/faq';</script>"; 
        }
	}

	public function edit($id){

		$dataEditFaq = $this->mod_faq->getEditFaqAdmin($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_faq',array("data"=>$dataEditFaq),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));
	}

	public function do_edit(){

		$id = $this->input->post('id');
		$title = $this->input->post('judul');
		$status = $this->input->post('status');
		$question = $this->input->post('question');
		$answer = $this->input->post('answer');

		$data = array(
			"judul"=>$title,
			"pertanyaan"=>$question,
			"jawaban"=>$answer,
    		"status"=>$status
		);


		$updateData = $this->mod_faq->editFaq($data,$id);
		
		if ($updateData) {
			echo  "<script language=\"javascript\">alert('Berhasil Edit FAQ !');location='".base_url()."index.php/backend/faq';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/faq';</script>";
    	}

	}

	public function delete($id){
		$this->mod_faq->deleteFaq($id);
		redirect("backend/faq");
	}


}

?>