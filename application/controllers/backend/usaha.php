<?php
class Usaha extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("backend/login");
		}
	}

	public function index(){

		$dataCategory = $this->mod_usaha->getUsahaAdmin();

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/usaha', array('data'=>$dataCategory),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function add(){

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/add_usaha', '',true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));			
	}

	public function save_data(){

		$nama = $this->input->post('nama');
		$desc = $this->input->post("deskripsi");

		$data = array(
			"id_usaha"=>"",
			"nama_usaha"=>$nama,
			"deskripsi"=>$desc
		);

		$addKategori = $this->mod_usaha->addUsaha($data);

		if($addKategori){
			echo  "<script language=\"javascript\">alert('Berhasil Tambah Usaha !');location='".base_url()."index.php/backend/usaha';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/usaha';</script>";
    	}

	}

	public function edit($id){


		$dataEdit = $this->mod_usaha->getWhere($id);

		$data['header'] = $this->load->view('backend/layout/header','',true);

		$body = $this->load->view('backend/page/edit_usaha',array("data"=>$dataEdit),true);

		$data['body'] = $this->load->view('backend/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('backend/layout/sidebar','',true);

		$data['footer'] = $this->load->view('backend/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));		
	}

	public function do_edit(){

		$nama = $this->input->post('nama');
		$desc = $this->input->post("deskripsi");
		$id = $this->input->post('id');

		$data = array(
			"nama_usaha"=>$nama,
			"deskripsi"=>$desc
		);

		$updateData = $this->mod_usaha->updateUsaha($data,$id);

		if($updateData){
			echo  "<script language=\"javascript\">alert('Berhasil Edit Usaha !');location='".base_url()."index.php/backend/usaha';</script>";
	    }
    	else{
    		echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/backend/usaha';</script>";
    	}

	}

	public function delete($id){
		$this->mod_usaha->deleteUsaha($id);
		redirect("backend/usaha");
	}


}

?>