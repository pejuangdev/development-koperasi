<?php
class started extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_member');
	}

	public function index(){
			// $data_berkas = $this->mod_member->getArchive();
			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");
			$status = 5;
			$data['header'] = $this->load->view('front/tools/header',array("about"=>$about->result()),true);

			$body = $this->load->view('member/page/started', array('status'=>$status),true);

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			
			// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

			$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

			$this->load->view("front/master",array("data"=>$data));
			
			// $this->load->view('backend/layout/head',array('template'=>$data));	
					

	}



	public function upload_data(){

		$id_kop = $this->session->userdata('id_kop');

		$files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);

		
		$this->load->library('upload');

	    for($i=0; $i<$cpt; $i++)
	    {

	    	if ($_FILES['userfile']['name'][$i] == ""){
	    		$dt[] = $_FILES['userfile']['name'][$i] = "name.xyz";
	    		$cx[] = $files['userfile']['tmp_name'][$i];
			    $file_ext = explode(".", $dt[$i]);
	            $ext[$i] = $file_ext[1];
	            $filename[$i] = "NULL";
	    	}
	    	else{

	    		$dt[] = $files['userfile']['name'][$i];
		        $cx[] = $files['userfile']['tmp_name'][$i];
			    $file_ext = explode(".", $dt[$i]);
	            $ext[$i] = $file_ext[1];
	             	if($ext[$i]!="jpg" && "png" && "pdf"){
	            		echo "<script language=\"javascript\">alert('Format File $dt[$i] Tidak Sesuai');location='".base_url()."index.php/member/archive';</script>";
	            	}
	            if($i==0){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/ktp/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i]; 
	            }
	            if($i==1){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/kk/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==2){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/slip_gaji/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==3){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/tabungan/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==4){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/id_card/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==5){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/surat_kuasa/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }	
	            if($i==6){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/npwp/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            	           
	        }          
          
	    }

	    $data = array(
	    				"id_berkas"=>"",
	        			"id_kop"=>$id_kop,
	        			"ktp"=>$filename[0],
	        			"kk"=>$filename[1],
	        			"slip_gaji"=>$filename[2],
	        			"buku_tabungan"=>$filename[3],
	        			"id_card"=>$filename[4],
	        			"surat_kuasa"=>$filename[5],
	        			"npwp"=>$filename[6]
	        		);

	    $insert = $this->mod_member->addArchive($data);

        if($insert){
        	echo "<script language=\"javascript\">alert('Berhasil Mengupload Berkas!');location='".base_url()."index.php/member/archive';</script>";
        }
        else{
        	echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/member/archive';</script>";
        }  
	}


}

?>