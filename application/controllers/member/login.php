<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("mod_member");
	}

	public function index(){
		$this->load->view('member/started');
	}

	public function proseslog() {
		$data = array(
			'id_kop' => $this->input->post('idTxt', TRUE),
			'password' => $this->input->post('passTxt', TRUE)
		);
		
		$hasil = $this->mod_member->GetUser($data);

		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				// $sess_data['logged_in'] = 'Sudah Login';
				$sess_data['npk'] = $sess->npk;
				$sess_data['id_kop'] = $sess->id_kop;
				$sess_data['nama'] = $sess->nama_anggota;
				$sess_data['immediately'] = $sess->immediately_password;
				$sess_data['status'] = $sess->status_anggota;
				$sess_data['password'] = $sess->password;
				$this->session->set_userdata($sess_data);
			}

			if ($this->session->userdata('status')=='1'){
				$this->session->set_userdata('useradmin', $sess_data);
				redirect(base_url()."index.php/member/profil");
			}

			else{
				$this->session->set_userdata('userfailed', $sess_data);
				echo "<script>alert('Gagal Login Anggota! Anda tidak memiliki akses!');document.location.href='".site_url()."/member/started';</script>";
			}		
		}

		else {
			$info='<div style="color:red">PERIKSA KEMBALI NAMA PENGGUNA DAN PASSWORD ANDA!</div>';
			$this->session->set_userdata('info',$info);

			echo "<script>alert('Gagal Login Anggota! ID KOPERASI atau PASSWORD salah!');document.location.href='".site_url()."/member/started';</script>";
		}
	}

	public function logout(){ 
			$this->session->sess_destroy();
			$this->session->set_flashdata(array("messages"=>"Udah Logout"));
			redirect("member/started"); 
	}

	public function forgotPass(){

		$id_kop = $this->input->post('id');

		$status = $this->mod_member->checkPass($id_kop);

		if($status>0){
			$this->load->model("mod_anggota");
			
			$no_hp = $this->input->post('no_hp');
			$email = $this->input->post('email');

			$data = array("forgot_password"=>1,"no_hp"=>$no_hp,"email"=>$email);

			$forgot = $this->mod_anggota->doForgotPassword($data,$id_kop);
			$status = 3;
		}else{
			$status = 4;
		}

		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$data['header'] = $this->load->view('front/tools/header',array("about"=>$about->result()),true);

		$body = $this->load->view('member/page/started', array('status'=>$status),true);

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("front/master",array("data"=>$data));

	}

	public function checkId(){
		$npk = $this->input->post('id');
		$result = $this->mod_member->getIdKop($npk);
		$status = $this->mod_member->getStatusId($npk);

		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$data['header'] = $this->load->view('front/tools/header',array("about"=>$about->result()),true);

		$body = $this->load->view('member/page/started', array('anggota'=>$result,'status'=>$status),true);

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("front/master",array("data"=>$data));

	}

}