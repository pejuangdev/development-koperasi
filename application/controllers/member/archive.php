<?php
class archive extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_member');
		$this->load->model('mod_archive');
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("app/login");
		}
	}

	public function index(){


		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

		$npk = $this->session->userdata("npk");
		$dataApplicant = $this->mod_member->getApplicant($npk);

		$body = $this->load->view('member/page/archive_member', array('data'=>$dataApplicant),true);
		

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("member/master",array("data"=>$data));

		
		
		// $data['header'] = $this->load->view('member/layout/header','',true);

		// $body = $this->load->view('member/page/archive_member', array('data'=>$dataApplicant),true);

		// $data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		// $data['footer'] = $this->load->view('member/layout/footer','',true);
		
		// $this->load->view('member/layout/head',array('template'=>$data));
		

	}


	public function archiveFile(){
		
	}

	public function applia($offset=0){

			$npk = $this->session->userdata("npk");

			$config['uri_segment'] = 4;
			$config['base_url'] = base_url()."index.php/member/archive/index"; 
			$config['total_rows'] = $this->mod_archive->count_all_where($npk);
			$config['per_page'] = 20;

			$dataBerkas = $this->mod_archive->getArchiveByUser($config['per_page'], $offset, $npk);

			 // CSS Bootstrap               
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';            
			$config['prev_link'] = '«';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '»';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
	         // Akhir CSS

			$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );           
			$this->pagination->initialize($config);
			
			$pages = $this->pagination->create_links();


			$data['header'] = $this->load->view('member/layout/header','',true);

			$body = $this->load->view('member/page/archive', array('data'=>$dataBerkas,"pages"=>$pages),true);

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			
			$data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

			$data['footer'] = $this->load->view('member/layout/footer','',true);
			
			$this->load->view('backend/layout/head',array('template'=>$data));				

	}

	public function appliance(){


		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

		$npk = $this->session->userdata("npk");
		$dataApplicant = $this->mod_member->getApplicant($npk);

		$body = $this->load->view('member/page/appliance_member', array('data'=>$dataApplicant),true);
		

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("member/master",array("data"=>$data));

		// $npk = $this->session->userdata("npk");
		// $dataApplicant = $this->mod_member->getApplicant($npk);
		
		// $data['header'] = $this->load->view('member/layout/header','',true);

		// $body = $this->load->view('member/page/appliance', array('data'=>$dataApplicant),true);

		// $data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		// $data['footer'] = $this->load->view('member/layout/footer','',true);
		
		// $this->load->view('member/layout/head',array('template'=>$data));
	}

	public function add_archive(){
			//$data_berkas = $this->mod_member->getArchive();

			$data['header'] = $this->load->view('member/layout/header','',true);

			$body = $this->load->view('member/page/add_archive', '',true);

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			
			$data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

			$data['footer'] = $this->load->view('member/layout/footer','',true);
			
			$this->load->view('backend/layout/head',array('template'=>$data));
	}

	public function do_add_appliance(){

		  $npk = $this->session->userdata("npk");
		  $statusAppliance=0;
		  $periksa = mysql_num_rows(mysql_query("SELECT * FROM tbl_appliance WHERE npk_appliance='$npk'")); 
	      if($periksa>0){
	      	$statusAppliance = 1;
	      }


		$idKop = $this->session->userdata("id_kop");
		$queryDate = "SELECT tanggal_gabung FROM tbl_anggota WHERE id_kop = '$idKop'";
		$data = $this->db->query($queryDate)->result();
		foreach ($data as $value) {
		 	$dateJoin = $value->tanggal_gabung;
		 } 

		$todayMinYears = strtotime(date("Y-m-d"));
		$dateJoin = strtotime(date($dateJoin));
		$secs = $todayMinYears - $dateJoin;

		$days = $secs / 86400;
		
		if($days<=365){
        	echo "<script language=\"javascript\">alert('Mohon maaf anda belum bisa mengajukan peminjaman dikarenakan keanggotaan kurang dari 1 tahun $days');location='".base_url()."index.php/member/archive/appliance';</script>";
        }
		

		$npk = $this->input->post("npk");
		$penempatan = $this->input->post("penempatan");
		$pinjaman = $this->input->post("pinjaman");
		$jangkaWaktu = $this->input->post("waktu");
		$usages = $this->input->post("usage");
		$hp = $this->input->post("telp");

		
		$today = date("Y-m-d");
		$jangkaWaktu = $jangkaWaktu." month";

		$newDate = strtotime('+ '.$jangkaWaktu,strtotime($today));
		$newDate = date('Y-m-j', $newDate);

		

		 $data = array(
    				"id_appliance"=>"",
        			"npk_appliance"=>$npk,
        			"id_installation_appliance"=>$penempatan,
        			"value_of"=>$pinjaman,
        			"create_date"=>$today,
        			"deadline"=>$newDate,
        			"phone_number"=>$hp,
        			"usages"=>$usages,
        			"status_appliance"=>$statusAppliance
        		);

	    $insert = $this->mod_member->addAppliance($data);

        if($insert){
        	if($statusAppliance==0){
        		echo "<script language=\"javascript\">alert('Berhasil Mengupload Form Pengajuan');location='".base_url()."index.php/member/archive/add_archive';</script>";
        	}
        	else{
        		echo "<script language=\"javascript\">alert('Berhasil Mengupload Form Pengajuan');location='".base_url()."index.php/member/archive/';</script>";
        	}
        	
        }
        else{
        	echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/member/archive/appliance';</script>";
        }  
	}



	public function upload_data(){


		$id_kop = $this->session->userdata('id_kop');

		$files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);

		
		$this->load->library('upload');

	    for($i=0; $i<$cpt; $i++)
	    {
	    	

	    	if ($_FILES['userfile']['name'][$i] == ""){
	    		$dt[] = $_FILES['userfile']['name'][$i] = "name.xyz";
	    		$cx[] = $files['userfile']['tmp_name'][$i];
			    $file_ext = explode(".", $dt[$i]);
	            $ext[$i] = $file_ext[1];
	            $filename[$i] = "NULL";
	    	}
	    	else{

	    		$dt[] = $files['userfile']['name'][$i];
		        $cx[] = $files['userfile']['tmp_name'][$i];
			    $file_ext = explode(".", $dt[$i]);
	            $ext[$i] = $file_ext[1];
	             	if($ext[$i]!="jpg" && "png" && "pdf"){
	            		echo "<script language=\"javascript\">alert('Format File $dt[$i] Tidak Sesuai');location='".base_url()."index.php/member/archive';</script>";
	            	}
	            if($i==0){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/ktp/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i]; 
	            }
	            if($i==1){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/kk/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==2){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/slip_gaji/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==3){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/tabungan/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==4){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/id_card/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            if($i==5){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/surat_kuasa/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }	
	            if($i==6){
	            	$filename[$i] = time()."-".time().".".$ext[$i];
	            	move_uploaded_file($cx[$i], "public/file/anggota/archive/npwp/".$filename[$i]);
	            	$dt[$i] = $files['userfile']['name'][$i];
	            }
	            	           
	        }          
          
	    }

	    $data = array(
	    				"id_berkas"=>"",
	        			"id_kop"=>$id_kop,
	        			"ktp"=>$filename[0],
	        			"kk"=>$filename[1],
	        			"slip_gaji"=>$filename[2],
	        			"buku_tabungan"=>$filename[3],
	        			"id_card"=>$filename[4],
	        			"surat_kuasa"=>$filename[5],
	        			"npwp"=>$filename[6]
	        		);

	    $insert = $this->mod_member->addArchive($data);

        if($insert){
        	echo "<script language=\"javascript\">alert('Berhasil Mengupload Berkas!');location='".base_url()."index.php/member/archive';</script>";
        }
        else{
        	echo "<script language=\"javascript\">alert('Gagal!');location='".base_url()."index.php/member/archive';</script>";
        }  
	}


	public function delete($npk){
		$delete = $this->mod_archive->deleteByUser($npk);

        if($delete){
        	echo "<script language=\"javascript\">alert('Berhasil Hapus Berkas!');location='".base_url()."index.php/member/archive';</script>";
        }
        else{
        	echo "<script language=\"javascript\">alert('Gagal Hapus Berkas!');location='".base_url()."index.php/member/archive';</script>";
        }
	}


}

?>