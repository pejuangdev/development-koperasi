<?php
class profil extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_member');
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("member/starter");
		}
	}

	public function index(){
		if($this->session->userdata("immediately")==1){

			$this->changePass();
		} 
		else{

			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			$id_kop = $this->session->userdata("id_kop");

			$dataAnggota = $this->mod_member->getMemberWhere($id_kop);

			$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

			$body = $this->load->view('member/page/dashboard_member', array('data'=>$dataAnggota),true);

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			

			$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

			$this->load->view("member/master",array("data"=>$data));
		}				

	}

     public function seeProfil(){
		$id_kop = $this->session->userdata("id_kop");

		$dataAnggota = $this->mod_member->getMemberWhere($id_kop);

		$data['header'] = $this->load->view('member/layout/header','',true);

		$body = $this->load->view('member/page/dashboard', array('data'=>$dataAnggota),true);

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		$data['footer'] = $this->load->view('member/layout/footer','',true);
		
		$this->load->view('backend/layout/head',array('template'=>$data));
	}


    public function changePass(){


    		$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

			$body = $this->load->view('member/page/change_pass_member', '',true);

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			
			// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

			$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

			$this->load->view("front/master",array("data"=>$data));
			
    }

	public function doChangePassword(){

		$id_kop =  $this->session->userdata("id_kop");
		$immediately_status = $this->session->userdata("immediately");
		$currentPwd = $this->input->post("currentPass");

		$data = array("password"=>$currentPwd, "immediately_password"=>0);

		$passChanged = $this->mod_member->changeData($data, $id_kop);

		if($passChanged){
			$sess_data['immediately'] = 0;
			$sess_data['password'] = $currentPwd;
			$this->session->set_userdata($sess_data);
			echo "<script>alert('Berhasil Ganti Password');document.location.href='".site_url()."/member/profil';</script>";
		}else{
			echo "<script>alert('Gagal');document.location.href='".site_url()."/member/profil';</script>";
		}


	}

	public function changeProfil(){

			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			$id_kop =  $this->session->userdata("id_kop");

			$dataAnggota = $this->mod_member->getMemberWhere($id_kop);

			$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

			$body = $this->load->view('member/page/change_profil_member', array("data"=>$dataAnggota),true);

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			
			// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

			$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

			$this->load->view("front/master",array("data"=>$data));
	}

	public function do_change_profil(){

		$id_kop = $this->session->userdata("id_kop");
		$email= $this->input->post("email");
		$no_hp= $this->input->post("no_hp");
		$alamat= $this->input->post("alamat");
		$foto = $_FILES["userfile"]["name"];


		if(empty($foto)){
			$data = array(
				"email"=>$email,
				"no_hp"=>$no_hp,
				"alamat_anggota"=>$alamat
			);
		}else{
			$ext = explode(".",$foto);
			$filename = time() . "-" . rand(0,1000) . ".".$ext[1];
			$this->load->library('upload');
			$config = array(
			                'allowed_types' => 'jpg|png|jpeg',
			                'upload_path'=> 'public/img/member/',            
			                'max-size' => '2048',
			                'file_name' => $filename
			                );

			$this->upload->initialize($config);
	        
	        $x = $this->upload->do_upload('userfile');

	        if($x){
	        	$data = array(
				"email"=>$email,
				"no_hp"=>$no_hp,
				"alamat_anggota"=>$alamat,
				"foto"=>$filename
				);
	        }
		}

		$update = $this->mod_member->changeData($data,$id_kop);

		if($update){
			echo "<script>alert('Berhasil Update Anggota');document.location.href='".site_url()."/member/profil';</script>";
		}else{
			echo "<script>alert('Gagal Update Anggota');document.location.href='".site_url()."/member/profil';</script>";
		}

	}


	// public function upload(){
		
	// 	$data['header'] = $this->load->view('app/layout/header','',true);

	// 	$body = $this->load->view('app/page/upload_member', array(),true);

	// 	$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
	// 	$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

	// 	$data['footer'] = $this->load->view('app/layout/footer','',true);
		
	// 	$this->load->view('backend/layout/head',array('template'=>$data));
	// }

	// public function upload_data(){

	// 	$this->db->query("TRUNCATE TABLE `tbl_cost_center`");

	// 	$this->load->library('upload');
		    
 //        $dt = $_FILES['userfile']['name'];
 //        $cx = $_FILES['userfile']['tmp_name'];
	//     $file_ext = explode(".", $dt);
 //        $file_ext = $file_ext[1];
 //        $bulan;
 //    	$filename = time().".".$file_ext;
 //    	move_uploaded_file($cx, "public/file/anggota/".$filename);

 //    	$excel = 'public/file/anggota/'.$filename;

 //    	$this->load->library('excel');

 //    	$objPHPExcel = PHPExcel_IOFactory::load($excel);

 //    	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	// 		$highrow = $worksheet->getHighestRow();
	// 		for($row=2;$row<=$highrow;$row++){
	// 			$npk = $worksheet->getCellByColumnAndRow(0,$row)->getValue();
	// 			$nama_anggota = $worksheet->getCellByColumnAndRow(1,$row)->getValue();
	// 			$alamat = $worksheet->getCellByColumnAndRow(2,$row)->getValue();
	// 			$tgl_lahir = $worksheet->getCellByColumnAndRow(3,$row)->getValue();
	// 			$tgl_gabung = $worksheet->getCellByColumnAndRow(4,$row)->getValue();
				
	// 			//echo $tgl_lahir."<br>";
	// 			$exPass = explode("-", $tgl_lahir);
	// 			$joinDate = explode("-", $tgl_gabung);
	// 			if($exPass[1]=="Jan"){$bulan == "01";}
	// 			elseif($exPass[1]=="Feb"){$bulan = "02";}
	// 			elseif($exPass[1]=="Mar"){$bulan = "03";}
	// 			elseif($exPass[1]=="Apr"){$bulan = "04";}
	// 			elseif ($exPass[1]=="May"){$bulan = "05";}
	// 			elseif ($exPass[1]=="Jun"){$bulan = "06";}
	// 			elseif ($exPass[1]=="Jul"){$bulan = "07";}
	// 			elseif ($exPass[1]=="Aug"){$bulan = "08";}
	// 			elseif ($exPass[1]=="Sep"){$bulan = "09";}
	// 			elseif ($exPass[1]=="Oct"){$bulan = "10";}
	// 			elseif ($exPass[1]=="Nov"){$bulan = "11";}
	// 			elseif ($exPass[1]=="Dec"){$bulan = "12";}
	// 			else {$bulan = "00";}
	// 			if($joinDate[1]=="Jan"){$bulanJoin == "01";}
	// 			elseif($joinDate[1]=="Feb"){$bulanJoin = "02";}
	// 			elseif($joinDate[1]=="Mar"){$bulanJoin = "03";}
	// 			elseif($joinDate[1]=="Apr"){$bulanJoin = "04";}
	// 			elseif ($joinDate[1]=="May"){$bulanJoin = "05";}
	// 			elseif ($joinDate[1]=="Jun"){$bulanJoin = "06";}
	// 			elseif ($joinDate[1]=="Jul"){$bulanJoin = "07";}
	// 			elseif ($joinDate[1]=="Aug"){$bulanJoin = "08";}
	// 			elseif ($joinDate[1]=="Sep"){$bulanJoin = "09";}
	// 			elseif ($joinDate[1]=="Oct"){$bulanJoin = "10";}
	// 			elseif ($joinDate[1]=="Nov"){$bulanJoin = "11";}
	// 			elseif ($joinDate[1]=="Dec"){$bulanJoin = "12";}
	// 			else  {$joinDate="00";}


	// 			$trueBorn = $exPass[2]."-".$bulan."-".$exPass[0];
	// 			$trueJoin = $joinDate[2]."-".$bulanJoin."-".$joinDate[0];
	// 			$pwd = $exPass[0].$bulan.$exPass[2];

	// 			// echo $trueBorn."<br>";
	// 			// echo $pwd."<br>";
	// 			// echo $trueJoin."<br>";
						
	// 			$data = array(
	// 				"id_kop"=>"",
	// 				"npk"=>$npk,
	// 				"password"=>$pwd,
	// 				"nama_anggota"=>$nama_anggota,
	// 				"tanggal_lahir"=>$trueBorn,
	// 				"tanggal_gabung"=>$trueJoin,
	// 				"alamat_anggota"=>$alamat,
	// 				"immediately_password"=>1,
	// 				"status_anggota"=>1
	// 				);

	// 			 $insert = $this->mod_anggota->addAnggota($data);
	// 		}
	// 	}
	// 	unlink($excel);
	// 	if($insert){
			
	// 		echo "<script>alert('Berhasil Input Cost Center');document.location.href='".site_url()."/app/anggota';</script>";
	// 	}else{
	// 		echo "<script>alert('Gagal Input Cost Center');document.location.href='".site_url()."/app/anggota';</script>";
	// 	}
    	
	// }

	// public function save_data(){
	// 	$npk = $this->input->post('npk');
	// 	$nama = $this->input->post('nama');
	// 	$alamat = $this->input->post('alamat');
	// 	$tgl_lahir = $this->input->post('tgl_lahir');
	// 	$tgl_gabung = date('Y-m-d');
	// 	$status = $this->input->post('status');

	// 	$exPass = explode("-", $tgl_lahir);
	// 	$exPass = $exPass[2].$exPass[1].$exPass[0];

		
	// 	 $data = array(
	// 	 	"id_kop"=>"",
	// 	 	"npk"=>$npk,
	// 	 	"password"=>$exPass,
	// 	 	"nama_anggota"=>$nama,
	// 	 	"tanggal_lahir"=>$tgl_lahir,
	// 	 	"tanggal_gabung"=>$tgl_gabung,
	// 	 	"alamat_anggota"=>$alamat,
	// 	 	"immediately_password"=>1,
	// 	 	"status_anggota"=>$status
	// 	 );

	// 	 $insert = $this->mod_anggota->addAnggota($data);

	// 	if($insert){
	// 		echo "<script>alert('Berhasil Input Anggota');document.location.href='".site_url()."/app/anggota';</script>";
	// 	}else{
	// 		echo "<script>alert('Gagal');document.location.href='".site_url()."/app/anggota';</script>";
	// 	}

	// }

	// public function edit($id){
	// 	$data_anggota = $this->mod_anggota->getWhereMember($id);

	// 	$data['header'] = $this->load->view('app/layout/header','',true);

	// 	$body = $this->load->view('app/page/edit_member', array('data'=>$data_anggota),true);

	// 	$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
	// 	$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

	// 	$data['footer'] = $this->load->view('app/layout/footer','',true);
		
	// 	$this->load->view('backend/layout/head',array('template'=>$data));
	// }

	// public function do_edit(){
	// 	$id_kop = $this->input->post('id_kop');
	// 	$npk = $this->input->post('npk');
	// 	$nama = $this->input->post('nama');
	// 	$alamat = $this->input->post('alamat');
	// 	$tgl_lahir = $this->input->post('tgl_lahir');
	// 	$tgl_keluar = $this->input->post('tgl_keluar');
	// 	$status = $this->input->post('status');
		
	// 	$data = array("npk"=>$npk,
	// 		"nama_anggota"=>$nama,
	// 		"tanggal_lahir"=>$tgl_lahir,
	// 		"tanggal_keluar"=>$tgl_keluar,
	// 		"alamat_anggota"=>$alamat,
	// 		"status_anggota"=>$status
	// 	);

	// 	$update = $this->mod_anggota->updateAnggota($data,$id_kop);

	// 	if($update){
	// 		echo "<script>alert('Berhasil Update Anggota');document.location.href='".site_url()."/app/anggota';</script>";
	// 	}else{
	// 		echo "<script>alert('Gagal Update Anggota');document.location.href='".site_url()."/app/anggota';</script>";
	// 	}
	// }
}

?>