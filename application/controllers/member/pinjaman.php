<?php
class Pinjaman extends CI_Controller {

	var $bulan = "";
	var $tahun = "";

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_instalasi');
		$this->load->model('mod_anggota');
		$this->load->model('mod_pinjaman_member');
		$this->load->model('mod_transaksi');
		$this->load->model('mod_member');
		$this->load->model('mod_archive');
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("member/started");
		}
	}

	public function index(){

		$id_kop = $this->session->userdata('id_kop');

		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");


		$dataAnggota = $this->mod_member->getLoanMember($id_kop);

		$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

		$body = $this->load->view('member/page/pinjaman_member', array('data'=>$dataAnggota),true);
		

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("member/master",array("data"=>$data));
	}

	public function detail_angsuran($id_pinjaman){

		$info = $this->mod_member->getInfoPinjaman($id_pinjaman);
		$angsuran = $this->mod_member->getAngsuran($id_pinjaman);

		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

		// $data_anggota = $this->mod_anggota->getMember();
		// $data_instalasi = $this->mod_instalasi->getLoanMember($id);

		$body = $this->load->view('member/page/angsuran_member', array('info'=>$info,'angsuran'=>$angsuran),true);
		

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("member/master",array("data"=>$data));
	}

	public function ajax_list()
	{	
		
		$list = $this->mod_pinjaman_member->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $pinjaman) {
			
			$no++;
			$row = array();
			$row[] = $pinjaman->id_pinjaman;
			$row[] = $pinjaman->npk;
			$row[] = $pinjaman->nama_anggota;
			$row[] = $pinjaman->nama_instalasi;
			$row[] = number_format($pinjaman->jumlah_pinjaman);
			$row[] = $pinjaman->tenor;
			$row[] = $this->mod_transaksi->listBulan($pinjaman->bulan_pinjaman) . "-" . $pinjaman->tahun_pinjaman;
			$row[] = $this->convertStatus($pinjaman->status_pinjaman);
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_pinjaman('."'".$pinjaman->id_pinjaman."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_pinjaman('."'".$pinjaman->id_pinjaman."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_pinjaman_member->count_all(),
						"recordsFiltered" => $this->mod_pinjaman_member->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_add()
	{
		$this->form_validation->set_rules('id_pinjaman', 'id_pinjaman', 'trim|required|xss_clean');
        $this->form_validation->set_rules('instalasi', 'instalasi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('npk', 'npk', 'trim|required|xss_clean');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'trim|number|required|xss_clean');
        $this->form_validation->set_rules('tenor','Tenor','required|number');
        $this->form_validation->set_rules('status','Status','required|number');
        $this->form_validation->set_rules('bulan','Bulan','required|number');
        $this->form_validation->set_rules('tahun','Tahun','required|number');
        if ($this->form_validation->run() != FALSE)
        {

        $id_pinjaman = $this->input->post('id_pinjaman');
		$instalasi = $this->input->post('instalasi');
		$id_kop = $this->input->post('npk');
		$tenor = $this->input->post('tenor');
		$jumlah = $this->input->post('jumlah');
		$status = $this->input->post('status');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');

		$data = array(
					"id_pinjaman"=> $id_pinjaman,
					"id_kop"=>$id_kop,
					"id_instalasi"=>$instalasi,
					"jumlah_pinjaman" =>$jumlah,
					"tenor"=>$tenor,
					"bulan_pinjaman"=>$bulan,
					"tahun_pinjaman"=>$tahun,
					"status_pinjaman"=>$status
					);

		$insert = $this->mod_pinjaman_member->addLoan($data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_update()
	{
		$this->form_validation->set_rules('id_pinjaman', 'id_pinjaman', 'trim|required|xss_clean');
        $this->form_validation->set_rules('instalasi', 'instalasi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('npk', 'npk', 'trim|required|xss_clean');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'trim|number|required|xss_clean');
        $this->form_validation->set_rules('tenor','Tenor','required|number');
        $this->form_validation->set_rules('status','Status','required|number');
        $this->form_validation->set_rules('bulan','Bulan','required|number');
        $this->form_validation->set_rules('tahun','Tahun','required|number');
        if ($this->form_validation->run() != FALSE)
        {

        $id_pinjaman = $this->input->post('id_pinjaman');
		$instalasi = $this->input->post('instalasi');
		$id_kop = $this->input->post('npk');
		$tenor = $this->input->post('tenor');
		$jumlah = $this->input->post('jumlah');
		$status = $this->input->post('status');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');

		$data = array(
					"id_pinjaman"=> $id_pinjaman,
					"id_kop"=>$id_kop,
					"id_instalasi"=>$instalasi,
					"jumlah_pinjaman" =>$jumlah,
					"tenor"=>$tenor,
					"bulan_pinjaman"=>$bulan,
					"tahun_pinjaman"=>$tahun,
					"status_pinjaman"=>$status
					);

		$this->mod_pinjaman_member->update(array('id_pinjaman' => $this->input->post('id_pinjaman')), $data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_delete($id)
	{
		$this->mod_pinjaman_member->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function upload_data(){

			$this->db->query("TRUNCATE TABLE `tbl_pinjaman`");

			$name = $_FILES["userfile"]["name"];
	        $name_path = $_FILES["userfile"]["tmp_name"];

	        $this->excel_reader->read($name_path);

	        $worksheet = $this->excel_reader->sheets[0];
	        $baris = $worksheet['numRows'];


	        $numRows = $worksheet['numRows']; // ex: 14
	        $numCols = $worksheet['numCols']; // ex: 4
	        $cells = $worksheet['cells']; // the 1st row are usually the field's name

	        $x = 0;

	        for ($i=2; $i <=$numRows ; $i++) { 
	        	$x +=1;

				$id_pinjaman = $cells[$i][1];
				$id_kop = $cells[$i][2];
				$id_instalasi = $cells[$i][3];
				$jumlah = $cells[$i][4];
				$term = $cells[$i][5];
				$tahun = $cells[$i][6];
				$bulan = $cells[$i][7];
				$status = $cells[$i][8];
				
				$data = array(
				"id_pinjaman"=> $id_pinjaman,
				"id_kop"=>$id_kop,
				"id_instalasi"=>$id_instalasi,
				"jumlah_pinjaman" =>$jumlah,
				"tenor"=>$term,
				"bulan_pinjaman"=>$bulan,
				"tahun_pinjaman"=>$tahun,
				"status_pinjaman"=>$status
				);

				$insert = $this->mod_pinjaman_member->addLoan($data);
			}
			if($insert){
				echo "<script>alert('Berhasil Input Pinjaman');document.location.href='".site_url()."/loan/pinjaman';</script>";
			}else{
				echo "<script>alert('Gagal Input Pinjaman');document.location.href='".site_url()."/loan/pinjaman';</script>";
			}
			
		} 

	private function convertStatus($status){
		$result = "";
		if($status == 0){
			$result = "Belum Lunas";
		}else if($status == 1){
			$result = "Lunas";
		}else{
			$result = "Pending";
		}
		return $result;
	}

}

?>