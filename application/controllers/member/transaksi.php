<?php
class transaksi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_member');
		$this->load->model('mod_transaksi');
		$this->load->model('mod_anggota');
		$this->load->model('mod_instalasi');
		$this->cek_login();
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("app/login");
		}
	}

	public function index(){
		
		$type = $this->input->get('type');
		if(empty($type)){
			$type = "simpanan";
		}		

		$id_kop = $this->session->userdata('id_kop');

		$about = $this->db->query("SELECT * FROM tbl_about");
		$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$dataAnggota = $this->mod_member->getMemberWhere($id_kop);

		$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

		$data_transaksi = $this->mod_member->getListTrans($id_kop);

		$body = $this->load->view('member/page/transaksi_member', array("id"=>$id_kop,"data"=>$data_transaksi),true);	

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("member/master",array("data"=>$data));

		// $data['header'] = $this->load->view('member/layout/header','',true);

		
		
		// $data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		// $data['footer'] = $this->load->view('app/layout/footer','',true);
		
		// $this->load->view('app/layout/head',array('template'=>$data));			
	}

	public function ajax_list()
	{	
		$id = $this->session->userdata('id_kop');
		$this->mod_transaksi->setID($id);
		$list = $this->mod_transaksi->get_datatables_member();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $transaksi) {
			
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $transaksi->nama_anggota;
			$row[] = $transaksi->nama_instalasi;
			$row[] = $this->mod_transaksi->listBulan($transaksi->bulan_transaksi) . "-" . $transaksi->tahun_transaksi;
			$row[] = number_format($transaksi->simpanan_wajib);
			$row[] = number_format($transaksi->simpanan_pokok);
			$row[] = number_format($transaksi->simpanan_sukarela);
			$row[] = number_format($transaksi->SHU);
			
			//add html for action
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_transaksi->count_all(),
						"recordsFiltered" => $this->mod_transaksi->count_filtered_member(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_potongan()
	{	
		
		$this->mod_transaksi->setOrder(array('tbl_transaksi.id_kop','nama_anggota','nama_instalasi','tahun_transaksi','pinjaman','potongan_belanja','potongan_sembako','potongan_icare')); 
		$id = $this->session->userdata('id_kop');
		$this->mod_transaksi->setID($id);
		$list = $this->mod_transaksi->get_datatables_member();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $transaksi) {
			
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $transaksi->nama_anggota;
			$row[] = $transaksi->nama_instalasi;
			$row[] = $this->mod_transaksi->listBulan($transaksi->bulan_transaksi) . "-" . $transaksi->tahun_transaksi;
			$row[] = number_format($transaksi->pinjaman);
			$row[] = number_format($transaksi->potongan_belanja);
			$row[] = number_format($transaksi->potongan_sembako);
			$row[] = number_format($transaksi->potongan_icare);
			
			//add html for action
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_transaksi->count_all(),
						"recordsFiltered" => $this->mod_transaksi->count_filtered_member(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_lainnya()
	{	
		$this->mod_transaksi->setOrder(array('tbl_transaksi.id_kop','nama_anggota','nama_instalasi','tahun_transaksi','potongan_emoney','potongan_motor','potongan_or','potongan_other')); 
		$id = $this->session->userdata('id_kop');
		$this->mod_transaksi->setID($id);
		$list = $this->mod_transaksi->get_datatables_member();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $transaksi) {
			
			$no++;
			$row = array();
			$row[] = $transaksi->id_kop;
			$row[] = $transaksi->nama_anggota;
			$row[] = $transaksi->nama_instalasi;
			$row[] = $this->mod_transaksi->listBulan($transaksi->bulan_transaksi) . "-" . $transaksi->tahun_transaksi;
			$row[] = number_format($transaksi->potongan_emoney);
			$row[] = number_format($transaksi->potongan_motor);
			$row[] = number_format($transaksi->potongan_or);
			$row[] = number_format($transaksi->potongan_other);
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_transaksi->count_all(),
						"recordsFiltered" => $this->mod_transaksi->count_filtered_member(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}

?>