<?php
class Appliance extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_member');
		$this->load->model('mod_archive');
		$this->load->model('mod_instalasi');
	}

	public function index(){

			$id_kop = $this->session->userdata('id_kop');
			$data_aplikasi = $this->mod_archive->getListPengajuanMember($id_kop);
			$data_instalasi = $this->mod_instalasi->getListInstalasi();
			$pengajuan = $this->mod_archive->checkListPengajuan($id_kop);
			$berkas = $this->mod_archive->checkBerkas($id_kop);
			$data_berkas = $this->mod_archive->getBerkas($id_kop);

			$data_anggota = $this->mod_archive->getInfo($id_kop);


			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

			$npk = $this->session->userdata("npk");
			//$dataApplicant = $this->mod_member->getApplicant($npk);

			$body = $this->load->view('member/page/archive_member', array('data'=>$data_anggota,'instalasi'=>$data_instalasi,'applikasi'=>$data_aplikasi,'data_berkas'=>$data_berkas,'berkas'=>$berkas,'pengajuan'=>$pengajuan),true);
			

			$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
			

			$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

			$this->load->view("member/master",array("data"=>$data));		

	}

	public function add_applikasi(){
		$id = $this->input->post('id');

		$status = "false";

		$id_kop = $this->input->post('id_anggota');
		$id_instalasi = $this->input->post('instalasi');
		$pinjaman = $this->input->post('pinjaman');
		$waktu = $this->input->post('tenor');
		$date = date('Y-m-d');
		$phone = $this->input->post('telepon');
		$tujuan = $this->input->post('tujuan');
		$istri = $this->input->post('istri');

		$kode = explode('-', $id_kop);

		$row = $this->mod_archive->checkPengajuan($id_kop);

			if($kode[1] == 'A'){
				$joinDate = $this->mod_member->getJoinDate($id_kop);
				$joinDate = strtotime($joinDate);
				$one = date('Y-m-d', strtotime('+ 1 years',$joinDate));
			    $five = date('Y-m-d', strtotime('+ 5 years',$joinDate));
			    $now = date('Y-m-d');
				if($one >= $now ){
					$status = "false";
					echo "<script>alert('Gagal Input Pengajuan! Anda Belum Boleh Mengajukan Pinjaman !');document.location.href='".site_url()."/member/appliance';</script>";
				}else if(($one <= $now) && ($five >= $now)){
					if($pinjaman > 5000000){
						$status = "false";
						echo "<script>alert('Gagal Input Pengajuan! Maximal Pinjaman 5.000.000 !');document.location.href='".site_url()."/member/appliance';</script>";
					}else{
						$status = "true";
					}
				}else if($five <= $now){
					if($pinjaman > 8000000){
						$status = "false";
						echo "<script>alert('Gagal Input Pengajuan! Maximal Pinjaman 8.000.000 !');document.location.href='".site_url()."/member/appliance';</script>";
					}else{
						$status = "true";
					}
				}
			}else{
				$status = "true";
			}

			if($status == "true"){
				if($id != "edit"){
					if($row>0){
						echo "<script>alert('Gagal Input Pengajuan! Masih ada pengajuan dalam proses!');document.location.href='".site_url()."/member/appliance';</script>";
					}else{
						$data = array(
							'id_appliance' => '' ,
							'id_kop' => $id_kop,
							'id_installation' => $id_instalasi,
							'value_of' => $pinjaman,
							'time_of' => $waktu,
							'create_date' => $date,
							'phone_number' => $phone,
							'usages' => $tujuan,
							'wife' => $istri
							);

						$insert = $this->mod_archive->addApp($data);

						if($insert){
							echo "<script>alert('Pengajuan anda sedang di proses!');document.location.href='".site_url()."/member/appliance';</script>";
						}else{
							echo "<script>alert('Pengajuan anda gagal di proses!');document.location.href='".site_url()."/member/appliance';</script>";
						}
					}
				}else{
					$data = array(
						'id_kop' => $id_kop,
						'id_installation' => $id_instalasi,
						'value_of' => $pinjaman,
						'time_of' => $waktu,
						'create_date' => $date,
						'phone_number' => $phone,
						'usages' => $tujuan,
						'wife' => $istri
						);

					$update = $this->mod_archive->update(array('id_appliance' => $this->input->post('id_appliance')), $data);

					if($update){
						echo "<script>alert('Pengajuan anda berhasil di edit!');document.location.href='".site_url()."/member/appliance';</script>";
					}else{
						echo "<script>alert('Pengajuan anda gagal di edit!');document.location.href='".site_url()."/member/appliance';</script>";
					}
				}
			}
			
	}

	public function detail_appliance($id){
		$id_kop = $this->session->userdata('id_kop');
		$data_aplikasi = $this->mod_archive->getDetailPengajuan($id);
		$berkas = $this->mod_archive->checkBerkas($id_kop);
		$data_berkas = $this->mod_archive->getBerkas($id_kop);

		$data_anggota = $this->mod_archive->getInfo($id_kop);

		$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

		$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);

		$npk = $this->session->userdata("npk");
		//$dataApplicant = $this->mod_member->getApplicant($npk);

		$body = $this->load->view('member/page/detail_pengajuan', array('berkas'=>$berkas,'data_berkas'=>$data_berkas,'applikasi'=>$data_aplikasi,'id_appliance'=>$id),true);
		

		$data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		

		$data['footer'] = $this->load->view('member/layout/foot',array("about"=>$about->result(),"random_news"=>$random_news->result()),true);

		$this->load->view("member/master",array("data"=>$data));

		// $data['header'] = $this->load->view('member/layout/header','',true);

		// $body = $this->load->view('member/page/detail', array('berkas'=>$berkas,'data_berkas'=>$data_berkas,'applikasi'=>$data_aplikasi,'id_appliance'=>$id),true);

		// $data['body'] = $this->load->view('member/layout/content',array('body'=>$body),true);
		
		// $data['sidebar'] = $this->load->view('member/layout/sidebar','',true);

		// $data['footer'] = $this->load->view('app/layout/footer','',true);
		
		// $this->load->view('app/layout/head',array('template'=>$data));
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_archive->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_delete($id)
	{
		$this->mod_archive->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function approve_applikasi(){
		$id_appliance = $this->input->post('id_appliance');
		$nilai = $this->input->post('nilai');
		$tenor = $this->input->post('tenor_baru');
		$tgl_cair = $this->input->post('tgl_cair');
		$status = $this->input->post('status');
		$nama = $this->input->post('nama');

		$data = array(
				'nilai_persetujuan' => $nilai ,
				'waktu_persetujuan' => $tenor,
				'rencana_pencairan' => $tgl_cair,
				'nama_menyetujui' => $nama,
				'status_appliance' => $status 
				);
		$update = $this->mod_archive->update(array('id_appliance' => $id_appliance), $data);

		if($update){
			echo "<script>alert('Pengajuan berhasil di Approve!');document.location.href='".site_url()."/member/appliance';</script>";
		}else{
			echo "<script>alert('Pengajuan gagal di Approve!');document.location.href='".site_url()."/member/appliance';</script>";
		}
	}

	public function upload_data(){
		
		$this->load->library('upload');
		
		$ktp = $_FILES["userfile1"]["name"];
        $kk = $_FILES["userfile2"]["name"];
        $slip = $_FILES["userfile3"]["name"];
        $buku = $_FILES["userfile4"]["name"];
		$id_card = $_FILES["userfile5"]["name"];
		$surat = $_FILES["userfile6"]["name"];
		$lain = $_FILES["userfile7"]["name"];
		
		$id_kop = $this->session->userdata('id_kop');
		$folder = array('ktp','kk','slip_gaji','buku_tabungan','id_card','surat_kuasa','npwp');
		$file = array($ktp,$kk,$slip,$buku,$id_card,$surat,$lain);

		if(!empty($file)){
        	for($i=0;$i<7;$i++){
        		if(empty($file[$i])){
        			continue;
        		}else{
        			$ext = explode(".",$file[$i]);
        			$filename = time() . "-" . rand(0,1000) . ".".$ext[1];

        			if($i == 0){$ktp = $filename;}
			        else if($i == 1){$kk = $filename;}
			        else if($i == 2){$slip = $filename;}
			        else if($i == 3){$buku = $filename;}
			        else if($i == 4){$id_card = $filename;}
			        else if($i == 5){$surat = $filename;}
			        else if($i == 6){$lain = $filename;}

        			$config = array(
			                'allowed_types' => 'jpg|png|jpeg|pdf',
			                'upload_path'=> 'public/file/anggota/archive/'.$folder[$i].'/',            
			                'max-size' => '2048',
			                'file_name' => $filename
			                );

        			$this->upload->initialize($config);
			        
			        $x = $this->upload->do_upload('userfile'.($i+1));
			        
			    }
			}
			
			$berkas = $this->mod_archive->checkBerkas($id_kop);

	        if($berkas < 1){
	        	$data = array(
	        			'id_berkas' => '' ,
	        			'id_kop' => $id_kop,
	        			'ktp' => $ktp,
	        			'kk' => $kk,
	        			'slip_gaji' => $slip,
	        			'buku_tabungan' => $buku,
	        			'id_card' => $id_card,
	        			'surat_kuasa' => $surat,
	        			'npwp' => $lain, 
	        			);
	        	$insert = $this->mod_member->addArchive($data);

	        	if($insert){
	        		echo "<script language=\"javascript\">alert('Berhasil Mengupload Berkas!');location='".base_url()."index.php/member/appliance';</script>";
	        	}else{
	        		echo "<script language=\"javascript\">alert('Gagal Mengupload Berkas!');location='".base_url()."index.php/member/appliance';</script>";
	        	}
	        }else{
	        	$update = "false";
	        	$file = array($ktp,$kk,$slip,$buku,$id_card,$surat,$lain);

	        	for($i=0;$i<7;$i++){
	        		if(empty($file[$i])){
	        			continue;
	        		}else{
	        			$x = $this->mod_member->updateArchive(array('id_kop' => $id_kop) , array($folder[$i] => $file[$i]));
	        			if($x){
	        				$update = "true";
	        			}else{
	        				$update = "false";
	        			}
	        		}
	        	}

	        	// $data = array(
	        	// 		'ktp' => $ktp,
	        	// 		'kk' => $kk,
	        	// 		'slip_gaji' => $slip,
	        	// 		'buku_tabungan' => $buku,
	        	// 		'id_card' => $id_card,
	        	// 		'surat_kuasa' => $surat,
	        	// 		'npwp' => $lain, 
	        	// 		);
	        	// $update = $this->mod_member->updateArchive(array('id_kop' => $id_kop), $data);

		        if($update == "true"){
		        	echo "<script language=\"javascript\">alert('Berhasil Update Berkas!');location='".base_url()."index.php/member/appliance';</script>";
		        }
		        else{
		        	echo "<script language=\"javascript\">alert('Gagal Update Berkas!');location='".base_url()."index.php/member/appliance';</script>";
		        }
	        }
			   
        }else{
    		echo "<script language=\"javascript\">alert('Gagal! Berkas Tidak Lengkap ');location='".base_url()."index.php/member/appliance';</script>";
		}
	}

	public function delete($npk){
		$delete = $this->mod_archive->deleteByUser($npk);

        if($delete){
        	echo "<script language=\"javascript\">alert('Berhasil Hapus Berkas!');location='".base_url()."index.php/member/archive';</script>";
        }
        else{
        	echo "<script language=\"javascript\">alert('Gagal Hapus Berkas!');location='".base_url()."index.php/member/archive';</script>";
        }
	}


}

?>