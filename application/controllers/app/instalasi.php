<?php
class instalasi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_instalasi');
		$this->load->library("Excel_reader");
	}

	public function index(){

		$data_cost_center = $this->mod_instalasi->getCostCenter();

		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/instalasi', array('data'=>$data_cost_center),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));			
	}

	public function ajax_list()
	{	
		$this->load->model('mod_cost_center');
		$list = $this->mod_instalasi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $instalasi) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $instalasi->id_instalasi;
			$row[] = $instalasi->nama_instalasi;
			$row[] = $this->mod_instalasi->GetStatus($instalasi->status_instalasi);
			$row[] = $instalasi->nama_cost_center;
			$row[] = $this->mod_cost_center->GetStatus($instalasi->status_cost_center);
			$row[] = $instalasi->nama_arh;
			//$row[] = $person->dob;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_instalasi('."'".$instalasi->id_instalasi."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_instalasi('."'".$instalasi->id_instalasi."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_instalasi->count_all(),
						"recordsFiltered" => $this->mod_instalasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_instalasi->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->form_validation->set_rules('id_instalasi', 'id_instalasi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('kode_cost', 'kode_cost', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nama_instalasi','nama_instalasi','required|xss_clean');
        $this->form_validation->set_rules('arh','ARH','required|xss_clean');
        $this->form_validation->set_rules('status','Status','required|number xss_clean');
        if ($this->form_validation->run() != FALSE)
        {
			$data = array(
				"id_instalasi"=>$this->input->post('id_instalasi'),
				"id_cost_center"=>$this->input->post('kode_cost'),
				"nama_instalasi"=>$this->input->post('nama_instalasi'),
				"nama_arh"=> $this->input->post('arh'),
				"status_instalasi" => $this->input->post('status')
				);
			$insert = $this->mod_instalasi->addInstalasi($data);
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array('status' => FALSE,'message'=>'data tidak lengkap'));
		}
	}

	public function ajax_update()
	{
		$this->form_validation->set_rules('kode_cost', 'kode_cost', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nama_instalasi','nama_instalasi','required|xss_clean');
        $this->form_validation->set_rules('arh','ARH','required|xss_clean');
        $this->form_validation->set_rules('status','Status','required|number xss_clean');
        if ($this->form_validation->run() != FALSE)
        {
		$data = array(
			"id_cost_center"=>$this->input->post('kode_cost'),
			"nama_instalasi"=>$this->input->post('nama_instalasi'),
			"nama_arh"=> $this->input->post('arh'),
			"status_instalasi" =>$this->input->post('status')
			);
		$this->mod_instalasi->update(array('id_instalasi' => $this->input->post('id_instalasi')), $data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_delete($id)
	{
		$this->mod_instalasi->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function upload_data(){

			$this->db->query("TRUNCATE TABLE `tbl_instalasi`");

			$name = $_FILES["userfile"]["name"];
	        $name_path = $_FILES["userfile"]["tmp_name"];

	        $this->excel_reader->read($name_path);

	        $worksheet = $this->excel_reader->sheets[0];
	        $baris = $worksheet['numRows'];


	        $numRows = $worksheet['numRows']; // ex: 14
	        $numCols = $worksheet['numCols']; // ex: 4
	        $cells = $worksheet['cells']; // the 1st row are usually the field's name

	        $x = 0;

	        for ($i=2; $i <=$numRows ; $i++) { 
	        	$x +=1;

				$kode_instalasi = $cells[$i][1];
				$nama_instalasi = $cells[$i][2];
				$kode_cost = $cells[$i][3];
				$arh = $cells[$i][4];
				$status = $cells[$i][5];
	           	
	           	$data = array(
	           		"id_instalasi"=>$kode_instalasi,
	           		"id_cost_center"=>$kode_cost,
	           		"nama_instalasi"=>$nama_instalasi,
	           		"nama_arh"=>$arh,
	           		"status_instalasi"=>$status);

				$insert = $this->mod_instalasi->addInstalasi($data);
			}
			if($insert){
				echo "<script>alert('Berhasil Input Instalasi');document.location.href='".site_url()."/app/instalasi';</script>";
			}else{
				echo "<script>alert('Gagal Input Instalasi');document.location.href='".site_url()."/app/instalasi';</script>";
			}
			
		} 
}

?>