<?php
class company extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_cost_center');
		$this->load->library("Excel_reader");
	}

	private function cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect("app/login");
		}
	}

	public function index(){

		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/company', array(),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));			
	}

	public function ajax_list()
	{
		$list = $this->mod_cost_center->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cost) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $cost->id_cost_center;
			$row[] = $cost->nama_cost_center;
			$row[] = $this->mod_cost_center->GetStatus($cost->status_cost_center);
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_cost('."'".$cost->id_cost_center."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_cost('."'".$cost->id_cost_center."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_cost_center->count_all(),
						"recordsFiltered" => $this->mod_cost_center->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_cost_center->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->form_validation->set_rules('id_cost', 'id_cost_center', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nama_cost', 'nama_cost_center', 'trim|required|xss_clean');
        $this->form_validation->set_rules('status','status_cost_center','required|number xss_clean');
        if ($this->form_validation->run() != FALSE)
        {
        $data = array(
			"id_cost_center"=>$this->input->post('id_cost'),
			"nama_cost_center"=>$this->input->post('nama_cost'),
			"status_cost_center"=>$this->input->post('status')
			);
		$insert = $this->mod_cost_center->addCost($data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_update()
	{
		$this->form_validation->set_rules('nama_cost', 'nama_cost_center', 'trim|required|xss_clean');
        $this->form_validation->set_rules('status','status_cost_center','required|number xss_clean');
        if ($this->form_validation->run() != FALSE)
        {
		$data = array(
			"nama_cost_center"=>$this->input->post('nama_cost'),
			"status_cost_center"=>$this->input->post('status')
			);
		$this->mod_cost_center->update_status(array('id_cost_center' => $this->input->post('id_cost')), $this->input->post('status'));
		$this->mod_cost_center->update(array('id_cost_center' => $this->input->post('id_cost')), $data);
		echo json_encode(array("status" => TRUE));
		} else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_delete($id)
	{
		$this->mod_cost_center->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function upload_data(){

			$this->db->query("TRUNCATE TABLE `tbl_cost_center`");
			$name = $_FILES["userfile"]["name"];
	        $name_path = $_FILES["userfile"]["tmp_name"];

	        $this->excel_reader->read($name_path);

	        $worksheet = $this->excel_reader->sheets[0];
	        $baris = $worksheet['numRows'];


	        $numRows = $worksheet['numRows']; // ex: 14
	        $numCols = $worksheet['numCols']; // ex: 4
	        $cells = $worksheet['cells']; // the 1st row are usually the field's name

	        $x = 0;

	        for ($i=2; $i <=$numRows ; $i++) { 
	        	$x +=1;


	            $id_cost_center = $cells[$i][1];
	            $nama_cost_center = $cells[$i][2];
	            $status_cost_center = $cells[$i][3];
	           	
	           	$data = array(
	           				"id_cost_center"=>$id_cost_center,
	           				"nama_cost_center"=>$nama_cost_center,
	           				"status_cost_center"=>$status_cost_center,
	           				);

	           $insert = $this->mod_cost_center->addCost($data);
			}
			if($insert){
				echo "<script>alert('Berhasil Input Cost Center');document.location.href='".site_url()."/app/company';</script>";
			}else{
				echo "<script>alert('Gagal Input Cost Center');document.location.href='".site_url()."/app/company';</script>";
			}
			
		}

}

?>