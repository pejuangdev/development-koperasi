<?php
class Transaksi extends CI_Controller {

	var $bulan = "";
	var $tahun = "";

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_instalasi');
		$this->load->model('mod_anggota');
		$this->load->model('mod_transaksi');
		$this->load->library("Excel_reader");
	}

	public function index(){
		
		$type = $this->input->get('type');
		if(empty($type)){
			$type = "simpanan";
		}

		$month = $this->input->get('bulan');
		$year = $this->input->get('tahun');

		if(!empty($month)&&!empty($year)){
			$this->bulan = $month;
			$this->tahun = $year;
		}else{
			$data = $this->mod_transaksi->getMax();
		 	foreach ($data as $key => $value) {
		 		$this->bulan = $value->bulan;
		 		$this->tahun = $value->tahun;
		 	}
		}

		$data_anggota = $this->mod_anggota->getMember();
		$data_instalasi = $this->mod_instalasi->getListInstalasi();

		$data['header'] = $this->load->view('app/layout/header','',true);

		if($type=="simpanan"){
			$body = $this->load->view('app/page/transaksi', array('anggota'=>$data_anggota,'instalasi'=>$data_instalasi,'bulan'=>$this->bulan,'tahun'=>$this->tahun),true);
		}else if($type=="potongan"){
			$body = $this->load->view('app/page/potongan', array('anggota'=>$data_anggota,'instalasi'=>$data_instalasi,'bulan'=>$this->bulan,'tahun'=>$this->tahun),true);
		}else if($type=="lainnya"){
			$body = $this->load->view('app/page/lainnya', array('anggota'=>$data_anggota,'instalasi'=>$data_instalasi,'bulan'=>$this->bulan,'tahun'=>$this->tahun),true);
		}
		
		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));			
	}

        public function remove_all($month="",$year=""){
            $this->db->where(array('bulan_transaksi'=>$month,'tahun_transaksi'=>$year));
            $this->db->delete('tbl_transaksi');
            redirect(site_url()."/app/transaksi");
        }

	public function ajax_list($month="",$year="")
	{	
		
		$this->mod_transaksi->setDate($month,$year);
		$list = $this->mod_transaksi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $transaksi) {
			
			$no++;
			$row = array();
			$row[] = $transaksi->id_kop;
			$row[] = $transaksi->nama_anggota;
			$row[] = $transaksi->nama_instalasi;
			$row[] = $this->mod_transaksi->listBulan($transaksi->bulan_transaksi) . "-" . $transaksi->tahun_transaksi;
			$row[] = number_format($transaksi->simpanan_wajib);
			$row[] = number_format($transaksi->simpanan_pokok);
			$row[] = number_format($transaksi->simpanan_sukarela);
			$row[] = number_format($transaksi->SHU);
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_transaksi->count_all(),
						"recordsFiltered" => $this->mod_transaksi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_potongan($month="",$year="")
	{	
		if(empty($month) && empty($year)){
		 	$data = $this->mod_transaksi->getMax();
		 	foreach ($data as $key => $value) {
		 		$month = $value->bulan;
		 		$year = $value->tahun;
		 	}
		 }
		$this->mod_transaksi->setOrder(array('tbl_transaksi.id_kop','nama_anggota','nama_instalasi','tahun_transaksi','potongan_belanja','potongan_sembako','potongan_icare')); 
		$this->mod_transaksi->setDate($month,$year);
		$list = $this->mod_transaksi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $transaksi) {
			
			$no++;
			$row = array();
			$row[] = $transaksi->id_kop;
			$row[] = $transaksi->nama_anggota;
			$row[] = $transaksi->nama_instalasi;
			$row[] = $this->mod_transaksi->listBulan($transaksi->bulan_transaksi) . "-" . $transaksi->tahun_transaksi;
			$row[] = number_format($transaksi->potongan_belanja);
			$row[] = number_format($transaksi->potongan_sembako);
			$row[] = number_format($transaksi->potongan_icare);
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_transaksi->count_all(),
						"recordsFiltered" => $this->mod_transaksi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_lainnya($month="",$year="")
	{	
		if(empty($month) && empty($year)){
		 	$data = $this->mod_transaksi->getMax();
		 	foreach ($data as $key => $value) {
		 		$month = $value->bulan;
		 		$year = $value->tahun;
		 	}
		 }
		$this->mod_transaksi->setOrder(array('tbl_transaksi.id_kop','nama_anggota','nama_instalasi','tahun_transaksi','potongan_emoney','potongan_motor','potongan_or','potongan_other')); 
		$this->mod_transaksi->setDate($month,$year);
		$list = $this->mod_transaksi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $transaksi) {
			
			$no++;
			$row = array();
			$row[] = $transaksi->id_kop;
			$row[] = $transaksi->nama_anggota;
			$row[] = $transaksi->nama_instalasi;
			$row[] = $this->mod_transaksi->listBulan($transaksi->bulan_transaksi) . "-" . $transaksi->tahun_transaksi;
			$row[] = number_format($transaksi->potongan_emoney);
			$row[] = number_format($transaksi->potongan_motor);
			$row[] = number_format($transaksi->potongan_or);
			$row[] = number_format($transaksi->potongan_other);
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_transaksi('."'".$transaksi->id_transaksi."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_transaksi->count_all(),
						"recordsFiltered" => $this->mod_transaksi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_transaksi->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->form_validation->set_rules('npk', 'NPK', 'trim|required|xss_clean');
        $this->form_validation->set_rules('instalasi', 'instalasi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan','Bulan','required|number');
        $this->form_validation->set_rules('tahun','Tahun','required|number');
        if ($this->form_validation->run() != FALSE)
        {
        $id_kop = $this->input->post('npk');
		$instalasi = $this->input->post('instalasi');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sim_jip = $this->input->post('sim_jip');
		$sim_kok = $this->input->post('sim_pok');
		$sim_suk = $this->input->post('sim_suk');
		$pot_bel = $this->input->post('pot_bel');
		$pot_sem = $this->input->post('pot_sem');
		$pot_or = $this->input->post('pot_or');
		$icare = $this->input->post('icare');
		$emoney = $this->input->post('emoney');
		$motor = $this->input->post('motor');
		$shu = $this->input->post('shu');
		$other = $this->input->post('other');

		$data = array(
					"id_transaksi"=> "",
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"id_kop" =>$id_kop,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or,
					"SHU" =>$shu,
					"potongan_icare"=>$icare,
					"potongan_emoney"=>$emoney,
					"potongan_motor"=>$motor,
					"potongan_other"=>$other
					);

		$insert = $this->mod_transaksi->addTrans($data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_update()
	{
		$this->form_validation->set_rules('npk', 'NPK', 'trim|required|xss_clean');
        $this->form_validation->set_rules('instalasi', 'instalasi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan','Bulan','required|number');
        $this->form_validation->set_rules('tahun','Tahun','required|number');
        if ($this->form_validation->run() != FALSE)
        {
        $id_kop = $this->input->post('npk');
		$instalasi = $this->input->post('instalasi');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sim_jip = $this->input->post('sim_jip');
		$sim_kok = $this->input->post('sim_pok');
		$sim_suk = $this->input->post('sim_suk');
		$pot_bel = $this->input->post('pot_bel');
		$pot_sem = $this->input->post('pot_sem');
		$pot_or = $this->input->post('pot_or');
		$icare = $this->input->post('icare');
		$emoney = $this->input->post('emoney');
		$motor = $this->input->post('motor');
		$shu = $this->input->post('shu');
		$other = $this->input->post('other');

		$data = array(
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"id_kop" =>$id_kop,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or,
					"SHU" =>$shu,
					"potongan_icare"=>$icare,
					"potongan_emoney"=>$emoney,
					"potongan_motor"=>$motor,
					"potongan_other"=>$other
					);

		$this->mod_transaksi->update(array('id_transaksi' => $this->input->post('id_transaksi')), $data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_delete($id)
	{
		$this->mod_transaksi->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function upload(){
		
		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/transact_upload', array(),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));
	}

	public function upload_data(){

			$tahun = $this->input->post("tahun");  
			$bulan = $this->input->post("bulan");

			$name = $_FILES["userfile"]["name"];
	        $name_path = $_FILES["userfile"]["tmp_name"];

	        $this->excel_reader->read($name_path);

	        $worksheet = $this->excel_reader->sheets[0];
	        $baris = $worksheet['numRows'];


	        $numRows = $worksheet['numRows']; // ex: 14
	        $numCols = $worksheet['numCols']; // ex: 4
	        $cells = $worksheet['cells']; // the 1st row are usually the field's name

	        $x = 0;

	        for ($i=2; $i <=$numRows ; $i++) { 
	        	$x +=1;

				$id_kop = $cells[$i][1];
				$instalasi = $cells[$i][2];
				$sim_jip = $cells[$i][3];
				$sim_kok = $cells[$i][4];
				$sim_suk = $cells[$i][5];
				$shu = $cells[$i][6];
				$pot_bel = $cells[$i][7];
				$pot_sem = $cells[$i][8];
				$pot_icare = $cells[$i][9];
				$pot_emoney = $cells[$i][10];
				$pot_motor = $cells[$i][11];
				$pot_or = $cells[$i][12];
				$pot_other = $cells[$i][13];
				
				$data = array(
					"id_transaksi"=> "",
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"id_kop" =>$id_kop,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"SHU"=>$shu,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or,
					"potongan_icare"=>$pot_icare,
					"potongan_emoney"=>$pot_emoney,
					"potongan_motor"=>$pot_motor,
					"potongan_other"=>$pot_other
					);

				$insert = $this->mod_transaksi->addTrans($data);
			}
			if($insert){
				echo "<script>alert('Berhasil Input Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
			}else{
				echo "<script>alert('Gagal Input Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
			}
			
		} 

	// public function upload_data(){

	// 	$tahun = $this->input->post("tahun");  
	// 	$bulan = $this->input->post("bulan");	

	// 	$this->load->library('upload');
		    
 //        $dt = $_FILES['userfile']['name'];
 //        $cx = $_FILES['userfile']['tmp_name'];
	//     $file_ext = explode(".", $dt);
 //        $file_ext = $file_ext[1];
 //    	$filename = time().".".$file_ext;
 //    	move_uploaded_file($cx, "public/file/transaksi/".$filename);

 //    	$excel = 'public/file/transaksi/'.$filename;

 //    	$this->load->library('excel');

 //    	$objPHPExcel = PHPExcel_IOFactory::load($excel);

 //    	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	// 		$highrow = $worksheet->getHighestRow();
	// 		for($row=2;$row<=$highrow;$row++){
	// 			$npk = $worksheet->getCellByColumnAndRow(2,$row)->getValue();
	// 			$instalasi = $worksheet->getCellByColumnAndRow(3,$row)->getValue();
	// 			$sim_jip = $worksheet->getCellByColumnAndRow(4,$row)->getValue();
	// 			$sim_kok = $worksheet->getCellByColumnAndRow(5,$row)->getValue();
	// 			$sim_suk = $worksheet->getCellByColumnAndRow(6,$row)->getValue();
	// 			$pinjaman = $worksheet->getCellByColumnAndRow(7,$row)->getValue();
	// 			$pot_bel = $worksheet->getCellByColumnAndRow(8,$row)->getValue();
	// 			$pot_sem = $worksheet->getCellByColumnAndRow(9,$row)->getValue();
	// 			$pot_or = $worksheet->getCellByColumnAndRow(10,$row)->getValue();
							

	// 			$data = array(
	// 				"id_transaksi"=> "",
	// 				"bulan_transaksi"=>$bulan,
	// 				"tahun_transaksi"=>$tahun,
	// 				"npk" =>$npk,
	// 				"id_instalasi"=>$instalasi,
	// 				"simpanan_wajib"=>$sim_jip,
	// 				"simpanan_pokok"=>$sim_kok,
	// 				"simpanan_sukarela"=>$sim_suk,
	// 				"pinjaman"=>$pinjaman,
	// 				"potongan_belanja"=>$pot_bel,
	// 				"potongan_sembako"=>$pot_sem,
	// 				"potongan_or"=>$pot_or
	// 				);

	// 			$insert = $this->mod_transaksi->addTrans($data);
	// 		}
	// 	}
	// 	unlink($excel);
	// 	if($insert){
	// 		echo "<script>alert('Berhasil Input Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
	// 	}else{
	// 		echo "<script>alert('Gagal Input Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
	// 	}


    	
	// }

	public function save_data(){
		$npk = $this->input->post('npk');
		$instalasi = $this->input->post('instalasi');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sim_jip = $this->input->post('sim_jip');
		$sim_kok = $this->input->post('sim_kok');
		$sim_suk = $this->input->post('sim_suk');
		$pot_bel = $this->input->post('pot_bel');
		$pot_sem = $this->input->post('pot_sem');
		$pot_or = $this->input->post('pot_or');

		$data = array(
					"id_transaksi"=> "",
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"npk" =>$npk,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or
					);

		$insert = $this->mod_transaksi->addTrans($data);

		if($insert){
			echo "<script>alert('Berhasil Input Data Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}else{
			echo "<script>alert('Gagal Input Data Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}

	}

	public function edit($id){
		$data_anggota = $this->mod_transaksi->getListMember();
		$data_instalasi = $this->mod_instalasi->getListInstalasi();
		$data_transaksi = $this->mod_transaksi->getWhereTrans($id);

		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/edit_transaksi', array('data'=>$data_transaksi,'anggota'=>$data_anggota,'instalasi'=>$data_instalasi),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));
	}

	public function update_data($id){
		$npk = $this->input->post('npk');
		$instalasi = $this->input->post('instalasi');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sim_jip = $this->input->post('sim_jip');
		$sim_kok = $this->input->post('sim_kok');
		$sim_suk = $this->input->post('sim_suk');
		$pot_bel = $this->input->post('pot_bel');
		$pot_sem = $this->input->post('pot_sem');
		$pot_or = $this->input->post('pot_or');

		$data = array(
					"bulan_transaksi"=>$bulan,
					"tahun_transaksi"=>$tahun,
					"npk" =>$npk,
					"id_instalasi"=>$instalasi,
					"simpanan_wajib"=>$sim_jip,
					"simpanan_pokok"=>$sim_kok,
					"simpanan_sukarela"=>$sim_suk,
					"potongan_belanja"=>$pot_bel,
					"potongan_sembako"=>$pot_sem,
					"potongan_or"=>$pot_or
					);

		$update = $this->mod_transaksi->updateTrans($data,$id);

		if($update){
			echo "<script>alert('Berhasil Update Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}else{
			echo "<script>alert('Gagal Update Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}
	}

	public function delete($id){
		$delete = $this->mod_transaksi->delTrans($id);
		if($delete){
			echo "<script>alert('Berhasil Hapus Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}else{
			echo "<script>alert('Gagal Hapus Transaksi');document.location.href='".site_url()."/app/transaksi';</script>";
		}
	}

}

?>