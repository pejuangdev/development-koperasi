<?php
class anggota extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('mod_anggota');
		$this->load->library("Excel_reader");
	}

	public function index(){

		$dataAnggota = $this->mod_anggota->getMember();
		
		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/member', array('data'=>$dataAnggota),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));	
	}

        public function delete_all(){
            $this->db->query('TRUNCATE `tbl_anggota`');
            redirect(site_url().'app/anggota');
        }

	public function ajax_list()
	{
		$list = $this->mod_anggota->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$status = "";
		foreach ($list as $person) {
			if($person->status_anggota == 1){
				$status = "Aktif";
			}else{
				$status = "Tidak Aktif";
			}
			$no++;
			$row = array();
			$row[] = $person->id_kop;
			$row[] = $person->npk;
			$row[] = $person->nama_anggota;
			$row[] = $person->tanggal_lahir;
			$row[] = $person->tanggal_gabung;
			$row[] = $person->tanggal_keluar;
			$row[] = $person->alamat_anggota;
			$row[] = $status;
			
			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$person->id_kop."'".')"><i class="fa fa-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$person->id_kop."'".')"><i class="fa fa-trash-o"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mod_anggota->count_all(),
						"recordsFiltered" => $this->mod_anggota->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->mod_anggota->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->form_validation->set_rules('id_kop', 'id_kop', 'trim|required|number xss_clean');
        $this->form_validation->set_rules('npk', 'npk', 'trim|required|number xss_clean');
        $this->form_validation->set_rules('nama', 'nama_anggota', 'trim|required|xss_clean');
        $this->form_validation->set_rules('status','status_anggota','required|number xss_clean');
        $this->form_validation->set_rules('tgl_lahir','tanggal_lahir','required|date xss_clean');
        $this->form_validation->set_rules('tgl_gabung','tanggal_gabung','required|date xss_clean');
        if ($this->form_validation->run() != FALSE)
        {
        $id_kop = $this->input->post('id_kop');
		$npk = $this->input->post('npk');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$tgl_gabung = $this->input->post('tgl_gabung');
		$status = $this->input->post('status');

		$exPass = explode("-", $tgl_lahir);
		$exPass = $exPass[2].$exPass[1].$exPass[0];

		$data = array(
		 	"id_kop"=>$id_kop,
		 	"npk"=>$npk,
		 	"password"=>$exPass,
		 	"nama_anggota"=>$nama,
		 	"tanggal_lahir"=>$tgl_lahir,
		 	"tanggal_gabung"=>$tgl_gabung,
		 	"alamat_anggota"=>$alamat,
		 	"immediately_password"=>1,
		 	"status_anggota"=>$status
		);

		$insert = $this->mod_anggota->addAnggota($data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_update()
	{
		$this->form_validation->set_rules('npk', 'npk', 'trim|required|number xss_clean');
        $this->form_validation->set_rules('nama', 'nama_anggota', 'trim|required|xss_clean');
        $this->form_validation->set_rules('status','status_anggota','required|number xss_clean');
        $this->form_validation->set_rules('tgl_lahir','tanggal_lahir','required|date xss_clean');
        $this->form_validation->set_rules('tgl_gabung','tanggal_gabung','required|date xss_clean');
        if ($this->form_validation->run() != FALSE)
        {
		$id_kop = $this->input->post('id_kop');
		$npk = $this->input->post('npk');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$tgl_keluar = $this->input->post('tgl_keluar');
		$tgl_gabung = $this->input->post('tgl_gabung');
		if($tgl_keluar == ''){
			$tgl_keluar = NULL;
		}
		if($tgl_lahir == ''){
			$tgl_lahir = NULL;
		} if($tgl_gabung == ''){
			$tgl_gabung = NULL;
		}

		$status = $this->input->post('status');
		
		$data = array(
			"npk"=>$npk,
			"nama_anggota"=>$nama,
			"tanggal_lahir"=>$tgl_lahir,
			"tanggal_gabung"=>$tgl_gabung,
			"tanggal_keluar"=>$tgl_keluar,
			"alamat_anggota"=>$alamat,
			"status_anggota"=>$status
		);

		$this->mod_anggota->update(array('id_kop' => $this->input->post('id_kop')), $data);
		echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_delete($id)
	{
		$this->mod_anggota->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function upload_data(){

			$this->db->query("TRUNCATE TABLE `tbl_anggota`");

			$name = $_FILES["userfile"]["name"];
	        $name_path = $_FILES["userfile"]["tmp_name"];

	        $this->excel_reader->read($name_path);

	        $worksheet = $this->excel_reader->sheets[0];
	        $baris = $worksheet['numRows'];


	        $numRows = $worksheet['numRows']; // ex: 14
	        $numCols = $worksheet['numCols']; // ex: 4
	        $cells = $worksheet['cells']; // the 1st row are usually the field's name

	        $x = 0;

	        for ($i=2; $i <=$numRows ; $i++) { 
	        	$x +=1;

				$id_kop = $cells[$i][1];
				$npk = $cells[$i][2];
				$nama_anggota = $cells[$i][3];
				$alamat = $cells[$i][4];
				$tgl_lahir = $cells[$i][5];
				$tgl_gabung = $cells[$i][6];
				$status_anggota = $cells[$i][7];
				
				//echo $tgl_lahir."<br>";
				$exPass = explode("-", $tgl_lahir);
				$joinDate = explode("-", $tgl_gabung);
				if($exPass[1]=="Jan"){$bulan == "01";}
				elseif($exPass[1]=="Feb"){$bulan = "02";}
				elseif($exPass[1]=="Mar"){$bulan = "03";}
				elseif($exPass[1]=="Apr"){$bulan = "04";}
				elseif ($exPass[1]=="May"){$bulan = "05";}
				elseif ($exPass[1]=="Jun"){$bulan = "06";}
				elseif ($exPass[1]=="Jul"){$bulan = "07";}
				elseif ($exPass[1]=="Aug"){$bulan = "08";}
				elseif ($exPass[1]=="Sep"){$bulan = "09";}
				elseif ($exPass[1]=="Oct"){$bulan = "10";}
				elseif ($exPass[1]=="Nov"){$bulan = "11";}
				elseif ($exPass[1]=="Dec"){$bulan = "12";}
				else {$bulan = "00";}
				if($joinDate[1]=="Jan"){$bulanJoin == "01";}
				elseif($joinDate[1]=="Feb"){$bulanJoin = "02";}
				elseif($joinDate[1]=="Mar"){$bulanJoin = "03";}
				elseif($joinDate[1]=="Apr"){$bulanJoin = "04";}
				elseif ($joinDate[1]=="May"){$bulanJoin = "05";}
				elseif ($joinDate[1]=="Jun"){$bulanJoin = "06";}
				elseif ($joinDate[1]=="Jul"){$bulanJoin = "07";}
				elseif ($joinDate[1]=="Aug"){$bulanJoin = "08";}
				elseif ($joinDate[1]=="Sep"){$bulanJoin = "09";}
				elseif ($joinDate[1]=="Oct"){$bulanJoin = "10";}
				elseif ($joinDate[1]=="Nov"){$bulanJoin = "11";}
				elseif ($joinDate[1]=="Dec"){$bulanJoin = "12";}
				else  {$joinDate="00";}


				$trueBorn = $exPass[2]."-".$bulan."-".$exPass[0];
				$trueJoin = $joinDate[2]."-".$bulanJoin."-".$joinDate[0];
				$pwd = $exPass[0].$bulan.$exPass[2];

						
				$data = array(
					"id_kop"=>$id_kop,
					"npk"=>$npk,
					"password"=>$pwd,
					"nama_anggota"=>$nama_anggota,
					"tanggal_lahir"=>$trueBorn,
					"tanggal_gabung"=>$trueJoin,
					"alamat_anggota"=>$alamat,
					"immediately_password"=>1,
					"status_anggota"=>$status_anggota
					);

				 $insert = $this->mod_anggota->addAnggota($data);
			}
			if($insert){
				echo "<script>alert('Berhasil Input Anggota');document.location.href='".site_url()."/app/anggota';</script>";
			}else{
				echo "<script>alert('Gagal Input Anggota');document.location.href='".site_url()."/app/anggota';</script>";
			}
			
		}

	public function forgot_password(){

		$forgotData = $this->mod_anggota->getForgotPassword();

		$data['header'] = $this->load->view('app/layout/header','',true);

		$body = $this->load->view('app/page/forgot_password', array('data'=>$forgotData),true);

		$data['body'] = $this->load->view('app/layout/content',array('body'=>$body),true);
		
		$data['sidebar'] = $this->load->view('app/layout/sidebar','',true);

		$data['footer'] = $this->load->view('app/layout/footer','',true);
		
		$this->load->view('app/layout/head',array('template'=>$data));	
	} 
}

?>