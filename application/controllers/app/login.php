<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("mod_login_app");
	}

	public function index(){
		$this->load->view('app/login');
	}

	public function proseslog() {
		$data = array(
			'npk' => $this->input->post('npkTxt', TRUE),
			'password' => $this->input->post('passTxt', TRUE)
		);
		
		$hasil = $this->mod_login_app->GetUser($data);

		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				// $sess_data['logged_in'] = 'Sudah Login';
				$sess_data['role'] = "anggota";
				$sess_data['npk'] = $sess->npk;
				$sess_data['nama'] = $sess->nama_anggota;
				$sess_data['immediately'] = $sess->immediately_password;
				$sess_data['status'] = $sess->status_anggota;
				$sess_data['password'] = $sess->password;
				$this->session->set_userdata($sess_data);
			}

			if ($this->session->userdata('status')=='1'){
				$this->session->set_userdata('useradmin', $sess_data);
				redirect(base_url()."index.php/app/anggota");
			}

			else{
				$this->session->set_userdata('adminfailed', $sess_data);
				redirect(base_url());
			}		
		}

		else {
			$info='<div style="color:red">PERIKSA KEMBALI NAMA PENGGUNA DAN PASSWORD ANDA!</div>';
			$this->session->set_userdata('info',$info);

			redirect(base_url().'index.php/app/login');
		}
	}

	public function logout(){ 
			$this->session->sess_destroy();
			$this->session->set_flashdata(array("messages"=>"Udah Logout"));
			redirect("app/login"); 
	}

	public function forgotPass(){
		$this->load->model("mod_anggota");
		$npk = $this->input->post('npk');

		$data = array("forgot_password"=>1);

		$forgot = $this->mod_anggota->doForgotPassword($data,$npk);

		if($forgot){
			echo "<script>alert('Berhasil Dikirimkan Ke Admin');document.location.href='".site_url()."/app/login';</script>";
		}else{
			echo "<script>alert('Gagal');document.location.href='".site_url()."/app/login';</script>";
		}
	}
}