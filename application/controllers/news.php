<?php
	Class news Extends CI_Controller{ 

		public function index($id=0){

			$jml = $this->db->query("SELECT * FROM tbl_news")->num_rows();

			//pengaturan pagination
			$config['base_url'] = base_url().'index.php/news/index';
			$config['total_rows'] = $jml;
			$config['per_page'] = '6';

		   	$config['full_tag_open'] = "<ul class='pagination pagination-lg' style='position:relative; top:-25px;'>";
           	$config['full_tag_close'] ="</ul>";
		   	$config['num_tag_open'] = '<li>';
		   	$config['num_tag_close'] = '</li>';
		   	$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		   	$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		   	$config['next_tag_open'] = "<li>";
		   	$config['next_tagl_close'] = "</li>";
		   	$config['prev_tag_open'] = "<li>";
		   	$config['prev_tagl_close'] = "</li>";
		   	$config['first_tag_open'] = "<li>";
		   	$config['first_tagl_close'] = "</li>";
		   	$config['last_tag_open'] = "<li>";
		   	$config['last_tagl_close'] = "</li>";

			 // $config['first_page'] = 'Awal';
			 // $config['last_page'] = 'Akhir';
			 // $config['next_page'] = '&laquo;';
			 // $config['prev_page'] = '&raquo;';

			 //inisialisasi config
			 $this->pagination->initialize($config);

			//buat pagination
			 $data['halaman'] = $this->pagination->create_links();

			 //tamplikan data
			$data['query'] = $this->db->query("SELECT * FROM tbl_news WHERE status_news>='1' ORDER BY tanggal_news desc LIMIT $id,".$config['per_page'])->result();

			$about = $this->db->query("SELECT * FROM tbl_about");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,4");

			if($this->session->userdata('id_kop')==NULL){
				$data['header'] = $this->load->view("front/tools/header",array("about"=>$about->result()),true); 
			}else{
				$data['header'] = $this->load->view('member/layout/header-member',array("about"=>$about->result()),true);
			}
			
			$body = $this->load->view('front/page/news',$data,true);
			$data['body'] = $this->load->view("front/tools/body",array("body"=>$body),true);
			$data['footer'] = $this->load->view("front/tools/footer",array("about"=>$about->result(),"random_news"=>$random_news->result()),true); 

			$this->load->view("front/master",array("data"=>$data)); 

		}

		public function detail($id){
			$about = $this->db->query("SELECT * FROM tbl_about");
			$detail_news = $this->db->query("SELECT * FROM tbl_news where id_news='$id'");
			$news = $this->db->query("SELECT * FROM tbl_news where status_news='1' order by tanggal_news desc limit 0,4");
			$random_news = $this->db->query("SELECT id_news,image_news FROM tbl_news where status_news='1' order by RAND() limit 0,6");

			$data['header'] = $this->load->view("front/tools/header",array("about"=>$about->result()),true); 
			
			$body = $this->load->view('front/page/detail_news',array("detail_news"=>$detail_news->result(),"recent_news"=>$news->result()),true);
			$data['body'] = $this->load->view("front/tools/body",array("body"=>$body),true);
			$data['footer'] = $this->load->view("front/tools/footer",array("about"=>$about->result(),"random_news"=>$random_news->result()),true); 

			$this->load->view("front/master",array("data"=>$data)); 

		}


	}
?>