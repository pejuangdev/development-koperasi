<?php
	Class Mod_product extends CI_Model{


		public function getProduct($limit,$offset){
			$q = $this->db->query("SELECT * FROM d_product WHERE status='1' ORDER BY id_product DESC LIMIT ".$limit." OFFSET ".$offset);
			return $q->result();
		}

		public function getProductCategory($limit,$offset,$id){
			$q = $this->db->query("SELECT * FROM d_product WHERE id_cake_category='$id' ORDER BY id_product DESC LIMIT ".$limit." OFFSET ".$offset);
			return $q->result();
		}

		public function addProductData($data){
			return $this->db->insert("d_product", $data);
		}

		public function getProductRand($limit){
			$query = $this->db->query("SELECT * FROM d_product ORDER BY rand() LIMIT $limit");
			return $query->result();
		}

		public function getProductDetail($id){
			$q = $this->db->query("SELECT * FROM d_product WHERE id_product='$id'");
			return $q->result();
		}

		public function getByCategory($id){
			$q = $this->db->query("SELECT * FROM d_product WHERE id_cake_category='$id'");
			return $q->result();
		}

		public function count_all() {
			return $this->db->count_all('d_product');
		}


		//admin product
		public function getProductAdmin($limit,$offset) {
		    $data = $this->db->query("SELECT * FROM d_product ORDER BY id_product DESC LIMIT " .$limit. " OFFSET ".$offset);
		    return $data->result();
		 }

		 public function GetProduk($id) {
		    $data = $this->db->query("SELECT * FROM d_product WHERE id_product = '$id'");
		    return $data;
		 }

		 public function deleteProduct($id){
			return $this->db->delete("d_product", array("id_product"=>$id));
		}

		public function editProduct($data,$id){
			$dataUpdate = $this->db->where("id_product", $id);
			$dataUpdate = $this->db->update("d_product",$data);
			return $dataUpdate;
		}

		public function getEditProductAdmin($id){
			$q = "SELECT a.*, b.cake_category_title FROM d_product a INNER JOIN d_cake_category b ON a.id_cake_category=b.id_cake_category WHERE a.id_product='$id'";
			return $this->db->query($q)->result_array();
		}

	}
	
?>