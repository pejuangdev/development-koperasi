<?php
	Class Mod_cost_center extends CI_Model{

		var $table = 'tbl_cost_center';
		var $column_order = array(NULL,'id_cost_center','nama_cost_center','status_cost_center'); //set column field database for datatable orderable
		var $column_search = array('id_cost_center','nama_cost_center'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		var $order = array('id_cost_center' => 'desc'); // default order 

		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		private function _get_datatables_query()
		{
			
			$this->db->select("*");
			$this->db->from('tbl_cost_center');

			$i = 0;
		
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
					
					if($i===0) // first loop
					{
						//$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, $_POST['search']['value']);
					}
					else
					{
						$this->db->or_like($item, $_POST['search']['value']);
					}

					if(count($this->column_search) - 1 == $i){break;} //last loop
						//$this->db->group_end(); //close bracket
				}
				$i++;
			}
			
			if(isset($_POST['order'])) // here order processing
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		function get_datatables()
		{
			$this->_get_datatables_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function count_filtered()
		{
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all()
		{
			$this->db->from($this->table);
			return $this->db->count_all_results();
		}

		public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_cost_center',$id);
			$query = $this->db->get();

			return $query->row();
		}

		public function save($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

		public function update($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

		public function update_status($where,$status){
			if($status==0 || $status==2){
				$this->db->update('tbl_instalasi',array('status_instalasi'=>0),$where);
			}else{
				$this->db->update('tbl_instalasi',array('status_instalasi'=>1),$where);
			}
		}

		public function delete_by_id($id)
		{
			$this->db->where('id_cost_center', $id);
			$this->db->delete($this->table);
		}

		public function getCostCenter(){
			$q = $this->db->query("SELECT * FROM tbl_cost_center");
			return $q->result();
		}

		public function addCost($data){
			$q = $this->db->insert("tbl_cost_center",$data); 
			return $q;
		}

		public function getListCost(){
			$this->db->select('*');
			$query = $this->db->get('tbl_cost_center');
			return $query->result();
		}

		public function GetStatus($status){
			if($status == 0){
				return "Inactive";
			}else if($status == 1){
				return "Active";
			}else{
				return "Hold";
			}
		}
	}
?>