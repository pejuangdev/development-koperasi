<?php
	Class Mod_file extends CI_Model{

		public function getFile(){
			$q = $this->db->query("SELECT * FROM tbl_file ORDER BY id_file DESC"); 
			return $q->result();
		}

		//admin

		public function addFile($data){
			$q = $this->db->insert("tbl_file",$data); 
			return $q;
		}

		public function getWhere($id){
			$q = $this->db->get_where('tbl_file' ,array('id_file'=>$id));
			return $q->result();
		}

		public function updateFile($data,$id){
			$q = $this->db->where('id_file', $id);
			$q = $this->db->update('tbl_file', $data);
			return $q;
		}

		public function deleteFile($id){
			return $this->db->delete("tbl_file",array("id_file"=>$id));
		}

	}
	
?>