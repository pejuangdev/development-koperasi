<?php
	Class Mod_transaksi extends CI_Model{

		var $table = 'tbl_transaksi';
		var $column_order = array('tbl_transaksi.id_kop','nama_anggota','nama_instalasi','tahun_transaksi','simpanan_wajib','simpanan_pokok','simpanan_sukarela','SHU'); //set column field database for datatable orderable
		var $column_search = array('tbl_transaksi.id_kop','nama_anggota','nama_instalasi'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		var $order = array('id_transaksi' => 'desc'); // default order 
		var $bulan = 2;
		var $tahun = 2011;
		var $id = 0;


		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		private function _get_datatables_query()
		{
			
			$this->db->select("*");
			$this->db->join('tbl_anggota','tbl_transaksi.id_kop = tbl_anggota.id_kop');
			$this->db->join('tbl_instalasi','tbl_transaksi.id_instalasi = tbl_instalasi.id_instalasi');
			$this->db->from('tbl_transaksi');
			$this->db->where(array('bulan_transaksi' =>$this->bulan ,'tahun_transaksi' => $this->tahun));
			
			$i = 0;
		
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
					$search = $_POST['search']['value'];
					$this->db->where("(id_transaksi LIKE '%$search%' OR tbl_anggota.nama_anggota LIKE '%$search%' OR tbl_instalasi.nama_instalasi LIKE '%$search%')");
					
				}
				$i++;
			}
			
			if(isset($_POST['order'])) // here order processing
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		private function _get_datatables_member_query()
		{
			
			$this->db->select("*");
			$this->db->join('tbl_anggota','tbl_transaksi.id_kop = tbl_anggota.id_kop');
			$this->db->join('tbl_instalasi','tbl_transaksi.id_instalasi = tbl_instalasi.id_instalasi');
			$this->db->from('tbl_transaksi');
			$this->db->where(array('tbl_transaksi.id_kop'=>$this->id));
			
			$i = 0;
		
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
					$search = $_POST['search']['value'];
					$this->db->where("(id_transaksi LIKE '%$search%' OR tbl_anggota.nama_anggota LIKE '%$search%' OR tbl_instalasi.nama_instalasi LIKE '%$search%')");
					
				}
				$i++;
			}
			
			if(isset($_POST['order'])) // here order processing
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		function setDate($month,$year){
			$this->bulan = $month;
			$this->tahun = $year;
		}

		function setOrder($data){
			$this->column_order = $data;
		}

		function setID($id){
			$this->id = $id;
		}

		function get_datatables()
		{
			$this->_get_datatables_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function get_datatables_member()
		{
			$this->_get_datatables_member_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function count_filtered_member()
		{
			$this->_get_datatables_member_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		function count_filtered()
		{
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all()
		{
			$this->db->from($this->table);
			return $this->db->count_all_results();
		}

		public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_transaksi',$id);
			$query = $this->db->get();

			return $query->row();
		}

		public function save($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

		public function update($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

		public function delete_by_id($id)
		{
			$this->db->where('id_transaksi', $id);
			$this->db->delete($this->table);
		}

		public function getListTrans($bulan,$tahun){
			$this->db->select("tbl_transaksi.*,tbl_anggota.id_kop,tbl_anggota.nama_anggota,tbl_instalasi.nama_instalasi");
			$this->db->join("tbl_anggota","tbl_transaksi.npk = tbl_anggota.npk");
			$this->db->join("tbl_instalasi","tbl_transaksi.id_instalasi = tbl_instalasi.id_instalasi");
			$this->db->join("tbl_cost_center","tbl_instalasi.id_cost_center = tbl_cost_center.id_cost_center");

			$query = $this->db->get_where("tbl_transaksi",array("bulan_transaksi"=>$bulan,"tahun_transaksi"=>$tahun));
			return $query->result();
		}

		public function getList(){
			$this->db->select("*");
			$query = $this->db->get("tbl_transaksi");
			return $query->result();
		}

		public function getMax(){
			$this->db->select("max(bulan_transaksi) as bulan,max(tahun_transaksi) as tahun");
			$query = $this->db->get("tbl_transaksi");
			return $query->result();
		}

		public function addTrans($data){
			$q = $this->db->insert("tbl_transaksi",$data); 
			return $q;
		}

		public function hitung_total($field_name){
			$bulan = date("m");
			$tahun = date("Y");
			$bulan_in = $this->input->get('bulan');
			$tahun_in = $this->input->get('tahun');
			if(!empty($bulan_in) && !empty($tahun_in)){
				$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM tbl_transaksi WHERE bulan_transaksi='$bulan_in' AND tahun_transaksi='$tahun_in'")->result();
			}else{
				$data = $this->getMax();
				foreach ($data as $key => $value) {
					$bulan = $value->bulan;
					$tahun = $value->tahun;
				}
				$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM tbl_transaksi WHERE bulan_transaksi='$bulan' AND tahun_transaksi='$tahun'")->result();
			}
			
			foreach ($q as $key => $value);
			return $value->hasil;
		}

		public function hitung_total_member($field_name,$id){
			$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM tbl_transaksi WHERE id_kop='$id'")->result();
			foreach ($q as $key => $value);
			return $value->hasil;
		}

		public function delTrans($id){
			return $this->db->delete("tbl_transaksi",array("id_transaksi"=>$id));
		}

		public function updateTrans($data,$id){
			$q = $this->db->where('id_transaksi', $id);
			$q = $this->db->update('tbl_transaksi', $data);
			return $q;
		}

		public function getWhereTrans($id){
			$this->db->select("tbl_transaksi.*,tbl_anggota.id_kop,tbl_anggota.nama_anggota,tbl_instalasi.nama_instalasi");
			$this->db->join("tbl_anggota","tbl_transaksi.npk = tbl_anggota.npk");
			$this->db->join("tbl_instalasi","tbl_transaksi.id_instalasi = tbl_instalasi.id_instalasi");
			$this->db->join("tbl_cost_center","tbl_instalasi.id_cost_center = tbl_cost_center.id_cost_center");

			$query = $this->db->get_where("tbl_transaksi",array("id_transaksi"=>$id));
			return $query->result();
		}

		public function listBulan($i){
			$arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
			return $arr[$i];
		}
	}
?>