<?php
	Class Mod_staff extends CI_Model{

		public function getStaff(){
			$q = $this->db->query("SELECT * FROM tbl_staff ORDER BY id_staff DESC"); 
			return $q->result();
		}

		//admin
		public function deleteStaff($id){
			return $this->db->delete("tbl_staff", array("id_staff"=>$id));
		}

		public function addStaff($data){
			return $this->db->insert("tbl_staff",$data);
		}

		public function getWhere($id){
			return $this->db->get_where("tbl_staff", array("id_staff"=>$id))->result();
		}

		public function editStaff($data,$id){
			$q = $this->db->where("id_staff",$id);
			$q = $this->db->update("tbl_staff", $data);
			return $q;
		}

	}
	
?>