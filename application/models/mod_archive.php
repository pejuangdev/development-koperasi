<?php
	Class Mod_archive extends CI_Model{

		public function getInfo($id_kop){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_anggota',array('id_kop' => $id_kop));
			return $q->result();
		}

		public function getBunga(){
			$bunga = 0;
			$this->db->select('bunga');
			$q = $this->db->get('tbl_about')->result();

			foreach ($q as $key => $value) {
				$bunga = $value->bunga;
			}
			return $bunga;
		}

		public function addApp($data){
			$q = $this->db->insert('tbl_appliance',$data);
			return $q;
		}

		public function update($where, $data)
		{
			$this->db->update('tbl_appliance', $data, $where);
			return $this->db->affected_rows();
		}

		public function getListPengajuanMember($id_kop){
			$this->db->select('*');
			$this->db->join('tbl_anggota','tbl_anggota.id_kop = tbl_appliance.id_kop');
			$this->db->join('tbl_instalasi','tbl_instalasi.id_instalasi = tbl_appliance.id_installation');
			$q = $this->db->get_where('tbl_appliance',array('tbl_appliance.id_kop' => $id_kop));
			return $q->result();
		}

		public function getDetailPengajuan($id_appliance){
			$this->db->select('*');
			$this->db->join('tbl_anggota','tbl_anggota.id_kop = tbl_appliance.id_kop');
			$this->db->join('tbl_instalasi','tbl_instalasi.id_instalasi = tbl_appliance.id_installation');
			$q = $this->db->get_where('tbl_appliance',array('id_appliance' => $id_appliance));
			return $q->result();
		}

		public function checkBerkas($id_kop){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_berkas',array('id_kop' => $id_kop));
			return $q->num_rows();
		}

		public function getBerkas($id_kop){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_berkas',array('id_kop' => $id_kop));
			return $q->result();
		}
		
		public function checkPengajuan($id_kop){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_appliance',array('id_kop'=>$id_kop,'status_appliance'=>0));
			return $q->num_rows();
		}

		public function checkListPengajuan($id_kop){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_appliance',array('id_kop'=>$id_kop));
			return $q->num_rows();
		}

		public function get_by_id($id)
		{
			$this->db->from('tbl_appliance');
			$this->db->where('id_appliance',$id);
			$query = $this->db->get();

			return $query->row();
		}

		public function delete_by_id($id)
		{
			$this->db->where('id_appliance', $id);
			$this->db->delete('tbl_appliance');
		}
		
	}
?>