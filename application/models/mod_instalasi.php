<?php
	Class Mod_instalasi extends CI_Model{

		var $table = 'tbl_instalasi';
		var $column_order = array('id_instalasi','id_instalasi','nama_instalasi','status_instalasi','nama_cost_center','status_cost_center','nama_arh'); //set column field database for datatable orderable
		var $column_search = array('id_instalasi','nama_instalasi','nama_cost_center','nama_arh'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		var $order = array('id_instalasi' => 'desc'); // default order 

		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		private function _get_datatables_query()
		{
			
			$this->db->select("*");
			$this->db->join('tbl_cost_center','tbl_instalasi.id_cost_center = tbl_cost_center.id_cost_center');
			$this->db->from('tbl_instalasi');

			$i = 0;
		
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
					
					if($i===0) // first loop
					{
						//$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, $_POST['search']['value']);
					}
					else
					{
						$this->db->or_like($item, $_POST['search']['value']);
					}

					if(count($this->column_search) - 1 == $i){break;} //last loop
						//$this->db->group_end(); //close bracket
				}
				$i++;
			}
			
			if(isset($_POST['order'])) // here order processing
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		function get_datatables()
		{
			$this->_get_datatables_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function count_filtered()
		{
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all()
		{
			$this->db->from($this->table);
			return $this->db->count_all_results();
		}

		public function get_by_id($id)
		{
			$this->db->select("*");
			$this->db->join('tbl_cost_center','tbl_instalasi.id_cost_center = tbl_cost_center.id_cost_center');
			$query = $this->db->get_where('tbl_instalasi',array("id_instalasi"=>$id));
			return $query->row();
		}

		public function save($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

		public function addInstalasi($data){
			$q = $this->db->insert("tbl_instalasi",$data); 
			return $q;
		}

		public function update($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

		public function delete_by_id($id)
		{
			$this->db->where('id_instalasi', $id);
			$this->db->delete($this->table);
		}

		public function getListInstalasi(){
			$this->db->select('*');
			$this->db->join('tbl_cost_center','tbl_instalasi.id_cost_center = tbl_cost_center.id_cost_center');
			$query = $this->db->get('tbl_instalasi');
			return $query->result();
		}

		public function getCostCenter(){
			$q = $this->db->query("SELECT * FROM tbl_cost_center");
			return $q->result();
		}

		public function getListCost(){
			$this->db->select('*');
			$query = $this->db->get('tbl_cost_center');
			return $query->result();
		}

		public function GetStatus($status){
			if($status == 0){
				return "Inactive";
			}else if($status == 1){
				return "Active";
			}
		}
	}
?>