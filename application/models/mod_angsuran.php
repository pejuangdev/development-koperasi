<?php
	Class Mod_angsuran extends CI_Model{

		var $table = 'tbl_angsuran';
		var $column_order = array('id_angsuran','tbl_angsuran.id_pinjaman','npk','nama_anggota','jumlah_angsuran','tahun_angsuran','keterangan'); //set column field database for datatable orderable
		var $column_search = array('tbl_angsuran.id_pinjaman','nama_anggota','npk','keterangan'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		var $order = array('id_angsuran' => 'desc'); // default order 
		var $bulan = 2;
		var $tahun = 2011;
		var $id = 0;


		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		private function _get_datatables_query()
		{
			
			$this->db->select("*");
			$this->db->join('tbl_pinjaman','tbl_pinjaman.id_pinjaman = tbl_angsuran.id_pinjaman');
			$this->db->join('tbl_anggota','tbl_pinjaman.id_kop = tbl_anggota.id_kop');
			$this->db->from($this->table);
			$this->db->where(array('bulan_angsuran' =>$this->bulan ,'tahun_angsuran' => $this->tahun));
			
			$i = 0;
		
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
					$search = $_POST['search']['value'];
					$this->db->where("(tbl_anggota.nama_anggota LIKE '%$search%' OR tbl_angsuran.id_pinjaman LIKE'%$search%' OR tbl_anggota.npk LIKE'%$search%' OR tbl_angsuran.keterangan LIKE'%$search%')");
					
				}
				$i++;
			}
			
			if(isset($_POST['order'])) // here order processing
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		// private function _get_datatables_member_query()
		// {
			
		// 	$this->db->select("*");
		// 	$this->db->join('tbl_anggota','tbl_transaksi.id_kop = tbl_anggota.id_kop');
		// 	$this->db->join('tbl_instalasi','tbl_transaksi.id_instalasi = tbl_instalasi.id_instalasi');
		// 	$this->db->from('tbl_transaksi');
		// 	$this->db->where(array('tbl_transaksi.id_kop'=>$this->id));
			
		// 	$i = 0;
		
		// 	foreach ($this->column_search as $item) // loop column 
		// 	{
		// 		if($_POST['search']['value']) // if datatable send POST for search
		// 		{
		// 			$search = $_POST['search']['value'];
		// 			$this->db->where("(id_transaksi LIKE '%$search%' OR tbl_anggota.nama_anggota LIKE '%$search%' OR tbl_instalasi.nama_instalasi LIKE '%$search%')");
					
		// 		}
		// 		$i++;
		// 	}
			
		// 	if(isset($_POST['order'])) // here order processing
		// 	{
		// 		$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		// 	} 
		// 	else if(isset($this->order))
		// 	{
		// 		$order = $this->order;
		// 		$this->db->order_by(key($order), $order[key($order)]);
		// 	}
		// }

		function setDate($month,$year){
			$this->bulan = $month;
			$this->tahun = $year;
		}

		function setID($id){
			$this->id = $id;
		}

		function get_datatables()
		{
			$this->_get_datatables_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		// function get_datatables_member()
		// {
		// 	$this->_get_datatables_member_query();
		// 	if($_POST['length'] != -1)
		// 	$this->db->limit($_POST['length'], $_POST['start']);
		// 	$query = $this->db->get();
		// 	return $query->result();
		// }

		// function count_filtered_member()
		// {
		// 	$this->_get_datatables_member_query();
		// 	$query = $this->db->get();
		// 	return $query->num_rows();
		// }

		function count_filtered()
		{
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all()
		{
			$this->db->from($this->table);
			return $this->db->count_all_results();
		}

		public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_angsuran',$id);
			$query = $this->db->get();

			return $query->row();
		}

		public function save($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

		public function update($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

		public function delete_by_id($id)
		{
			$this->db->where('id_angsuran', $id);
			$this->db->delete($this->table);
		}

		public function getMax(){
			$this->db->select("max(bulan_angsuran) as bulan,max(tahun_angsuran) as tahun");
			$query = $this->db->get($this->table);
			return $query->result();
		}

		public function addAngsuran($data){
			$q = $this->db->insert($this->table,$data); 
			return $q;
		}

		public function hitung_total($field_name){
			$bulan = date("m");
			$tahun = date("Y");
			$bulan_in = $this->input->get('bulan');
			$tahun_in = $this->input->get('tahun');
			if(!empty($bulan_in) && !empty($tahun_in)){
				$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM $this->table WHERE bulan_angsuran='$bulan_in' AND tahun_angsuran='$tahun_in'")->result();
			}else{
				$data = $this->getMax();
				foreach ($data as $key => $value) {
					$bulan = $value->bulan;
					$tahun = $value->tahun;
				}
				$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM $this->table WHERE bulan_angsuran='$bulan' AND tahun_angsuran='$tahun'")->result();
			}
			
			foreach ($q as $key => $value);
			return $value->hasil;
		}

		// public function hitung_total_member($field_name,$id){
		// 	$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM tbl_transaksi WHERE id_kop='$id'")->result();
		// 	foreach ($q as $key => $value);
		// 	return $value->hasil;
		// }

		public function listBulan($i){
			$arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
			return $arr[$i];
		}
	}
?>