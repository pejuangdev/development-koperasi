<?php
	Class mod_member extends CI_Model{


		public function getMemberWhere($id){
			$q = $this->db->query("SELECT * FROM tbl_anggota WHERE id_kop='$id'");
			return $q->result();
		}

		function GetUser($data) {
	        $query = $this->db->get_where('tbl_anggota', $data);
	        return $query;
	    }

		public function getListTrans($npk){
			$this->db->select("tbl_transaksi.*,tbl_anggota.id_kop,tbl_anggota.nama_anggota,tbl_instalasi.nama_instalasi");
			$this->db->join("tbl_anggota","tbl_transaksi.id_kop = tbl_anggota.id_kop");
			$this->db->join("tbl_instalasi","tbl_transaksi.id_instalasi = tbl_instalasi.id_instalasi");
			$this->db->join("tbl_cost_center","tbl_instalasi.id_cost_center = tbl_cost_center.id_cost_center");

			$query = $this->db->get_where("tbl_transaksi",array("tbl_transaksi.id_kop"=>$npk));
			return $query->result();
		}

		public function getForgotPassword(){
			$q = $this->db->query("SELECT * FROM tbl_anggota WHERE forgot_password=1"); 
			return $q->result();
		}

		public function addArchive($data){
			$q = $this->db->insert("tbl_berkas",$data); 
			return $q;
		}

		public function updateArchive($where, $data)
		{
			$this->db->update('tbl_berkas', $data, $where);
			return $this->db->affected_rows();
		}

		public function addAppliance($data){
			$q = $this->db->insert("tbl_appliance",$data); 
			return $q;
		}

		public function getArchive(){
			$id_kop = $this->session->userdata('id_kop');
			$query = $this->db->get_where('tbl_berkas', array('id_kop' => $id_kop));
	        if ($query->num_rows() == 1) {
	            $result = $query->result();
	            return $result;
	        } else {
	            return FALSE;
	        }
		}		

		public function getWhereMember($id){
			$q = $this->db->get_where('tbl_anggota' ,array('id_kop'=>$id));
			return $q->result();
		}

		public function changeData($data, $id){
			$q = $this->db->where('id_kop', $id);
			$q = $this->db->update('tbl_anggota', $data);
			return $q;
		}

		public function listBulan($i){
			$arr = array('',"Januari","Febuari","Maret","April","Mei","Juni","Juli","Agutus","September","Oktober","November","Desember");
			return $arr[$i];
		}

		public function hitung_total($field_name){
			$npk = $this->session->userdata('id_kop'); 
			$bulan_in = $this->input->post('bulan');
			$tahun_in = $this->input->post('tahun');
			$q = $this->db->query("SELECT SUM($field_name) AS 'hasil' FROM tbl_transaksi WHERE id_kop='$npk'")->result();
			
			foreach ($q as $key => $value);
			return $value->hasil;
		}

		public function getIdKop($npk){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_anggota',array("npk"=>$npk));
			return $q->result();
		}

		public function getStatusId($npk){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_anggota',array("npk"=>$npk));
			return $q->num_rows();
		}

		public function checkPass($id_kop){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_anggota',array('id_kop'=>$id_kop));
			return $q->num_rows();
		}

		public function getJoinDate($id_kop){
			$joinDate = "";
			$this->db->select('tanggal_gabung');
			$q = $this->db->get_where('tbl_anggota',array('id_kop' => $id_kop));
			$result = $q->result();
			foreach ($result as $key => $value) {
				$joinDate = $value->tanggal_gabung;
			}
			return $joinDate;
		}

		public function getLoanMember($id_kop){
			$q = $this->db->query("SELECT tbl_pinjaman.id_pinjaman,tbl_pinjaman.status_pinjaman,tbl_pinjaman.kategori, tbl_anggota.nama_anggota, tbl_instalasi.nama_instalasi, tbl_pinjaman.jumlah_pinjaman, tbl_pinjaman.tenor, tbl_pinjaman.bulan_pinjaman, tbl_pinjaman.tahun_pinjaman FROM tbl_pinjaman INNER JOIN tbl_instalasi ON tbl_pinjaman.id_instalasi=tbl_instalasi.id_instalasi INNER JOIN tbl_anggota ON tbl_pinjaman.id_kop=tbl_anggota.id_kop WHERE tbl_pinjaman.id_kop='$id_kop'");
			return $q->result();
		}

		public function getInfoPinjaman($id_pinjaman){
			$q = $this->db->query("SELECT * FROM (`tbl_pinjaman`) JOIN `tbl_anggota` ON `tbl_anggota`.`id_kop` = `tbl_pinjaman`.`id_kop` JOIN `tbl_instalasi` ON `tbl_instalasi`.`id_instalasi` = `tbl_instalasi`.`id_instalasi` WHERE `id_pinjaman` = '$id_pinjaman' GROUP BY id_pinjaman");
			return $q->result();
		}

		public function getAngsuran($id_pinjaman){
			$this->db->select('*');
			$q = $this->db->get_where('tbl_angsuran',array('id_pinjaman'=>$id_pinjaman));
			return $q->result();
		}
		
		public function getTotalSimpanan($id_kop){
			$total = 0;
			$q = $this->db->query("SELECT SUM(simpanan_wajib) as simjip,SUM(simpanan_pokok) as simpok,SUM(simpanan_sukarela) as simsuk,
				SUM(SHU) as shu from tbl_transaksi where id_kop ='$id_kop'");
			$result = $q->result();

			foreach ($result as $key => $value) {
				$total = ($value->simjip + $value->simpok + $value->simsuk + $value->shu);
			}

			return $total;
		}

		public function getTotalAngsuran($id_pinjaman){
			$total = 0;
			$q = $this->db->query("SELECT SUM(jumlah_angsuran) as total_angsuran from tbl_angsuran where id_pinjaman ='$id_pinjaman'");
			$result = $q->result();

			foreach ($result as $key => $value) {
				$total = $value->total_angsuran;
			}
			return $total;
		}

		public function getTotalPinjaman($id_kop){
			$total = 0;
			$this->db->select('*');
			$q = $this->db->get_where('tbl_pinjaman',array('id_kop'=>$id_kop))->result();

			foreach ($q as $key => $value) {
				$total += $value->jumlah_pinjaman;
				$total -= $this->getTotalAngsuran($value->id_pinjaman);
			}

			return $total;
		}

	}
?>