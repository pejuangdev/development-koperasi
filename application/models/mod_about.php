<?php
	Class Mod_about extends CI_Model{

		public function getAbout(){
			$q = $this->db->query("SELECT * FROM tbl_about ORDER BY id_about DESC"); 
			return $q->result();
		}

		//admin

		public function getWhere($id){
			$q = $this->db->get_where('tbl_about' ,array('id_about'=>$id));
			return $q->result();
		}

		public function updateAbout($data,$id){
			$q = $this->db->where('id_about', $id);
			$q = $this->db->update('tbl_about', $data);
			return $q;
		}

		public function deleteAbout($id){
			return $this->db->delete("tbl_misi",array("id_misi"=>$id));
		}

		public function add_misi($data){
			return $this->db->insert("tbl_misi",$data);
		}

	}
	
?>