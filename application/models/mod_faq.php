<?php
	Class Mod_faq extends CI_Model{

		public function getFaq(){
			$q = $this->db->query("SELECT * FROM d_faq ORDER BY id_faq ASC"); 
			return $q->result();
		}

		//admin
		public function getFaqAdmin(){
			$q = $this->db->query("SELECT * FROM tbl_faq ORDER BY id_faq DESC"); 
			return $q->result();
		}

		public function addFaqData($data){
			return $this->db->insert("tbl_faq", $data);
		}

		public function deleteFaq($id){
			return $this->db->delete("tbl_faq", array("id_faq"=>$id));
		}

		public function getEditFaqAdmin($id){
			return $this->db->get_where("tbl_faq", array("id_faq"=>$id))->result();
		}

		public function editFaq($data,$id){
			$q = $this->db->where("id_faq",$id);
			$q = $this->db->update("tbl_faq", $data);
			return $q;
		}

	}
	
?>