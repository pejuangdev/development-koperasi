<?php
	Class Mod_news extends CI_Model{

		public function getNews($limit,$offset){
			$q = $this->db->query("SELECT * FROM tbl_news WHERE status='1' ORDER BY id_news DESC LIMIT ".$limit." OFFSET ".$offset); 
			return $q->result();
		}

		public function count_all(){
			return $this->db->count_all("tbl_news");
		}

		public function getDetailNews($id){
			$q = $this->db->query("SELECT * FROM tbl_news WHERE id_news='$id'");
			return $q->result();
		}

		//admin

		public function getNewsAdmin($limit,$offset){
			$q = $this->db->query("SELECT * FROM tbl_news ORDER BY id_news DESC LIMIT " .$limit. " OFFSET ".$offset); 
			return $q->result();
		}

		public function getNewsWhere($id){
			$q = $this->db->get_where("tbl_news",array("id_news"=>$id));
			return $q->result();
		}

		public function editNews($datax, $id){
			$data = $this->db->where("id_news", $id);
			$data = $this->db->update("tbl_news", $datax);
			return $data;
		}

		public function addNews($data){
			return $this->db->insert("tbl_news", $data);
		}

		public function deleteNews($id){
			return $this->db->delete("tbl_news",array("id_news"=>$id));
		}

	}
	
?>