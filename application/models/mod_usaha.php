<?php
	Class Mod_usaha extends CI_Model{

		public function getUsaha(){
			$q = $this->db->query("SELECT * FROM tbl_usaha ORDER BY id_usaha DESC"); 
			return $q->result();
		}

		//admin
		public function getUsahaAdmin(){
			$q = $this->db->query("SELECT * FROM tbl_usaha ORDER BY id_usaha DESC"); 
			return $q->result();
		}

		public function addUsaha($data){
			$q = $this->db->insert("tbl_usaha",$data);
			return $q;
		}

		public function getWhere($id){
			$q = $this->db->get_where('tbl_usaha' ,array('id_usaha'=>$id));
			return $q->result();
		}

		public function updateUsaha($data,$id){
			$q = $this->db->where('id_usaha', $id);
			$q = $this->db->update('tbl_usaha', $data);
			return $q;
		}

		public function deleteUsaha($id){
			return $this->db->delete("tbl_usaha",array("id_usaha"=>$id));
		}

	}
	
?>