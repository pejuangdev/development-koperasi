<?php
	Class Mod_slider extends CI_Model{

		public function getSlider(){
			$q = $this->db->query("SELECT * FROM d_cake ORDER BY id_cake_category DESC"); 
			return $q->result();
		}

		//admin
		public function getSliderAdmin(){
			$q = $this->db->query("SELECT * FROM tbl_slider ORDER BY status_slider DESC"); 
			return $q->result();
		}

		public function addSlider($data){
			$q = $this->db->insert("tbl_slider",$data); 
			return $q;
		}

		public function getCat(){
			$q = $this->db->query("SELECT * FROM d_cake_category");
			return $q->result_array();
		}

		public function addCategory($data){
			$q = $this->db->insert("d_cake_category",$data);
			return $q;
		}

		public function getWhere($id){
			$q = $this->db->get_where('tbl_slider' ,array('id_slider'=>$id));
			return $q->result();
		}

		public function updateSlider($data,$id){
			$q = $this->db->where('id_slider', $id);
			$q = $this->db->update('tbl_slider', $data);
			return $q;
		}

		public function getCheck(){
			$query = "SELECT * FROM tbl_slider WHERE status_slider='1'";
			$q = $this->db->query($query);
			return $q;
		}

		public function deleteSlider($id){
			return $this->db->delete("tbl_slider",array("id_slider"=>$id));
		}

	}
	
?>