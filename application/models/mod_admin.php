<?php
	Class Mod_admin extends CI_Model{

		public function getAdmin(){
			$q = $this->db->query("SELECT * FROM d_user ORDER BY id_user DESC"); 
			return $q->result();
		}

		public function deleteAdmin($id){
			return $this->db->delete("d_user",array("id_user"=>$id));
		}

		public function addAdmin($data){
			return $this->db->insert("d_user",$data);
		}

		public function getWhere($id){
			return $this->db->get_where("d_user",array("id_user"=>$id))->result();
		}

		public function editAdmin($data,$id){
			$q = $this->db->where("id_user",$id);
			$q = $this->db->update("d_user", $data);
			return $q;
		}
	}
	
?>