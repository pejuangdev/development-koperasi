var mdl = angular.module('myApp', []);
mdl.controller('appController', function($scope, $http){
	
	$scope.view_user = function(){
		$http.get('<?php echo base_url(); ?>index.php/admin/user/getUser').success(function(hasil){
			$scope.data = hasil;
		});
	}

	$scope.view_company = function(){
		$http.get('<?php echo base_url(); ?>index.php/admin/perusahaan/getCompany').success(function(hasil){
			$scope.data = hasil;
		});
	}

});