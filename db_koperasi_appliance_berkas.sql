-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2016 at 06:02 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appliance`
--

CREATE TABLE IF NOT EXISTS `tbl_appliance` (
`id_appliance` int(11) NOT NULL,
  `id_kop` varchar(255) NOT NULL,
  `id_installation` varchar(255) NOT NULL,
  `value_of` int(10) NOT NULL,
  `time_of` int(11) NOT NULL,
  `create_date` date DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `usages` text NOT NULL,
  `wife` varchar(255) DEFAULT NULL,
  `nilai_persetujuan` int(11) DEFAULT '0',
  `waktu_persetujuan` int(11) DEFAULT '0',
  `rencana_pencairan` date DEFAULT NULL,
  `nama_menyetujui` varchar(255) DEFAULT NULL,
  `status_appliance` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 belum upload berkas, 1 udah upload berkas'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_berkas`
--

CREATE TABLE IF NOT EXISTS `tbl_berkas` (
`id_berkas` int(11) NOT NULL,
  `id_kop` varchar(255) NOT NULL,
  `ktp` text,
  `kk` text,
  `slip_gaji` text,
  `buku_tabungan` text,
  `id_card` text,
  `surat_kuasa` text,
  `npwp` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_appliance`
--
ALTER TABLE `tbl_appliance`
 ADD PRIMARY KEY (`id_appliance`);

--
-- Indexes for table `tbl_berkas`
--
ALTER TABLE `tbl_berkas`
 ADD PRIMARY KEY (`id_berkas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_appliance`
--
ALTER TABLE `tbl_appliance`
MODIFY `id_appliance` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_berkas`
--
ALTER TABLE `tbl_berkas`
MODIFY `id_berkas` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
